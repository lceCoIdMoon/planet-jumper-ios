﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AdManager::Awake()
extern void AdManager_Awake_m23660705BCDFB8A358226BF4701EB28031B62E57 (void);
// 0x00000002 System.Void AdManager::Start()
extern void AdManager_Start_m7441282F4D11035BC4FA1D580553465DD9A85700 (void);
// 0x00000003 System.Void AdManager::ShowVideoAd()
extern void AdManager_ShowVideoAd_mF570DAD340FD93AC8D34CC31FB879F0920BC08DE (void);
// 0x00000004 System.Void AdManager::ShowBannerAd()
extern void AdManager_ShowBannerAd_m25552025306D26698AB1A0139897715CCE2F467D (void);
// 0x00000005 System.Void AdManager::HideBannerAd()
extern void AdManager_HideBannerAd_m80F7DDC674F742D8DF24A9175E1F31DBCC7C2B85 (void);
// 0x00000006 System.Collections.IEnumerator AdManager::ShowVideoWhenReady()
extern void AdManager_ShowVideoWhenReady_mAB6EB29D956C7A4948933149B96666936EDEE9D4 (void);
// 0x00000007 System.Collections.IEnumerator AdManager::ShowBannerWhenReady()
extern void AdManager_ShowBannerWhenReady_m44DB37C15766190A506F4FBF3A31CAD0634A4F33 (void);
// 0x00000008 System.Collections.IEnumerator AdManager::ShowRewardVideo()
extern void AdManager_ShowRewardVideo_mD03DD0DDF0EC57C86FBE44405B8C786C2D67ED6E (void);
// 0x00000009 System.Collections.IEnumerator AdManager::ShowSkipableVideo()
extern void AdManager_ShowSkipableVideo_mB56B41E0FA099E0B987E84F52B36DC8C9C7D936B (void);
// 0x0000000A System.Void AdManager::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void AdManager_OnUnityAdsDidFinish_m6FD703F7881BE4A1B281B3693A65DF8C86E3BEE6 (void);
// 0x0000000B System.Void AdManager::OnUnityAdsReady(System.String)
extern void AdManager_OnUnityAdsReady_m11DE56EA9248B9F61E77CFD49F565AC9A8A1A725 (void);
// 0x0000000C System.Void AdManager::OnUnityAdsDidError(System.String)
extern void AdManager_OnUnityAdsDidError_mBA37A489E9DCC40E679FD2B1C757F2D9C5655ACC (void);
// 0x0000000D System.Void AdManager::OnUnityAdsDidStart(System.String)
extern void AdManager_OnUnityAdsDidStart_m0110D7C0B5F451603085F927C50EA9DDC8149454 (void);
// 0x0000000E System.Boolean AdManager::DetermineIfAdRequired()
extern void AdManager_DetermineIfAdRequired_mF13D5BB47214A22751A0E8AC0F90499C2431A5C9 (void);
// 0x0000000F System.Void AdManager::.ctor()
extern void AdManager__ctor_m90E4F3ECE5164648C36A623707AF2B9F67A9C6E6 (void);
// 0x00000010 System.Void AdManager::.cctor()
extern void AdManager__cctor_m4DFC56B7E5F3E5582B1DAE04280A04606712C903 (void);
// 0x00000011 System.Boolean AdManager::<ShowVideoWhenReady>b__12_0()
extern void AdManager_U3CShowVideoWhenReadyU3Eb__12_0_m1E2BC1E0CFCA10A98463B7A17C68033ED6E4757A (void);
// 0x00000012 System.Boolean AdManager::<ShowBannerWhenReady>b__13_0()
extern void AdManager_U3CShowBannerWhenReadyU3Eb__13_0_mF475F81D498A73C074630803D03B503019A754AE (void);
// 0x00000013 System.Boolean AdManager::<ShowRewardVideo>b__14_0()
extern void AdManager_U3CShowRewardVideoU3Eb__14_0_mDD23B6010B4B75D017C19A37650D7AC20B3D6D78 (void);
// 0x00000014 System.Boolean AdManager::<ShowRewardVideo>b__14_1()
extern void AdManager_U3CShowRewardVideoU3Eb__14_1_m40220AB1F9F5737D38DCB1443D82A4E75C5DEA6B (void);
// 0x00000015 System.Boolean AdManager::<ShowSkipableVideo>b__15_0()
extern void AdManager_U3CShowSkipableVideoU3Eb__15_0_m8562CAC9BA1995F5B5A1B317255D06A2FD4E8442 (void);
// 0x00000016 System.Boolean AdManager::<ShowSkipableVideo>b__15_1()
extern void AdManager_U3CShowSkipableVideoU3Eb__15_1_m1587D67F5B0B83F3CA4030AEC2F7BA5200018A66 (void);
// 0x00000017 System.Void CameraFollow::Awake()
extern void CameraFollow_Awake_mDEEAFC7E875D37498563B8045AEEC8B948A74BAD (void);
// 0x00000018 System.Void CameraFollow::Start()
extern void CameraFollow_Start_mB2BDD148E8E2037AE10A4B479CFDD3477E8F8FCA (void);
// 0x00000019 System.Void CameraFollow::UpdatePosition()
extern void CameraFollow_UpdatePosition_m2905CA042449E8C8A25D15560005F604FF8A72F3 (void);
// 0x0000001A System.Void CameraFollow::Update()
extern void CameraFollow_Update_m7DD0DB22B0A37A8E6D882095C8FD3A597CF07D93 (void);
// 0x0000001B System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m511C23D2A1559F967CA471B8E1E33CCB9CEDBF3E (void);
// 0x0000001C System.Void CameraFollow::.cctor()
extern void CameraFollow__cctor_m343CA8142E0CB14FE520A6F2BD91C16B1120EFD0 (void);
// 0x0000001D System.Void ScrollBackground::Update()
extern void ScrollBackground_Update_mE99D99086BB59D197AD10D7A330DFCD7702B88F2 (void);
// 0x0000001E System.Void ScrollBackground::.ctor()
extern void ScrollBackground__ctor_m2686390B5F67D92130F4E3CD9C14E1DC5D215EBC (void);
// 0x0000001F System.Void Global::.ctor()
extern void Global__ctor_mE88BB1E563125731AFAF9A6D0670472EC2DE219B (void);
// 0x00000020 System.Void MenuManager::Start()
extern void MenuManager_Start_m3488752248C062EB8FFA7B9E279B96F7F9882E97 (void);
// 0x00000021 System.Collections.IEnumerator MenuManager::StartLevel(System.Int32)
extern void MenuManager_StartLevel_m464210AEFE9A76D81BC80F4754D6EA3282CA17AD (void);
// 0x00000022 System.Collections.IEnumerator MenuManager::FadeOutUI()
extern void MenuManager_FadeOutUI_m3785CA328E92BFAC071CDEE1409C064178E4C520 (void);
// 0x00000023 System.Void MenuManager::OnPlayButton()
extern void MenuManager_OnPlayButton_m398B2D88D092EC8A24D3A843C13723F88688922C (void);
// 0x00000024 System.Void MenuManager::OnTutorialButton()
extern void MenuManager_OnTutorialButton_mE0CC23B609E83F6EC30A75DA3BCF66FBB5158EBC (void);
// 0x00000025 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_mC328EA95D8565E70AAEDA542072A25B4EFB7ECED (void);
// 0x00000026 System.Void ScoreKeeper::Awake()
extern void ScoreKeeper_Awake_m582C6B5EF288CE019ADDE183E5F82E6BF29401A8 (void);
// 0x00000027 System.Void ScoreKeeper::Start()
extern void ScoreKeeper_Start_m3F2568722F6BE86E236EA2F3EC6616083C254B09 (void);
// 0x00000028 System.Void ScoreKeeper::IncrementScore()
extern void ScoreKeeper_IncrementScore_mD8588127D30983BA1DFC2280E06398B6984488FC (void);
// 0x00000029 System.Void ScoreKeeper::CheckHighScore()
extern void ScoreKeeper_CheckHighScore_mED24FEDF03E673C3D876F5F21B7F7B64872B874F (void);
// 0x0000002A System.Void ScoreKeeper::.ctor()
extern void ScoreKeeper__ctor_mAB060FA16AC4FB1EA2A8512271B3E35B9FB13549 (void);
// 0x0000002B System.Void SpriteAnimation::Start()
extern void SpriteAnimation_Start_m20A08E457A626A6844D96C83FA20FA83D109432F (void);
// 0x0000002C System.Void SpriteAnimation::Play(SpriteAnimationClip)
extern void SpriteAnimation_Play_m87833C545FB12571166E0F21CE05A2EF75E369DF (void);
// 0x0000002D System.Collections.IEnumerator SpriteAnimation::PlayEnum(SpriteAnimationClip)
extern void SpriteAnimation_PlayEnum_m8E0504F15977C07478DFF1F57883C8171972027F (void);
// 0x0000002E System.Void SpriteAnimation::.ctor()
extern void SpriteAnimation__ctor_m0402471B79214F0578E98DF27FB2CC97A732CA5B (void);
// 0x0000002F System.Void SpriteAnimationClip::.ctor()
extern void SpriteAnimationClip__ctor_mC70DF943BEAD2B7DB818A799893650237B55730B (void);
// 0x00000030 System.Void SpriteAnimator::.ctor()
extern void SpriteAnimator__ctor_m5DB4A7A81BBADE389F1A633AFB75E7EC82DC23DD (void);
// 0x00000031 System.Void FlyByMeteor::Start()
extern void FlyByMeteor_Start_mF178FB797A28AE6A289A360AC5864AADDC30F72A (void);
// 0x00000032 System.Void FlyByMeteor::Update()
extern void FlyByMeteor_Update_mAA8F09905210EA00AAEE8CE7D26AEDFC695D5429 (void);
// 0x00000033 System.Void FlyByMeteor::.ctor()
extern void FlyByMeteor__ctor_m3D74A479994810BDDD8B19CDFDD40AD83D815CB4 (void);
// 0x00000034 System.Void Meteor::Start()
extern void Meteor_Start_m14AA0D16FEC249A824E9758B3A3F6AA472C8DC7D (void);
// 0x00000035 System.Void Meteor::Update()
extern void Meteor_Update_m44859EA973A09EB6DBDCF7071288F81D589E2DC5 (void);
// 0x00000036 System.Void Meteor::CheckHitPlanet()
extern void Meteor_CheckHitPlanet_mE77A8C780A4DFFDD6E51AA6D4783F11E67511F4A (void);
// 0x00000037 System.Void Meteor::OnHitPlanet()
extern void Meteor_OnHitPlanet_m005574E6034F6F5C37AB3B8F8F85F96D159445A7 (void);
// 0x00000038 System.Void Meteor::HitPlanetEffect()
extern void Meteor_HitPlanetEffect_mE84182E8047D338E2F013F60A5A5762A56877BC3 (void);
// 0x00000039 UnityEngine.Vector2 Meteor::ToVector2(UnityEngine.Vector3)
extern void Meteor_ToVector2_m5060E58584CBB4F99686062FB1544E7CAE781186 (void);
// 0x0000003A System.Void Meteor::.ctor()
extern void Meteor__ctor_m6B4E3772F26133D5833915158631769EC1697B1A (void);
// 0x0000003B System.Void MeteorSpawner::Start()
extern void MeteorSpawner_Start_mDE3F99B27A157C5D9CA700C73FEFE926F877D666 (void);
// 0x0000003C System.Void MeteorSpawner::Update()
extern void MeteorSpawner_Update_m7AF0FFD1E3C7D2DAC4D112042A9D4BB1D1F98B43 (void);
// 0x0000003D UnityEngine.Vector3 MeteorSpawner::GetMeteorTarget(PlanetSetup&)
extern void MeteorSpawner_GetMeteorTarget_m2C3A5506EC6C5AFD39E4872A722CA086F7F8E1BC (void);
// 0x0000003E System.Void MeteorSpawner::ScoreIncremented()
extern void MeteorSpawner_ScoreIncremented_mE21EE310145D1843750ADF8B76C998DF9C63454A (void);
// 0x0000003F System.Void MeteorSpawner::QuickSpawnFlyBy()
extern void MeteorSpawner_QuickSpawnFlyBy_m4AF6038E2490B8F50A1F8A6742194434CAA7BC25 (void);
// 0x00000040 System.Void MeteorSpawner::SpawnFlyBy(UnityEngine.Vector2,UnityEngine.Vector2)
extern void MeteorSpawner_SpawnFlyBy_m9E749FC615F774DEFB8499253A2961998CE3F271 (void);
// 0x00000041 System.Void MeteorSpawner::.ctor()
extern void MeteorSpawner__ctor_mB655D3E5F9B2D84FCA2FE61594449E942C1B6905 (void);
// 0x00000042 System.Void Obstacle::Hit()
extern void Obstacle_Hit_m88ECB453EDB91F6E91725B19347D4811AE2890E5 (void);
// 0x00000043 System.Collections.IEnumerator Obstacle::HitCoroutine()
extern void Obstacle_HitCoroutine_m4E31D0ACC9ADF9BD7DD3536CBF3A9AA1B91CF72E (void);
// 0x00000044 System.Void Obstacle::.ctor()
extern void Obstacle__ctor_m2A721D4A6D3CB0FB812FC38AFCAE3E86AB2EB0F3 (void);
// 0x00000045 System.Void PlanetDestructor::Kill()
extern void PlanetDestructor_Kill_m83B431D92B99C0BF9A4127210270AFBCD8E0836E (void);
// 0x00000046 System.Collections.IEnumerator PlanetDestructor::DieCoroutine()
extern void PlanetDestructor_DieCoroutine_mD166E330F13B7F18EBC55152F991505D3E7D7A9B (void);
// 0x00000047 System.Void PlanetDestructor::Explode()
extern void PlanetDestructor_Explode_mFE381D9AB9D52F6532151A5CA9D4E8D2CA261B7D (void);
// 0x00000048 System.Void PlanetDestructor::StopDeath()
extern void PlanetDestructor_StopDeath_m6409C00EBF8CDAE2F41B960B62E57AC4769F0067 (void);
// 0x00000049 System.Void PlanetDestructor::.ctor()
extern void PlanetDestructor__ctor_mB3C9E3C1513A03B7B30B6CE65140C2EC2EC6A6A7 (void);
// 0x0000004A System.Void ObstacleGenerator::Generate(System.Int32,System.Int32,System.Single,System.Single)
extern void ObstacleGenerator_Generate_mE13F9F57093A77D3E2241D145B41022252458AEC (void);
// 0x0000004B System.Void ObstacleGenerator::SpawnObstacle(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void ObstacleGenerator_SpawnObstacle_m9BCC1C87FD29973759EF59B08AC44C9E7FE62AD1 (void);
// 0x0000004C System.Void ObstacleGenerator::SpawnMeteorTarget(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void ObstacleGenerator_SpawnMeteorTarget_mA0D29114CF72D4F54B908A84E986563240329F2B (void);
// 0x0000004D System.Void ObstacleGenerator::.ctor()
extern void ObstacleGenerator__ctor_m84BD07C44EC80AA2610E4899A7F9928ECCE43F84 (void);
// 0x0000004E System.Void PlanetGenerator::Awake()
extern void PlanetGenerator_Awake_m1BE745D32387C09B243904AB55E9F4443388350C (void);
// 0x0000004F System.Void PlanetGenerator::SpawnPlanet(UnityEngine.GameObject)
extern void PlanetGenerator_SpawnPlanet_mC86A811CCDBE6A09D1AEBCB2D7F68DEA6064E44E (void);
// 0x00000050 System.Void PlanetGenerator::RemovePlanet(System.Int32)
extern void PlanetGenerator_RemovePlanet_mC57579688890A67278F661D7091744D60BA97AD1 (void);
// 0x00000051 PlanetSetup PlanetGenerator::GetNearestPlanet(System.Int32)
extern void PlanetGenerator_GetNearestPlanet_mD93401C2A5DC804712F83A45888CB18E34243F70 (void);
// 0x00000052 System.Void PlanetGenerator::.ctor()
extern void PlanetGenerator__ctor_m646CA471F268304549A0FA10C70FF61E510D86C0 (void);
// 0x00000053 System.Void PlanetPicker::Start()
extern void PlanetPicker_Start_m5F0592F2C63A6E301274A1EED3FD92C80E13ADE0 (void);
// 0x00000054 System.Void PlanetPicker::Generate()
extern void PlanetPicker_Generate_m4CAFEA2D337AB18BD23648935C477D1FDA483F99 (void);
// 0x00000055 System.Void PlanetPicker::.ctor()
extern void PlanetPicker__ctor_m472E889E2D8A1F87FF4C2992E16764154EB37840 (void);
// 0x00000056 System.Void PlanetSetup::Start()
extern void PlanetSetup_Start_m54E478A3892DB2EACEAE9C52B06E95620BC6AE1D (void);
// 0x00000057 System.Void PlanetSetup::.ctor()
extern void PlanetSetup__ctor_m4302BE3DBD72BA60D9CBFB76EF7FB5D9A6F915B9 (void);
// 0x00000058 System.Void Effects::Awake()
extern void Effects_Awake_m8AAD15DE14CEBE13DD66CF4B2A7D79961B0337A2 (void);
// 0x00000059 System.Void Effects::QuickInjectPlanet(System.Single)
extern void Effects_QuickInjectPlanet_m1C070DE86EB18734DDED7AAFDE5B63672EA2DC21 (void);
// 0x0000005A System.Void Effects::InjectPlanet(UnityEngine.Vector2,PlanetSetup,System.Single)
extern void Effects_InjectPlanet_m349697C7F029D6EF2BE670B68CDA189898D0B33B (void);
// 0x0000005B System.Void Effects::SimpleInjectPlanet(UnityEngine.Vector3,PlanetSetup,System.Single)
extern void Effects_SimpleInjectPlanet_mA75A54042CA01530965A5C076920270BB37DA11A (void);
// 0x0000005C System.Collections.IEnumerator Effects::AnimatedInjectPlanet(UnityEngine.Vector2,PlanetSetup,System.Single)
extern void Effects_AnimatedInjectPlanet_m27B98B2A0A8B008E7858C1BB6EA2A3C6C2CDA065 (void);
// 0x0000005D System.Void Effects::ShakeCameraVerySmall()
extern void Effects_ShakeCameraVerySmall_m9CA0F91D0FCA362581C7789A8099DA6C9469DAA8 (void);
// 0x0000005E System.Void Effects::ShakeCameraSmall()
extern void Effects_ShakeCameraSmall_mB10417515ED710D154DA00079BCA0F8FC858619D (void);
// 0x0000005F System.Void Effects::ShakeCameraMedium()
extern void Effects_ShakeCameraMedium_m4EF2BED0F4E8A1C9550D1371A4A09ABD25BCEFCC (void);
// 0x00000060 System.Void Effects::ShakeCameraBig()
extern void Effects_ShakeCameraBig_mC2F2148E20E0C666BA5A5E91E652CAF9D50A8689 (void);
// 0x00000061 System.Void Effects::FlyLandOnPlanetEffect()
extern void Effects_FlyLandOnPlanetEffect_m26F3287005C66CD0CE97D4BF115C6E2668D78335 (void);
// 0x00000062 System.Void Effects::JumpLandOnPlanetEffect()
extern void Effects_JumpLandOnPlanetEffect_m3BA96EC90CEB4A6943318E6C22448798EB0C2628 (void);
// 0x00000063 System.Void Effects::JumpEffect()
extern void Effects_JumpEffect_m0EB96ECE3E2F40A0E9199FF3F8074BB69C1289AB (void);
// 0x00000064 System.Void Effects::FlyEffect()
extern void Effects_FlyEffect_m84C2B938781B0CB9C5C991F530FB1B53E1C76F41 (void);
// 0x00000065 System.Void Effects::BoostInAirEffect()
extern void Effects_BoostInAirEffect_mA43A756D916170C7448464BFF589FA19EE05A7BB (void);
// 0x00000066 System.Void Effects::DieEffect()
extern void Effects_DieEffect_mF499F72133124277C338A767F4A5D5E30816F06B (void);
// 0x00000067 System.Void Effects::ReviveEffect()
extern void Effects_ReviveEffect_m253AA6ABB23D1D1106436C7EFFEBC20ACA5C4E15 (void);
// 0x00000068 System.Collections.IEnumerator Effects::ReviveEffectCoroutine()
extern void Effects_ReviveEffectCoroutine_m38A2A361F4B7599CE8DB3C359C19882ACA96463C (void);
// 0x00000069 UnityEngine.Vector2 Effects::ToVector2(UnityEngine.Vector3)
extern void Effects_ToVector2_m7C62F47B83A9511304993F054121420A2D27ED6F (void);
// 0x0000006A System.Void Effects::.ctor()
extern void Effects__ctor_m683B62B9C966DE89F31DE842DEE1A5DA2A5849E7 (void);
// 0x0000006B System.Void Health::Start()
extern void Health_Start_m4081E3B6C29AF6A976DE7C096AC28296D0C61E1A (void);
// 0x0000006C System.Void Health::Update()
extern void Health_Update_mF072C7DA028FD78FE00F308522349F908CB3942A (void);
// 0x0000006D System.Void Health::OnRunning()
extern void Health_OnRunning_mE93841582AAAE53EB91CEED5CB90A760D344EA3E (void);
// 0x0000006E System.Void Health::OnFlying()
extern void Health_OnFlying_mC9F128A0AB908079854843C957C898C94B7847CA (void);
// 0x0000006F System.Void Health::OnFadeOut()
extern void Health_OnFadeOut_m683AEF5A8C09CE8FD237D1B2BEF858D6DBE559D2 (void);
// 0x00000070 System.Void Health::OnFadeIn()
extern void Health_OnFadeIn_m9162F2E1FE223E0C4D911D1FAF0852AEF0008E4F (void);
// 0x00000071 System.Void Health::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Health_OnTriggerEnter2D_m7B4A21320200CA283D587EBC8F884AC16A50AB11 (void);
// 0x00000072 System.Void Health::Die()
extern void Health_Die_m0B7272E045337982C3779E9725D4D36EA51D8500 (void);
// 0x00000073 System.Collections.IEnumerator Health::DieCoroutine()
extern void Health_DieCoroutine_m7802D78E90F05577E151C058DC86EAE9D3F8625F (void);
// 0x00000074 System.Collections.IEnumerator Health::WatchVideoAd()
extern void Health_WatchVideoAd_mC5ABD79841EFB47DFCC27EF4C8B1A1C79F65D880 (void);
// 0x00000075 System.Void Health::ResetScene()
extern void Health_ResetScene_mC8C6BB27862666E5CD11F4F961FA594B727E0DB0 (void);
// 0x00000076 System.Void Health::ReloadScene()
extern void Health_ReloadScene_mD39D9E54958888375E5FFB0B24E2ECD9CFB4A723 (void);
// 0x00000077 System.Void Health::Revive()
extern void Health_Revive_m7FABF5096D26431A79AAA46073CC2AFF6C435872 (void);
// 0x00000078 PlanetSetup Health::ResetPosition()
extern void Health_ResetPosition_m247FBAED1EDD825306EF156D28FA98EF82AE0EC5 (void);
// 0x00000079 System.Void Health::OnWatchAdButton()
extern void Health_OnWatchAdButton_m091E46F82103E793413EE77D112239F3679B0878 (void);
// 0x0000007A System.Void Health::OnReloadButton()
extern void Health_OnReloadButton_m810DB22DBAC56B65522AF56EBF4C90D186222302 (void);
// 0x0000007B System.Void Health::OnReplayTutorialButton()
extern void Health_OnReplayTutorialButton_mF440EE30EA2B19A28152E8AF069FC27A6999561C (void);
// 0x0000007C System.Void Health::OnFly()
extern void Health_OnFly_m3D38C07527A558C40D8A5D985AE1B39A8A836696 (void);
// 0x0000007D System.Void Health::.ctor()
extern void Health__ctor_m453CFD175281721E22D995E6C4930DD2D092759C (void);
// 0x0000007E MoveState Move::get_moveState()
extern void Move_get_moveState_m16B3C71EB138F9F5AC823992D4DA3C25F0FD174E (void);
// 0x0000007F System.Void Move::set_moveState(MoveState)
extern void Move_set_moveState_m6108E5E15C3CE82A5B03DA58213F4F273A6FD6A1 (void);
// 0x00000080 System.Void Move::Awake()
extern void Move_Awake_m03FC74073BD0D7670842B7E6118DED673E612F62 (void);
// 0x00000081 System.Void Move::Start()
extern void Move_Start_m429C3F7FC1D126B379F321BBF2181F1BE1190A15 (void);
// 0x00000082 System.Void Move::Update()
extern void Move_Update_mF39A91C7E7EE81357653523B731F1EE0C4F3EE8C (void);
// 0x00000083 System.Void Move::OnRunning()
extern void Move_OnRunning_m3D9CE254D96220606FCE11BDD29550260A504435 (void);
// 0x00000084 System.Void Move::OnJumping()
extern void Move_OnJumping_m18EAAAA1E3C43599F45AD8C2BC3CA3C523541ED2 (void);
// 0x00000085 System.Void Move::CheckGroundedOnPlanet()
extern void Move_CheckGroundedOnPlanet_mE424B165D5B77AFFEE3A9CC1931246BE146C2A20 (void);
// 0x00000086 System.Void Move::OnFlying()
extern void Move_OnFlying_mEC3BCE3CF7F5B078FA8FFC4DA5B5B317424F76B4 (void);
// 0x00000087 System.Void Move::CheckHitPlanet()
extern void Move_CheckHitPlanet_m15616C4ED94762D7A82740D5400FC32DAE2DCA23 (void);
// 0x00000088 System.Void Move::UpdateRunDirection()
extern void Move_UpdateRunDirection_m0B87B93E578912E764850A480EB357DE9EA126C7 (void);
// 0x00000089 System.Void Move::LandOnPlanet(PlanetSetup)
extern void Move_LandOnPlanet_mC916D6E6F85834FB3C408058E4F01A3091E2A6F6 (void);
// 0x0000008A System.Void Move::SetCurrentPlanet(PlanetSetup)
extern void Move_SetCurrentPlanet_m1F1F5D38965DDF650D7326AD5DA2D4129F23ACC9 (void);
// 0x0000008B System.Void Move::UpdateUpDirection()
extern void Move_UpdateUpDirection_mC2BA127749A32CFBC0DE89F4763B949B25C739AC (void);
// 0x0000008C System.Void Move::RotateAroundCurrentPlanet()
extern void Move_RotateAroundCurrentPlanet_m5ED0B3CE9FC4AEA6D95A88B00C3DCBBE4609095C (void);
// 0x0000008D System.Void Move::KillPlanet()
extern void Move_KillPlanet_mF6C7D784ABA10F50985268C83DF2B6BBD08D6600 (void);
// 0x0000008E System.Void Move::HurtPlanet()
extern void Move_HurtPlanet_m18C3CF9BA1F0AF978DCA5E598C2871B781291958 (void);
// 0x0000008F System.Void Move::OnJumpButton()
extern void Move_OnJumpButton_mC90AC996BDC4F8403AFD0EB41889C9DF330814AD (void);
// 0x00000090 System.Boolean Move::Jump()
extern void Move_Jump_m0F503C76D8F3498E70592E96F167CFC5BBEE8488 (void);
// 0x00000091 System.Boolean Move::Fly(System.Single)
extern void Move_Fly_m12BFDDE12FBE9D58D77245D5D9DE80A8791824EA (void);
// 0x00000092 System.Void Move::OnSwipe(SwipeData)
extern void Move_OnSwipe_m7D1146FC1BA3EE86BC5BA969705BD2981410675B (void);
// 0x00000093 UnityEngine.Vector2 Move::ToVector2(UnityEngine.Vector3)
extern void Move_ToVector2_mAB826BABBEB7954E7BCDA02AE77F26314DC2F02D (void);
// 0x00000094 System.Void Move::.ctor()
extern void Move__ctor_mE26369B6EE18DAEBE5F11E093BECEB0A89FF3707 (void);
// 0x00000095 System.Void MoveStateEvent::.ctor()
extern void MoveStateEvent__ctor_m6575701FD24D6117811963318F079D6297993A28 (void);
// 0x00000096 System.Void DialogManager::Awake()
extern void DialogManager_Awake_mD95596C67086C341E1656F225367BD3A975A2E32 (void);
// 0x00000097 System.Collections.IEnumerator DialogManager::ShowDialog(UnityEngine.Vector2,System.String,System.Single,System.Single,System.Single,System.Boolean,SpriteAnimationClip)
extern void DialogManager_ShowDialog_m407D070C380D9EEC7A7684BF489E25D1388A00ED (void);
// 0x00000098 System.Collections.IEnumerator DialogManager::FadeIn(System.Single)
extern void DialogManager_FadeIn_m137584C6DD2F8F81C182C7D6F87048950A63E32F (void);
// 0x00000099 System.Collections.IEnumerator DialogManager::FadeOut(System.Single)
extern void DialogManager_FadeOut_mD9EC28EA91343643D86C521A66A9608314998288 (void);
// 0x0000009A System.Void DialogManager::.ctor()
extern void DialogManager__ctor_m1D0F5CE9C6E3341EEFAB534C82658840798F9165 (void);
// 0x0000009B System.Collections.IEnumerator Tutorial::Start()
extern void Tutorial_Start_m86A43FDD735D975B6D9DB7FC1564262E398C3275 (void);
// 0x0000009C System.Collections.IEnumerator Tutorial::Tut_Jump()
extern void Tutorial_Tut_Jump_m395168CF18B649C6492255E251B766C4292C6A0B (void);
// 0x0000009D System.Collections.IEnumerator Tutorial::Tut_Boost()
extern void Tutorial_Tut_Boost_mB4EF956CA50540B45D73B391C9D95F6CFB3CBC50 (void);
// 0x0000009E System.Collections.IEnumerator Tutorial::Tut_Explode()
extern void Tutorial_Tut_Explode_m4A2F0BF3BD1EE09AD223B792941510853F4E3BC3 (void);
// 0x0000009F System.Collections.IEnumerator Tutorial::Tut_Practice()
extern void Tutorial_Tut_Practice_m55A8E422A9B39D7D2C79CDF4185653A44FE46781 (void);
// 0x000000A0 System.Void Tutorial::LandOnPlanet()
extern void Tutorial_LandOnPlanet_m5FEA5D19935A4C29332D93230EEDAD2D95BECD21 (void);
// 0x000000A1 System.Void Tutorial::Trigger(System.String)
extern void Tutorial_Trigger_m4CF438200DE93AAC3E08E9144E52572E7A0E3D74 (void);
// 0x000000A2 System.Void Tutorial::OnTap()
extern void Tutorial_OnTap_mCE12B53B9C0AD989E13A81713EDA469C39365493 (void);
// 0x000000A3 System.Void Tutorial::OnSwipe(SwipeData)
extern void Tutorial_OnSwipe_m52B977BF03B9437BC283B194E303C7CFE1A55392 (void);
// 0x000000A4 System.Void Tutorial::.ctor()
extern void Tutorial__ctor_m68D89ED1D7DA006C0BEABDAB7681CF3F323C7ED8 (void);
// 0x000000A5 System.Boolean Tutorial::<Tut_Jump>b__7_0()
extern void Tutorial_U3CTut_JumpU3Eb__7_0_m23463F0E3F9A4D3BCB2EFE188AC0CD6E27882B24 (void);
// 0x000000A6 System.Boolean Tutorial::<Tut_Boost>b__8_0()
extern void Tutorial_U3CTut_BoostU3Eb__8_0_mBFD82A0AB229927110D6B69B448B67D9D130E732 (void);
// 0x000000A7 System.Boolean Tutorial::<Tut_Boost>b__8_1()
extern void Tutorial_U3CTut_BoostU3Eb__8_1_m9E9FF2B4FCA04914DB9A8D3E8C3C36C33900A38A (void);
// 0x000000A8 System.Boolean Tutorial::<Tut_Explode>b__9_0()
extern void Tutorial_U3CTut_ExplodeU3Eb__9_0_mEF9EE462297F3E0F97BDD6C604B9E8005D6F26D5 (void);
// 0x000000A9 System.Boolean Tutorial::<Tut_Explode>b__9_1()
extern void Tutorial_U3CTut_ExplodeU3Eb__9_1_m926DCD4E9EEEAA6CAD4CAB9C128B754DB192F153 (void);
// 0x000000AA System.Void TutorialCamera::Awake()
extern void TutorialCamera_Awake_mEACD12015039D4B7B4052EA2E8F19B0323CB422C (void);
// 0x000000AB System.Void TutorialCamera::LateUpdate()
extern void TutorialCamera_LateUpdate_m764FAFB8B47963F532EDF9DCF09F4BF4C69F8721 (void);
// 0x000000AC System.Collections.IEnumerator TutorialCamera::ZoomCamera(System.Single)
extern void TutorialCamera_ZoomCamera_m7C0148E75F008112B6BCA2E80F925921BF7BE06E (void);
// 0x000000AD System.Void TutorialCamera::.ctor()
extern void TutorialCamera__ctor_m1BAF6A06E608855C8E74B332B5C31399039559DB (void);
// 0x000000AE System.Void TutorialHealth::Start()
extern void TutorialHealth_Start_m9442304A1762C685832DE63D528C1A74EF47115A (void);
// 0x000000AF System.Void TutorialHealth::Die()
extern void TutorialHealth_Die_mAE3AD2B5B9D6483D8213B2A7A06D2096CAE5BA2A (void);
// 0x000000B0 System.Void TutorialHealth::OnRetryButton()
extern void TutorialHealth_OnRetryButton_mC7E3F91A6FAAA4A501BB3E81553E91245DBBC78A (void);
// 0x000000B1 System.Void TutorialHealth::OnReloadButton()
extern void TutorialHealth_OnReloadButton_mE0D5E2341B3F0C9FD4E747C7EDFE14118573EADA (void);
// 0x000000B2 System.Void TutorialHealth::.ctor()
extern void TutorialHealth__ctor_m3BA7E56260CCC08765C859D7E8AC1338CA6D7C99 (void);
// 0x000000B3 System.Void TutorialPlayer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void TutorialPlayer_OnTriggerEnter2D_m04A3B14C0253B06EDC52F46DECF6D0D417D9A240 (void);
// 0x000000B4 System.Void TutorialPlayer::.ctor()
extern void TutorialPlayer__ctor_mA2B20CDE111EFC4B67CF494549D7457B8795C100 (void);
// 0x000000B5 System.Void Upgrade::Init()
extern void Upgrade_Init_mE9F024E838C6C212A61C73B0D9021C486AC012FD (void);
// 0x000000B6 System.Void Upgrade::Dispose()
extern void Upgrade_Dispose_mFF9B28ADC2EDB950071DA8D57E228659B53686D3 (void);
// 0x000000B7 System.Void Upgrade::UpdateUI()
extern void Upgrade_UpdateUI_mFE24356DE688E125DE55EB26E9606E74DF37D56A (void);
// 0x000000B8 System.Void Upgrade::Complete()
extern void Upgrade_Complete_m340FC426661517936282BA7632E2E7DEA8944A2C (void);
// 0x000000B9 System.Void Upgrade::.ctor()
extern void Upgrade__ctor_mCA177A1FB25C5D32C6D1EEE486134661470B9663 (void);
// 0x000000BA System.Void ScoreUpgrade::Init()
extern void ScoreUpgrade_Init_mD2D0C13C97789130E850CFE0C30DA0446A7992A3 (void);
// 0x000000BB System.Void ScoreUpgrade::Increment()
extern void ScoreUpgrade_Increment_m297D5C41DB49652E02A53EBF5D7919C0C54EC19F (void);
// 0x000000BC System.Void ScoreUpgrade::.ctor()
extern void ScoreUpgrade__ctor_m8E0DDCF9144A2C2EA109277D48E26F0DD0B69B3E (void);
// 0x000000BD System.Void HighScoreUpgrade::Init()
extern void HighScoreUpgrade_Init_m831A9FE403331D6C1085E8F8E0105340818CA77B (void);
// 0x000000BE System.Void HighScoreUpgrade::Update()
extern void HighScoreUpgrade_Update_m39E75DF45EB2604278658D70E6A6C18B54FB6947 (void);
// 0x000000BF System.Void HighScoreUpgrade::.ctor()
extern void HighScoreUpgrade__ctor_m3CCD37A114D8B31BC2F43E7179C172DE450767D8 (void);
// 0x000000C0 System.Void DestroyObstacleUpgrade::Init()
extern void DestroyObstacleUpgrade_Init_m531408F3A3BB944143B6D2393689056A5B3C4877 (void);
// 0x000000C1 System.Void DestroyObstacleUpgrade::Increment()
extern void DestroyObstacleUpgrade_Increment_m3F6A14D968E8263B792F5EC67E8526013EA1AE46 (void);
// 0x000000C2 System.Void DestroyObstacleUpgrade::.ctor()
extern void DestroyObstacleUpgrade__ctor_m84176CF1B8BDB0DDE41FC16523DF91038CEB4E77 (void);
// 0x000000C3 System.Void RunDistanceUpgrade::Init()
extern void RunDistanceUpgrade_Init_mCE34EC8E068EFA6EEB0DE0F12636DFD0C7C20189 (void);
// 0x000000C4 System.Void RunDistanceUpgrade::StateChanged(MoveState)
extern void RunDistanceUpgrade_StateChanged_m2805BAC3D349047BD5DA17BAAF6DFEB0EF837605 (void);
// 0x000000C5 System.Void RunDistanceUpgrade::Update()
extern void RunDistanceUpgrade_Update_m650BEA68548394AB777AAF20FE9BDEFAC53AD5F2 (void);
// 0x000000C6 System.Void RunDistanceUpgrade::.ctor()
extern void RunDistanceUpgrade__ctor_mEADD663E6DF0047531BB5E2E34BDE777D68395BD (void);
// 0x000000C7 System.Void FlyDistanceUpgrade::Init()
extern void FlyDistanceUpgrade_Init_m5574E0929AA9FAC9499A967C92335210DC69FE3F (void);
// 0x000000C8 System.Void FlyDistanceUpgrade::StateChanged(MoveState)
extern void FlyDistanceUpgrade_StateChanged_mF6165E75F6F190787B223B638957DC778E466EC7 (void);
// 0x000000C9 System.Void FlyDistanceUpgrade::Update()
extern void FlyDistanceUpgrade_Update_mF57C7C8631D618933D27E3FC288AA864E1888D0A (void);
// 0x000000CA System.Void FlyDistanceUpgrade::.ctor()
extern void FlyDistanceUpgrade__ctor_mA18B2E93F6F5721C7A233F5D5B22731F598158DB (void);
// 0x000000CB System.Void JumpUpgrade::Init()
extern void JumpUpgrade_Init_mEDCF4BE1305DC62DAB7E80BB327A3EF3D43016A2 (void);
// 0x000000CC System.Void JumpUpgrade::StateChanged(MoveState)
extern void JumpUpgrade_StateChanged_mC51C161E4F2057B6E07C8013D02CADC5A3EE99A1 (void);
// 0x000000CD System.Void JumpUpgrade::.ctor()
extern void JumpUpgrade__ctor_m0B70535E412B1BE6799AD4F58CBFCFDCC580141D (void);
// 0x000000CE System.Void Upgrades::Start()
extern void Upgrades_Start_mF56C88C194FA1735DD6896DAEFCB6E71BB37BB1E (void);
// 0x000000CF System.Void Upgrades::Init()
extern void Upgrades_Init_mA9C6A90A47B734429D631165CB6E578A379EDB5F (void);
// 0x000000D0 System.Void Upgrades::Dispose()
extern void Upgrades_Dispose_m13F634843EE2BDA3478230C79BF3330A09F8ACBF (void);
// 0x000000D1 System.Void Upgrades::.ctor()
extern void Upgrades__ctor_m2085AE54DF0FFBAB33AC516F252A589AC63460C9 (void);
// 0x000000D2 System.Void Upgrades::.cctor()
extern void Upgrades__cctor_mE10B08E1BFD14253C579211CC00BAFC2D869DB48 (void);
// 0x000000D3 System.Void SwipeDetector::add_OnSwipe(System.Action`1<SwipeData>)
extern void SwipeDetector_add_OnSwipe_mC7504374613C2BCDAB66E2C8A0604419284399EF (void);
// 0x000000D4 System.Void SwipeDetector::remove_OnSwipe(System.Action`1<SwipeData>)
extern void SwipeDetector_remove_OnSwipe_mEB1C8B52C04B4035AF43B355816798428A83D69C (void);
// 0x000000D5 System.Void SwipeDetector::Awake()
extern void SwipeDetector_Awake_m667DA64654B1B61C3DE6E88602BF68DBA9F77D05 (void);
// 0x000000D6 System.Void SwipeDetector::Update()
extern void SwipeDetector_Update_mA095F65D05FE0B2FC84E88D00DCE273D74A13BEC (void);
// 0x000000D7 System.Void SwipeDetector::DetectSwipe()
extern void SwipeDetector_DetectSwipe_mB180D3B7319D0ABA8018FFD241DE61E0BBEF6A04 (void);
// 0x000000D8 System.Boolean SwipeDetector::IsVerticalSwipe()
extern void SwipeDetector_IsVerticalSwipe_m4A18894A11C10AFE8B90BE5C46A72101ED0A5550 (void);
// 0x000000D9 System.Boolean SwipeDetector::SwipeDistanceCheckMet()
extern void SwipeDetector_SwipeDistanceCheckMet_mA7B50D8242EF75DE6BA87CE8D79680B6696F6391 (void);
// 0x000000DA System.Single SwipeDetector::VerticalMovementDistance()
extern void SwipeDetector_VerticalMovementDistance_m14D16164E406E9F36DE81D218A722E6BCD30BCA4 (void);
// 0x000000DB System.Single SwipeDetector::HorizontalMovementDistance()
extern void SwipeDetector_HorizontalMovementDistance_mC3C2108EDD8B5C6CECD862EB6A6BA4AED318C7B8 (void);
// 0x000000DC System.Void SwipeDetector::SendSwipe(SwipeDirection)
extern void SwipeDetector_SendSwipe_m0CCD80C1CB0244DB9ED3DC8E441FEDD2FBA7D5C8 (void);
// 0x000000DD System.Void SwipeDetector::.ctor()
extern void SwipeDetector__ctor_m2411490382B3EAD042053E6843814D951CE9D535 (void);
// 0x000000DE System.Void SwipeDetector::.cctor()
extern void SwipeDetector__cctor_m9FC815266974DB68A2AF5E734193FD3C8774AEC2 (void);
// 0x000000DF System.Void SwipeDrawer::Awake()
extern void SwipeDrawer_Awake_mAAFCDB0019A32F7EF383B8476EA20DCA87A988E3 (void);
// 0x000000E0 System.Void SwipeDrawer::SwipeDetector_OnSwipe(SwipeData)
extern void SwipeDrawer_SwipeDetector_OnSwipe_m99756C14E37FC26D3549A4FABFDA429FFDAEFE44 (void);
// 0x000000E1 System.Void SwipeDrawer::.ctor()
extern void SwipeDrawer__ctor_mEE3B2042133A3DE219A2D53F60C194799D91EC6F (void);
// 0x000000E2 System.Void SwipeLogger::Awake()
extern void SwipeLogger_Awake_m9E3FF13AEF09A6C9EF5D5A31D51D06D5B05E0F58 (void);
// 0x000000E3 System.Void SwipeLogger::SwipeDetector_OnSwipe(SwipeData)
extern void SwipeLogger_SwipeDetector_OnSwipe_mEC318FFD810E51493952FC5824F8295306AD0FA5 (void);
// 0x000000E4 System.Void SwipeLogger::.ctor()
extern void SwipeLogger__ctor_m40B0353714BD1354E361B1A2679C0E4FD14F8C3E (void);
// 0x000000E5 System.Void VibrationExample::Start()
extern void VibrationExample_Start_m3DAC7A15FAE7DE2C4A060097C7DE611D7F982CF1 (void);
// 0x000000E6 System.Void VibrationExample::Update()
extern void VibrationExample_Update_m097A5DAB7D2813DAE8B9F4FADDF937CC2E2B8898 (void);
// 0x000000E7 System.Void VibrationExample::TapVibrate()
extern void VibrationExample_TapVibrate_mFC1CCA8028D81DB0ABDA9F03880F3E1EEF575808 (void);
// 0x000000E8 System.Void VibrationExample::TapVibrateCustom()
extern void VibrationExample_TapVibrateCustom_m9449632FF066CF40A7534427E743900F810562BA (void);
// 0x000000E9 System.Void VibrationExample::TapVibratePattern()
extern void VibrationExample_TapVibratePattern_m7BE781FA1658AC2087D3EC16E9DA8EE948FDD9B0 (void);
// 0x000000EA System.Void VibrationExample::TapCancelVibrate()
extern void VibrationExample_TapCancelVibrate_m4F010407AB216C99EDAA12996BF62F125DA1D750 (void);
// 0x000000EB System.Void VibrationExample::TapPopVibrate()
extern void VibrationExample_TapPopVibrate_mD38944AA2CB17074F44EE4C36B9CDCA9C3E3F507 (void);
// 0x000000EC System.Void VibrationExample::TapPeekVibrate()
extern void VibrationExample_TapPeekVibrate_m71F93101E937529DD73517BB35F3A3C01FBAE9A8 (void);
// 0x000000ED System.Void VibrationExample::TapNopeVibrate()
extern void VibrationExample_TapNopeVibrate_mF53458C0F1B6616EF5AD719634E31B0AE3E356BE (void);
// 0x000000EE System.Void VibrationExample::.ctor()
extern void VibrationExample__ctor_m404E55A0D0417C6E6221F55F69398A65D28F6DF1 (void);
// 0x000000EF System.Boolean Vibration::_HasVibrator()
extern void Vibration__HasVibrator_mF5D80E6C6D4ED65EA31AF53F37239422DB2AD61A (void);
// 0x000000F0 System.Void Vibration::_Vibrate()
extern void Vibration__Vibrate_m3F16161D6FB468D91AB64E87B3128BA43CCCFE08 (void);
// 0x000000F1 System.Void Vibration::_VibratePop()
extern void Vibration__VibratePop_m2DE0606500D6C6E83A7262C9E951BA2CF4AD37DC (void);
// 0x000000F2 System.Void Vibration::_VibratePeek()
extern void Vibration__VibratePeek_mA41E2F4A7858DBDD4DAFD194D407E8D35E405323 (void);
// 0x000000F3 System.Void Vibration::_VibrateNope()
extern void Vibration__VibrateNope_mF571C84C03060C5EA2270E855DE9D7A0BA1832C5 (void);
// 0x000000F4 System.Void Vibration::VibratePop()
extern void Vibration_VibratePop_mFBC32B5F33BE00437A52116FB73EE8B1B89B6C90 (void);
// 0x000000F5 System.Void Vibration::VibratePeek()
extern void Vibration_VibratePeek_m8CF7BE3C8DE43C0868EB499F2F79C0F080BF831D (void);
// 0x000000F6 System.Void Vibration::VibrateNope()
extern void Vibration_VibrateNope_m2759889AFA07070CD7D11B36A2E1C89439A7869E (void);
// 0x000000F7 System.Void Vibration::Vibrate(System.Int64)
extern void Vibration_Vibrate_m507B42BA9A94C2A816AACEF43D4F915AA7EB6BEF (void);
// 0x000000F8 System.Void Vibration::Vibrate(System.Int64[],System.Int32)
extern void Vibration_Vibrate_m7F7AA2EC5D51C3695AD7F74ED7E57DE7EC6E5601 (void);
// 0x000000F9 System.Void Vibration::Cancel()
extern void Vibration_Cancel_m03501DBD76CFBE60752FCBF522232F7C31D68371 (void);
// 0x000000FA System.Boolean Vibration::HasVibrator()
extern void Vibration_HasVibrator_mAF5A512BCA28DE44876091B362B716A6FA76B4AD (void);
// 0x000000FB System.Void Vibration::Vibrate()
extern void Vibration_Vibrate_mB24FD6BCCA46120E8BC9E2B5A0AC07377A975F66 (void);
// 0x000000FC System.Void EZCameraShake.CameraShakeInstance::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void CameraShakeInstance__ctor_mFC5596C19A153DE6BF6D0D3307C2C0C23C28E2DE (void);
// 0x000000FD System.Void EZCameraShake.CameraShakeInstance::.ctor(System.Single,System.Single)
extern void CameraShakeInstance__ctor_m6F5BD948021AB46E0C20D9C0844460F08F682050 (void);
// 0x000000FE UnityEngine.Vector3 EZCameraShake.CameraShakeInstance::UpdateShake()
extern void CameraShakeInstance_UpdateShake_mCC4D1DA6A345DF8A451FD37D6602D64CB3B8BC92 (void);
// 0x000000FF System.Void EZCameraShake.CameraShakeInstance::StartFadeOut(System.Single)
extern void CameraShakeInstance_StartFadeOut_m34CBCB84CD38E30CCEE6EC1F4DD17D767D354BDD (void);
// 0x00000100 System.Void EZCameraShake.CameraShakeInstance::StartFadeIn(System.Single)
extern void CameraShakeInstance_StartFadeIn_mEFA63502BF4BF7F7F7F3BB16ABE84ABA32BC65A0 (void);
// 0x00000101 System.Single EZCameraShake.CameraShakeInstance::get_ScaleRoughness()
extern void CameraShakeInstance_get_ScaleRoughness_m448D2601DDF4634663D3E79116067A76885A0391 (void);
// 0x00000102 System.Void EZCameraShake.CameraShakeInstance::set_ScaleRoughness(System.Single)
extern void CameraShakeInstance_set_ScaleRoughness_m78D7353FC70E7A80ED8CDA52109A37120F5AD157 (void);
// 0x00000103 System.Single EZCameraShake.CameraShakeInstance::get_ScaleMagnitude()
extern void CameraShakeInstance_get_ScaleMagnitude_mBB9BCE67D634BDB5FFD991D0BCBC431F4AAE59D9 (void);
// 0x00000104 System.Void EZCameraShake.CameraShakeInstance::set_ScaleMagnitude(System.Single)
extern void CameraShakeInstance_set_ScaleMagnitude_mC98C0F1A4B21D68B819950908F3CDB9D8F597C66 (void);
// 0x00000105 System.Single EZCameraShake.CameraShakeInstance::get_NormalizedFadeTime()
extern void CameraShakeInstance_get_NormalizedFadeTime_m193EFA194C495DC175A25653FF05A4BBFE1098AF (void);
// 0x00000106 System.Boolean EZCameraShake.CameraShakeInstance::get_IsShaking()
extern void CameraShakeInstance_get_IsShaking_m08CF1FBD03F7A48FF965121F188BCD4A436CDE5C (void);
// 0x00000107 System.Boolean EZCameraShake.CameraShakeInstance::get_IsFadingOut()
extern void CameraShakeInstance_get_IsFadingOut_m2F735BB85CF48A5814F1A2BE5C4838CD49723670 (void);
// 0x00000108 System.Boolean EZCameraShake.CameraShakeInstance::get_IsFadingIn()
extern void CameraShakeInstance_get_IsFadingIn_m92D5619B0A3746B054EE8AA7200EECE7FF528CE7 (void);
// 0x00000109 EZCameraShake.CameraShakeState EZCameraShake.CameraShakeInstance::get_CurrentState()
extern void CameraShakeInstance_get_CurrentState_mD96B0436F64A34F09C14B43FF5F902BF25E21FB6 (void);
// 0x0000010A EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_Bump()
extern void CameraShakePresets_get_Bump_m490EC11DED2B8B797F7FE6160C5DB116C5FBCCE4 (void);
// 0x0000010B EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_Explosion()
extern void CameraShakePresets_get_Explosion_m1F4B3F311132B3B1BCE5603523CE6B276B69094B (void);
// 0x0000010C EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_Earthquake()
extern void CameraShakePresets_get_Earthquake_mE5DD52B6204F0B1D78AFD3A260BD41ECAB750A3B (void);
// 0x0000010D EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_BadTrip()
extern void CameraShakePresets_get_BadTrip_mF78A89A7F0325A4120B4ED1C5812FE00E228F6E8 (void);
// 0x0000010E EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_HandheldCamera()
extern void CameraShakePresets_get_HandheldCamera_m7DE7C2BD89100B7D56BB3650B892F52A0503B4B6 (void);
// 0x0000010F EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_Vibration()
extern void CameraShakePresets_get_Vibration_mB71BD095DB1796312CFCE06B0B4B46E3DB970C97 (void);
// 0x00000110 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShakePresets::get_RoughDriving()
extern void CameraShakePresets_get_RoughDriving_m1DA39EFEBBC40D5EF3C45210BC18E750108A513C (void);
// 0x00000111 System.Void EZCameraShake.CameraShaker::Awake()
extern void CameraShaker_Awake_mCB503790AE714332E2A4BBCCC8DB9D804646947B (void);
// 0x00000112 System.Void EZCameraShake.CameraShaker::Update()
extern void CameraShaker_Update_m8C390AF57DA49A52F9C3F7D06CC615BD8DA1ACEF (void);
// 0x00000113 EZCameraShake.CameraShaker EZCameraShake.CameraShaker::GetInstance(System.String)
extern void CameraShaker_GetInstance_m55861D8421901E79FA1603E60911B0991EA55478 (void);
// 0x00000114 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShaker::Shake(EZCameraShake.CameraShakeInstance)
extern void CameraShaker_Shake_m225F6E34DA20D1B94B78EBEF9588351DC815F4B1 (void);
// 0x00000115 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShaker::ShakeOnce(System.Single,System.Single,System.Single,System.Single)
extern void CameraShaker_ShakeOnce_m4375B86141F295BF0A0D6F2A905F7CDDAC9F7773 (void);
// 0x00000116 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShaker::ShakeOnce(System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CameraShaker_ShakeOnce_mA4171FA0972A828E82A86530F38338B93C2D3A9B (void);
// 0x00000117 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShaker::StartShake(System.Single,System.Single,System.Single)
extern void CameraShaker_StartShake_mB8629FFD4D4FA5B53A51669F53B88D353C3E575F (void);
// 0x00000118 EZCameraShake.CameraShakeInstance EZCameraShake.CameraShaker::StartShake(System.Single,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CameraShaker_StartShake_mF25DED37988031A60BCD1B6C219D1BD9A2D97B71 (void);
// 0x00000119 System.Collections.Generic.List`1<EZCameraShake.CameraShakeInstance> EZCameraShake.CameraShaker::get_ShakeInstances()
extern void CameraShaker_get_ShakeInstances_m2B2986F453E6F17E6F5AC9CD2CFFE5CBD7F95EAE (void);
// 0x0000011A System.Void EZCameraShake.CameraShaker::OnDestroy()
extern void CameraShaker_OnDestroy_m7043F40F1928B2D4840FA91388FE534C07946B37 (void);
// 0x0000011B System.Void EZCameraShake.CameraShaker::.ctor()
extern void CameraShaker__ctor_mB068A2B5829D8F40BEB22E317CFE65F225B4D5D4 (void);
// 0x0000011C System.Void EZCameraShake.CameraShaker::.cctor()
extern void CameraShaker__cctor_m5B153C9D59A5DAEDC164F8AD643DD8895C9AAD7B (void);
// 0x0000011D UnityEngine.Vector3 EZCameraShake.CameraUtilities::SmoothDampEuler(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single)
extern void CameraUtilities_SmoothDampEuler_m263B709C1D8A1B7DF206F990BB9173AEADB2CD69 (void);
// 0x0000011E UnityEngine.Vector3 EZCameraShake.CameraUtilities::MultiplyVectors(UnityEngine.Vector3,UnityEngine.Vector3)
extern void CameraUtilities_MultiplyVectors_m6CC4653828412E5C92FC41591C277866C9AC21FC (void);
// 0x0000011F System.Void EZCameraShake.CameraUtilities::.ctor()
extern void CameraUtilities__ctor_m85D12A01645E4857A57295FA4971A2EFCB450AF2 (void);
// 0x00000120 System.Void AdManager/<ShowVideoWhenReady>d__12::.ctor(System.Int32)
extern void U3CShowVideoWhenReadyU3Ed__12__ctor_m98A7031866E979B71B2591C35D7F73A04A093660 (void);
// 0x00000121 System.Void AdManager/<ShowVideoWhenReady>d__12::System.IDisposable.Dispose()
extern void U3CShowVideoWhenReadyU3Ed__12_System_IDisposable_Dispose_mAC1F795668349D2EE5B0B95E71237596FD78468C (void);
// 0x00000122 System.Boolean AdManager/<ShowVideoWhenReady>d__12::MoveNext()
extern void U3CShowVideoWhenReadyU3Ed__12_MoveNext_m7AD3164E7538178A1B35835E7C27B9FA17327558 (void);
// 0x00000123 System.Object AdManager/<ShowVideoWhenReady>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowVideoWhenReadyU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35332681EC51C6A346A85AF9C6DA1AEE2DE8B29B (void);
// 0x00000124 System.Void AdManager/<ShowVideoWhenReady>d__12::System.Collections.IEnumerator.Reset()
extern void U3CShowVideoWhenReadyU3Ed__12_System_Collections_IEnumerator_Reset_m6D6CBCA897A40846DCD1A908214B508A74CA6314 (void);
// 0x00000125 System.Object AdManager/<ShowVideoWhenReady>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CShowVideoWhenReadyU3Ed__12_System_Collections_IEnumerator_get_Current_mD2D27C6DC724F16A54304AA8CBAD4D0EBAE17511 (void);
// 0x00000126 System.Void AdManager/<ShowBannerWhenReady>d__13::.ctor(System.Int32)
extern void U3CShowBannerWhenReadyU3Ed__13__ctor_mC99A5542C543C6FD4673DC2BD585834E1242C158 (void);
// 0x00000127 System.Void AdManager/<ShowBannerWhenReady>d__13::System.IDisposable.Dispose()
extern void U3CShowBannerWhenReadyU3Ed__13_System_IDisposable_Dispose_m59F972CEF5FA372D1FD0F8FABA32D422D984F145 (void);
// 0x00000128 System.Boolean AdManager/<ShowBannerWhenReady>d__13::MoveNext()
extern void U3CShowBannerWhenReadyU3Ed__13_MoveNext_mEE131F0C37E7D3842D7783EBF7E2B4C9C6993EAA (void);
// 0x00000129 System.Object AdManager/<ShowBannerWhenReady>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowBannerWhenReadyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m761C40B24422D905858B79C85633B976F332E47D (void);
// 0x0000012A System.Void AdManager/<ShowBannerWhenReady>d__13::System.Collections.IEnumerator.Reset()
extern void U3CShowBannerWhenReadyU3Ed__13_System_Collections_IEnumerator_Reset_mD1E6D86064C47FE7F099612280F97D31D3FFF6F9 (void);
// 0x0000012B System.Object AdManager/<ShowBannerWhenReady>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CShowBannerWhenReadyU3Ed__13_System_Collections_IEnumerator_get_Current_m6F1E151A3289CCB1785FA06A605675ADB0F81A25 (void);
// 0x0000012C System.Void AdManager/<ShowRewardVideo>d__14::.ctor(System.Int32)
extern void U3CShowRewardVideoU3Ed__14__ctor_m90D906E77791EB707D2C6BC4BAC4C0D731505B7F (void);
// 0x0000012D System.Void AdManager/<ShowRewardVideo>d__14::System.IDisposable.Dispose()
extern void U3CShowRewardVideoU3Ed__14_System_IDisposable_Dispose_m8F203E5F3519EFCC36BCB2573497BB6E8F6CF75C (void);
// 0x0000012E System.Boolean AdManager/<ShowRewardVideo>d__14::MoveNext()
extern void U3CShowRewardVideoU3Ed__14_MoveNext_mB2FC0172AB15A1B197C8D8668DCE23D5DA483712 (void);
// 0x0000012F System.Object AdManager/<ShowRewardVideo>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowRewardVideoU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m935432429FC6BC2877A85D36C7432F2E2D62EED8 (void);
// 0x00000130 System.Void AdManager/<ShowRewardVideo>d__14::System.Collections.IEnumerator.Reset()
extern void U3CShowRewardVideoU3Ed__14_System_Collections_IEnumerator_Reset_m122E3E9868F6D40EB59A49F08022DB7B3AD3F934 (void);
// 0x00000131 System.Object AdManager/<ShowRewardVideo>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CShowRewardVideoU3Ed__14_System_Collections_IEnumerator_get_Current_mBB2349ECF4F8F75D08751DDFA0DF3E074D3CC514 (void);
// 0x00000132 System.Void AdManager/<ShowSkipableVideo>d__15::.ctor(System.Int32)
extern void U3CShowSkipableVideoU3Ed__15__ctor_m09F4507AAB728DE7871C6E8E4BED5A808FC702E2 (void);
// 0x00000133 System.Void AdManager/<ShowSkipableVideo>d__15::System.IDisposable.Dispose()
extern void U3CShowSkipableVideoU3Ed__15_System_IDisposable_Dispose_m868AD0583BF061992737C7C1AD56221E213C58CA (void);
// 0x00000134 System.Boolean AdManager/<ShowSkipableVideo>d__15::MoveNext()
extern void U3CShowSkipableVideoU3Ed__15_MoveNext_m5F29EA1D008647F049C3E12984A40C7896D273FF (void);
// 0x00000135 System.Object AdManager/<ShowSkipableVideo>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowSkipableVideoU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DC72B7C7E23EC68C5FC800FD8E10D0CAD51047C (void);
// 0x00000136 System.Void AdManager/<ShowSkipableVideo>d__15::System.Collections.IEnumerator.Reset()
extern void U3CShowSkipableVideoU3Ed__15_System_Collections_IEnumerator_Reset_m87E9EBA69833374D69D0093339A492F75C7089B8 (void);
// 0x00000137 System.Object AdManager/<ShowSkipableVideo>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CShowSkipableVideoU3Ed__15_System_Collections_IEnumerator_get_Current_m233D159FE9FD1F3A0B58B95592542961CDE59B92 (void);
// 0x00000138 System.Void MenuManager/<StartLevel>d__5::.ctor(System.Int32)
extern void U3CStartLevelU3Ed__5__ctor_m40E0F43B47737DC030C73A3E96356F9BD3F8DC3A (void);
// 0x00000139 System.Void MenuManager/<StartLevel>d__5::System.IDisposable.Dispose()
extern void U3CStartLevelU3Ed__5_System_IDisposable_Dispose_m5B6A3BD7CA5E679DDED35991F6E25C9C73C1C0C9 (void);
// 0x0000013A System.Boolean MenuManager/<StartLevel>d__5::MoveNext()
extern void U3CStartLevelU3Ed__5_MoveNext_mBF771A4248B63A656381BB08FAB25E3163352281 (void);
// 0x0000013B System.Object MenuManager/<StartLevel>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB03222A9ECB2B0A5FB517DC8A1992DE21F618EB9 (void);
// 0x0000013C System.Void MenuManager/<StartLevel>d__5::System.Collections.IEnumerator.Reset()
extern void U3CStartLevelU3Ed__5_System_Collections_IEnumerator_Reset_mFF66B8B005D400D58205F7659EEA52BD4F193514 (void);
// 0x0000013D System.Object MenuManager/<StartLevel>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CStartLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m46D1448A62D1C9665DD54CCC0DEE7110E17C6564 (void);
// 0x0000013E System.Void MenuManager/<FadeOutUI>d__6::.ctor(System.Int32)
extern void U3CFadeOutUIU3Ed__6__ctor_m2539C9CC0A45F866592433CF83D246B274238F62 (void);
// 0x0000013F System.Void MenuManager/<FadeOutUI>d__6::System.IDisposable.Dispose()
extern void U3CFadeOutUIU3Ed__6_System_IDisposable_Dispose_m9B5877393AB42E4FE79B6527098713E2ABA2706D (void);
// 0x00000140 System.Boolean MenuManager/<FadeOutUI>d__6::MoveNext()
extern void U3CFadeOutUIU3Ed__6_MoveNext_m6F41CFEB427B526C0D09780FC731B2FC5D595DFA (void);
// 0x00000141 System.Object MenuManager/<FadeOutUI>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutUIU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CC14574227BB61CA70117E2361C0F6D87CD26EE (void);
// 0x00000142 System.Void MenuManager/<FadeOutUI>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutUIU3Ed__6_System_Collections_IEnumerator_Reset_mFB8739A6E257A0720CE9A3AD6A932BC0D91FC5C8 (void);
// 0x00000143 System.Object MenuManager/<FadeOutUI>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutUIU3Ed__6_System_Collections_IEnumerator_get_Current_m53D5C355FC8DDC1FE0FAE3329A10FB0C7C06BD71 (void);
// 0x00000144 System.Void SpriteAnimation/<PlayEnum>d__6::.ctor(System.Int32)
extern void U3CPlayEnumU3Ed__6__ctor_m683E1D5BF11617BE9754230490D5B39B35BCDE4B (void);
// 0x00000145 System.Void SpriteAnimation/<PlayEnum>d__6::System.IDisposable.Dispose()
extern void U3CPlayEnumU3Ed__6_System_IDisposable_Dispose_m11744D53E003DE366748550850C1C1E704FC5F18 (void);
// 0x00000146 System.Boolean SpriteAnimation/<PlayEnum>d__6::MoveNext()
extern void U3CPlayEnumU3Ed__6_MoveNext_mD57644BCB6BB897F860F6EFB211034B0710A3562 (void);
// 0x00000147 System.Object SpriteAnimation/<PlayEnum>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayEnumU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEED7B3ED5FA1A7114E91DC8F0A515EC33CCA5F3E (void);
// 0x00000148 System.Void SpriteAnimation/<PlayEnum>d__6::System.Collections.IEnumerator.Reset()
extern void U3CPlayEnumU3Ed__6_System_Collections_IEnumerator_Reset_mBFA15579940218EA9472991CB4241955FC8D701E (void);
// 0x00000149 System.Object SpriteAnimation/<PlayEnum>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CPlayEnumU3Ed__6_System_Collections_IEnumerator_get_Current_m891D814CA530EAFBE6696FB8052CC30E611EC46D (void);
// 0x0000014A System.Void Obstacle/<HitCoroutine>d__5::.ctor(System.Int32)
extern void U3CHitCoroutineU3Ed__5__ctor_m6AD40ACDEC1086934D80F542287503167F401F23 (void);
// 0x0000014B System.Void Obstacle/<HitCoroutine>d__5::System.IDisposable.Dispose()
extern void U3CHitCoroutineU3Ed__5_System_IDisposable_Dispose_m2A80DBEB9842D8BBADC1E619F8FDEC79484C2165 (void);
// 0x0000014C System.Boolean Obstacle/<HitCoroutine>d__5::MoveNext()
extern void U3CHitCoroutineU3Ed__5_MoveNext_m4745EE755D415F989E61AAA96712FA29286359EC (void);
// 0x0000014D System.Object Obstacle/<HitCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHitCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6CD2908A82669BDC95E4F27EEF1FFCB166ABB3 (void);
// 0x0000014E System.Void Obstacle/<HitCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CHitCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m00610283EDCE305FC6FFCFE43EBD7672346B2F08 (void);
// 0x0000014F System.Object Obstacle/<HitCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CHitCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5908ABFC35FC5053D16FC54AAD13D26AE6EFBBE0 (void);
// 0x00000150 System.Void PlanetDestructor/<>c::.cctor()
extern void U3CU3Ec__cctor_mB709F9F3611E7B81D7C5D61067777C51EDFA2BAF (void);
// 0x00000151 System.Void PlanetDestructor/<>c::.ctor()
extern void U3CU3Ec__ctor_mC9241A41792541F3728B1769FA1C21093A704572 (void);
// 0x00000152 System.Boolean PlanetDestructor/<>c::<DieCoroutine>b__15_0()
extern void U3CU3Ec_U3CDieCoroutineU3Eb__15_0_mCEE744303E19F3BF656F17E0ACBA25FAB711FB8B (void);
// 0x00000153 System.Void PlanetDestructor/<DieCoroutine>d__15::.ctor(System.Int32)
extern void U3CDieCoroutineU3Ed__15__ctor_mC526EE7DE51527F8FCF48AC433EAEDDC24CB2D6F (void);
// 0x00000154 System.Void PlanetDestructor/<DieCoroutine>d__15::System.IDisposable.Dispose()
extern void U3CDieCoroutineU3Ed__15_System_IDisposable_Dispose_m72CECC45CF18D9AEBF9E72121A626DFC55740E77 (void);
// 0x00000155 System.Boolean PlanetDestructor/<DieCoroutine>d__15::MoveNext()
extern void U3CDieCoroutineU3Ed__15_MoveNext_mACEDF73FF962A543B1B0CBBD1560B4C383A4D40F (void);
// 0x00000156 System.Object PlanetDestructor/<DieCoroutine>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieCoroutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19C0962D36868DDF92F8BAB93CFBE269E912E2D7 (void);
// 0x00000157 System.Void PlanetDestructor/<DieCoroutine>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDieCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_mE7FBC826D4A9E7549C90E7ECE2BA5F75AF023D13 (void);
// 0x00000158 System.Object PlanetDestructor/<DieCoroutine>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDieCoroutineU3Ed__15_System_Collections_IEnumerator_get_Current_mD586E04066BF82A3AAEBEBA345291AA37C32BB2C (void);
// 0x00000159 System.Void Effects/<AnimatedInjectPlanet>d__11::.ctor(System.Int32)
extern void U3CAnimatedInjectPlanetU3Ed__11__ctor_m98BB095EE5A47C54F8632E65E5E2B24716DDCC45 (void);
// 0x0000015A System.Void Effects/<AnimatedInjectPlanet>d__11::System.IDisposable.Dispose()
extern void U3CAnimatedInjectPlanetU3Ed__11_System_IDisposable_Dispose_mC95FE3FDD263318ECFA511740A64F4139C2DEDB9 (void);
// 0x0000015B System.Boolean Effects/<AnimatedInjectPlanet>d__11::MoveNext()
extern void U3CAnimatedInjectPlanetU3Ed__11_MoveNext_mD7FB88BB991C09DD155EF214C89199319E772FD1 (void);
// 0x0000015C System.Object Effects/<AnimatedInjectPlanet>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatedInjectPlanetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0769F9D4CBC8E93031871DDCFD0F8C850C01454 (void);
// 0x0000015D System.Void Effects/<AnimatedInjectPlanet>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimatedInjectPlanetU3Ed__11_System_Collections_IEnumerator_Reset_m4BFA69312FFC201FA87F5AF749C70509871FD986 (void);
// 0x0000015E System.Object Effects/<AnimatedInjectPlanet>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatedInjectPlanetU3Ed__11_System_Collections_IEnumerator_get_Current_m371CC32A3F8C5511839ED9FA6941AAAC9C4A6B5B (void);
// 0x0000015F System.Void Effects/<ReviveEffectCoroutine>d__28::.ctor(System.Int32)
extern void U3CReviveEffectCoroutineU3Ed__28__ctor_mBA9A48DD5F68BDD684B0140514261E2D40E8A1CD (void);
// 0x00000160 System.Void Effects/<ReviveEffectCoroutine>d__28::System.IDisposable.Dispose()
extern void U3CReviveEffectCoroutineU3Ed__28_System_IDisposable_Dispose_m30F7FF873B8A3C9ECC3EBABAF209FE66D254E4D2 (void);
// 0x00000161 System.Boolean Effects/<ReviveEffectCoroutine>d__28::MoveNext()
extern void U3CReviveEffectCoroutineU3Ed__28_MoveNext_m5A0DAC4358705EA3150F42D0642A702A4FB0316F (void);
// 0x00000162 System.Object Effects/<ReviveEffectCoroutine>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReviveEffectCoroutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92FF2104968A48B721C2EB70DF81D860960E71D7 (void);
// 0x00000163 System.Void Effects/<ReviveEffectCoroutine>d__28::System.Collections.IEnumerator.Reset()
extern void U3CReviveEffectCoroutineU3Ed__28_System_Collections_IEnumerator_Reset_mC69F751E657AE8BBEAE33ADA4A18163663B920E1 (void);
// 0x00000164 System.Object Effects/<ReviveEffectCoroutine>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CReviveEffectCoroutineU3Ed__28_System_Collections_IEnumerator_get_Current_m52E8094A91EC9066E8F3782B25FC883E0DDE6818 (void);
// 0x00000165 System.Void Health/<DieCoroutine>d__18::.ctor(System.Int32)
extern void U3CDieCoroutineU3Ed__18__ctor_m85A6272C2742837C14B6E9C85D8B7C9496F8AE3F (void);
// 0x00000166 System.Void Health/<DieCoroutine>d__18::System.IDisposable.Dispose()
extern void U3CDieCoroutineU3Ed__18_System_IDisposable_Dispose_m3827272D4C78A5081CCDAD8385C43677A81B517B (void);
// 0x00000167 System.Boolean Health/<DieCoroutine>d__18::MoveNext()
extern void U3CDieCoroutineU3Ed__18_MoveNext_m129439FEFFD54E06C9DE97E682C8642F6C495395 (void);
// 0x00000168 System.Object Health/<DieCoroutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DB5ED9B4489E6212EF73C23A5D574FA7551071A (void);
// 0x00000169 System.Void Health/<DieCoroutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CDieCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_m7838FEBAA1F59EF0ADFDCCEFA7A5D04A576D95A8 (void);
// 0x0000016A System.Object Health/<DieCoroutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CDieCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mDCFD3A37B225C07EFB6F26D99EA405B5BF9EB645 (void);
// 0x0000016B System.Void Health/<WatchVideoAd>d__19::.ctor(System.Int32)
extern void U3CWatchVideoAdU3Ed__19__ctor_m755D3C98EAC457887CCDDB5C6A7A6382326B2F7B (void);
// 0x0000016C System.Void Health/<WatchVideoAd>d__19::System.IDisposable.Dispose()
extern void U3CWatchVideoAdU3Ed__19_System_IDisposable_Dispose_m0781E5A60F6B3E3F9068EA84FCCC4AD5985D38AD (void);
// 0x0000016D System.Boolean Health/<WatchVideoAd>d__19::MoveNext()
extern void U3CWatchVideoAdU3Ed__19_MoveNext_m1F6ED44F2335EFA100C8F3C8A9E468D2BD18A2D5 (void);
// 0x0000016E System.Object Health/<WatchVideoAd>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWatchVideoAdU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F28EF7CCC5B92CFDA69403AF0B2C8EB64955760 (void);
// 0x0000016F System.Void Health/<WatchVideoAd>d__19::System.Collections.IEnumerator.Reset()
extern void U3CWatchVideoAdU3Ed__19_System_Collections_IEnumerator_Reset_m550423B5B3DFEFB813AA9605F15CA7395CA95748 (void);
// 0x00000170 System.Object Health/<WatchVideoAd>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CWatchVideoAdU3Ed__19_System_Collections_IEnumerator_get_Current_m48942039A88204D3222BC866AAFC0D3B9D525B1D (void);
// 0x00000171 System.Void DialogManager/<ShowDialog>d__5::.ctor(System.Int32)
extern void U3CShowDialogU3Ed__5__ctor_m59490F79477F3F1F0336B49386EDDFD6F2BE5DB6 (void);
// 0x00000172 System.Void DialogManager/<ShowDialog>d__5::System.IDisposable.Dispose()
extern void U3CShowDialogU3Ed__5_System_IDisposable_Dispose_mD3C3324D7F52CF790B32776002C14F0D5DF180DF (void);
// 0x00000173 System.Boolean DialogManager/<ShowDialog>d__5::MoveNext()
extern void U3CShowDialogU3Ed__5_MoveNext_m8A142805DD0A9D730843D91CC39FF282149E66C2 (void);
// 0x00000174 System.Object DialogManager/<ShowDialog>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowDialogU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EDBD3E2F35600B85F7BF5B6938676E9BA941A39 (void);
// 0x00000175 System.Void DialogManager/<ShowDialog>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowDialogU3Ed__5_System_Collections_IEnumerator_Reset_m35D259DF22BE8F219C3F63C508E150AA2668C8E6 (void);
// 0x00000176 System.Object DialogManager/<ShowDialog>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowDialogU3Ed__5_System_Collections_IEnumerator_get_Current_m58E5E7E9D9EC275715D5A8DA31E9DE96FDB9B6ED (void);
// 0x00000177 System.Void DialogManager/<FadeIn>d__6::.ctor(System.Int32)
extern void U3CFadeInU3Ed__6__ctor_mCBC0193FAACBBAD2FDB0DB7F013AA40C5F75A586 (void);
// 0x00000178 System.Void DialogManager/<FadeIn>d__6::System.IDisposable.Dispose()
extern void U3CFadeInU3Ed__6_System_IDisposable_Dispose_m7C565B903714E7A6886506FAF7FA4E5FA8209B0D (void);
// 0x00000179 System.Boolean DialogManager/<FadeIn>d__6::MoveNext()
extern void U3CFadeInU3Ed__6_MoveNext_m912877866891797DAE4F7308A3C7C3C58F0C03FA (void);
// 0x0000017A System.Object DialogManager/<FadeIn>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m733AF1F2DFCE51D95944D3CD2501AD2B2AA9CDB9 (void);
// 0x0000017B System.Void DialogManager/<FadeIn>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_mAC469BA28C6E64FBEBBFD14F5E72947611F623FF (void);
// 0x0000017C System.Object DialogManager/<FadeIn>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_mC04B8879882898F71F71485924CD8D3DE788E4B7 (void);
// 0x0000017D System.Void DialogManager/<FadeOut>d__7::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__7__ctor_m0469D5239E03FC76E471F47C6AD20E09C217DA42 (void);
// 0x0000017E System.Void DialogManager/<FadeOut>d__7::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__7_System_IDisposable_Dispose_mC0687484F27947CDF841CC52CB5468D295B14224 (void);
// 0x0000017F System.Boolean DialogManager/<FadeOut>d__7::MoveNext()
extern void U3CFadeOutU3Ed__7_MoveNext_m166A586295E7D425386167246F19F419B25DEC37 (void);
// 0x00000180 System.Object DialogManager/<FadeOut>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BA1C14E62F274432A381F967016D7ABA24D5465 (void);
// 0x00000181 System.Void DialogManager/<FadeOut>d__7::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m0E0AADBCFA0827B325D31962F4590DA9AEE32C93 (void);
// 0x00000182 System.Object DialogManager/<FadeOut>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_mB1CB7EF6E6DC081BF797B40C3C03A67930165CBA (void);
// 0x00000183 System.Void Tutorial/<Start>d__6::.ctor(System.Int32)
extern void U3CStartU3Ed__6__ctor_m82292CD1868B5627877555B7179646ABEA7B1425 (void);
// 0x00000184 System.Void Tutorial/<Start>d__6::System.IDisposable.Dispose()
extern void U3CStartU3Ed__6_System_IDisposable_Dispose_m6764E5851B11059239F33A67FC6EF60CBAF45173 (void);
// 0x00000185 System.Boolean Tutorial/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_mB652BD2218FCEB7ACBE84D8BC6B46D9940F834D6 (void);
// 0x00000186 System.Object Tutorial/<Start>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m172D54AF1F0E9B6B4EB7BE7FB34F0057A187F774 (void);
// 0x00000187 System.Void Tutorial/<Start>d__6::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m6BB0768629033FC61B5A2366DE6CE3629FE2B2FA (void);
// 0x00000188 System.Object Tutorial/<Start>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_mB79F0C091CA65C6C54D90599160BD8877212DDE1 (void);
// 0x00000189 System.Void Tutorial/<Tut_Jump>d__7::.ctor(System.Int32)
extern void U3CTut_JumpU3Ed__7__ctor_mD39C0787D1AB2549C4FD6AC1A3E461BF60FCACEA (void);
// 0x0000018A System.Void Tutorial/<Tut_Jump>d__7::System.IDisposable.Dispose()
extern void U3CTut_JumpU3Ed__7_System_IDisposable_Dispose_mA9ADE99C8F444B25CA576E9EA60B0B81189CD42B (void);
// 0x0000018B System.Boolean Tutorial/<Tut_Jump>d__7::MoveNext()
extern void U3CTut_JumpU3Ed__7_MoveNext_m7C8F9862BB038B294CC000094476B7F604614A72 (void);
// 0x0000018C System.Object Tutorial/<Tut_Jump>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTut_JumpU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E0C3B1EE46760F63982A0E73A4C1FFF3D32E032 (void);
// 0x0000018D System.Void Tutorial/<Tut_Jump>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTut_JumpU3Ed__7_System_Collections_IEnumerator_Reset_m01FA362561D670ED095938E719E5315615E20077 (void);
// 0x0000018E System.Object Tutorial/<Tut_Jump>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTut_JumpU3Ed__7_System_Collections_IEnumerator_get_Current_mC18D9511F425AA47D26120219EC5F0407C331D6B (void);
// 0x0000018F System.Void Tutorial/<Tut_Boost>d__8::.ctor(System.Int32)
extern void U3CTut_BoostU3Ed__8__ctor_mF81ECB0514A06FCBD82614ACF1480A3DC52575DB (void);
// 0x00000190 System.Void Tutorial/<Tut_Boost>d__8::System.IDisposable.Dispose()
extern void U3CTut_BoostU3Ed__8_System_IDisposable_Dispose_m3CCCC47F56C961CCA6DD7DA6CE6375E8010DEB0C (void);
// 0x00000191 System.Boolean Tutorial/<Tut_Boost>d__8::MoveNext()
extern void U3CTut_BoostU3Ed__8_MoveNext_m583C5A5A5EEF4BE405E6524B75B2AFF5FEBEC2CF (void);
// 0x00000192 System.Object Tutorial/<Tut_Boost>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTut_BoostU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB207A75F57E4299B91D0D587E468999C45A53162 (void);
// 0x00000193 System.Void Tutorial/<Tut_Boost>d__8::System.Collections.IEnumerator.Reset()
extern void U3CTut_BoostU3Ed__8_System_Collections_IEnumerator_Reset_m12770DB896300573532DF134AA146B3663BE7DC8 (void);
// 0x00000194 System.Object Tutorial/<Tut_Boost>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CTut_BoostU3Ed__8_System_Collections_IEnumerator_get_Current_mEA021015F7C7E290BFB34DE1E9BB9C891407445D (void);
// 0x00000195 System.Void Tutorial/<Tut_Explode>d__9::.ctor(System.Int32)
extern void U3CTut_ExplodeU3Ed__9__ctor_m18E467A645CBC3DEDB86A21A68247609424AFE41 (void);
// 0x00000196 System.Void Tutorial/<Tut_Explode>d__9::System.IDisposable.Dispose()
extern void U3CTut_ExplodeU3Ed__9_System_IDisposable_Dispose_m975EB95E077E651E35E9663C2E8A3AC6DA876AC2 (void);
// 0x00000197 System.Boolean Tutorial/<Tut_Explode>d__9::MoveNext()
extern void U3CTut_ExplodeU3Ed__9_MoveNext_m4A6271A783AF0F9800E76BD0CBD32409965BDCB2 (void);
// 0x00000198 System.Object Tutorial/<Tut_Explode>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTut_ExplodeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46E89232831848ABF810BC2BAF5702D2A521EE3D (void);
// 0x00000199 System.Void Tutorial/<Tut_Explode>d__9::System.Collections.IEnumerator.Reset()
extern void U3CTut_ExplodeU3Ed__9_System_Collections_IEnumerator_Reset_mAE982EEC6377CCB571A7F06C245B1E685BEB149B (void);
// 0x0000019A System.Object Tutorial/<Tut_Explode>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CTut_ExplodeU3Ed__9_System_Collections_IEnumerator_get_Current_m1C1B730683CFDA089B8DB4D49C2E1827CAA5E32D (void);
// 0x0000019B System.Void Tutorial/<Tut_Practice>d__10::.ctor(System.Int32)
extern void U3CTut_PracticeU3Ed__10__ctor_mC3468E11ECCF0AC0E390C67756D43200D9567106 (void);
// 0x0000019C System.Void Tutorial/<Tut_Practice>d__10::System.IDisposable.Dispose()
extern void U3CTut_PracticeU3Ed__10_System_IDisposable_Dispose_m1F54A776717183BAA1A5645EFE61D33EA6D0940A (void);
// 0x0000019D System.Boolean Tutorial/<Tut_Practice>d__10::MoveNext()
extern void U3CTut_PracticeU3Ed__10_MoveNext_mFF7D6A3FAB4496550638570362B53F7609BD1572 (void);
// 0x0000019E System.Object Tutorial/<Tut_Practice>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTut_PracticeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1426E2C9A61D803F2515ED575BC67E07A558389F (void);
// 0x0000019F System.Void Tutorial/<Tut_Practice>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTut_PracticeU3Ed__10_System_Collections_IEnumerator_Reset_m2CDBDFD37AD47B2FC56B06DB5EED953967B1B7A2 (void);
// 0x000001A0 System.Object Tutorial/<Tut_Practice>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTut_PracticeU3Ed__10_System_Collections_IEnumerator_get_Current_mBEB2F2A6168E866ED63957856897E552CD37F182 (void);
// 0x000001A1 System.Void TutorialCamera/<ZoomCamera>d__14::.ctor(System.Int32)
extern void U3CZoomCameraU3Ed__14__ctor_mEBD3B8676175C2EAB4633C98233908304777E8ED (void);
// 0x000001A2 System.Void TutorialCamera/<ZoomCamera>d__14::System.IDisposable.Dispose()
extern void U3CZoomCameraU3Ed__14_System_IDisposable_Dispose_m66A7525BF8778B7CB07A9D027347B9BE9D31A21E (void);
// 0x000001A3 System.Boolean TutorialCamera/<ZoomCamera>d__14::MoveNext()
extern void U3CZoomCameraU3Ed__14_MoveNext_mB3483B827FB28C11BCC88A7EAE65D2D9E4D6BE62 (void);
// 0x000001A4 System.Object TutorialCamera/<ZoomCamera>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZoomCameraU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9A348AD948F348CEF2D158349BCA6C1B4C8E9FA (void);
// 0x000001A5 System.Void TutorialCamera/<ZoomCamera>d__14::System.Collections.IEnumerator.Reset()
extern void U3CZoomCameraU3Ed__14_System_Collections_IEnumerator_Reset_m9A908A9B51215ABA388376F2C078F29FC9C3C3DC (void);
// 0x000001A6 System.Object TutorialCamera/<ZoomCamera>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CZoomCameraU3Ed__14_System_Collections_IEnumerator_get_Current_mBFB9959811A8007298254CC5687480124657358D (void);
// 0x000001A7 System.Void Upgrades/<>c::.cctor()
extern void U3CU3Ec__cctor_m49BF333FA13443238FEFD2E849B6169B0DB79B1D (void);
// 0x000001A8 System.Void Upgrades/<>c::.ctor()
extern void U3CU3Ec__ctor_mE3DE0DEB66DE83A30D1E8CF1C40E2586DB0894FB (void);
// 0x000001A9 System.Void Upgrades/<>c::<Init>b__4_0(Upgrade)
extern void U3CU3Ec_U3CInitU3Eb__4_0_m4A03D7E42891CC89903DA4DD112FA48ED2BDCCDE (void);
// 0x000001AA System.Void Upgrades/<>c::<Dispose>b__5_0(Upgrade)
extern void U3CU3Ec_U3CDisposeU3Eb__5_0_m2FC4986EF38F7F30569FFB50AE63E27868CA92AE (void);
// 0x000001AB System.Void SwipeDetector/<>c::.cctor()
extern void U3CU3Ec__cctor_m374A83CE16AE11E127D8A869A3B7271983D2494D (void);
// 0x000001AC System.Void SwipeDetector/<>c::.ctor()
extern void U3CU3Ec__ctor_m2E9E3F6A1BA3E29377A0D89AF411B199C7CD8A47 (void);
// 0x000001AD System.Void SwipeDetector/<>c::<Awake>b__9_0(SwipeData)
extern void U3CU3Ec_U3CAwakeU3Eb__9_0_mD9619314793F9C42C4886B4D6C4BFD3D05A35CBE (void);
// 0x000001AE System.Void SwipeDetector/<>c::<.cctor>b__18_0(SwipeData)
extern void U3CU3Ec_U3C_cctorU3Eb__18_0_m00910BE03F489E8E64A5238B72B0F3E6C8153EC0 (void);
// 0x000001AF System.Void VibrationExample/<>c::.cctor()
extern void U3CU3Ec__cctor_m9F2C55C9D1E69A6EBC7431794D674FE9E313FEF0 (void);
// 0x000001B0 System.Void VibrationExample/<>c::.ctor()
extern void U3CU3Ec__ctor_m8136DE0AA0459F419FD1A7D801A066079204984B (void);
// 0x000001B1 System.Int64 VibrationExample/<>c::<TapVibratePattern>b__7_0(System.Char)
extern void U3CU3Ec_U3CTapVibratePatternU3Eb__7_0_mB27A3BA39CABD41D01E50A1E6A6AD76DCDD69881 (void);
static Il2CppMethodPointer s_methodPointers[433] = 
{
	AdManager_Awake_m23660705BCDFB8A358226BF4701EB28031B62E57,
	AdManager_Start_m7441282F4D11035BC4FA1D580553465DD9A85700,
	AdManager_ShowVideoAd_mF570DAD340FD93AC8D34CC31FB879F0920BC08DE,
	AdManager_ShowBannerAd_m25552025306D26698AB1A0139897715CCE2F467D,
	AdManager_HideBannerAd_m80F7DDC674F742D8DF24A9175E1F31DBCC7C2B85,
	AdManager_ShowVideoWhenReady_mAB6EB29D956C7A4948933149B96666936EDEE9D4,
	AdManager_ShowBannerWhenReady_m44DB37C15766190A506F4FBF3A31CAD0634A4F33,
	AdManager_ShowRewardVideo_mD03DD0DDF0EC57C86FBE44405B8C786C2D67ED6E,
	AdManager_ShowSkipableVideo_mB56B41E0FA099E0B987E84F52B36DC8C9C7D936B,
	AdManager_OnUnityAdsDidFinish_m6FD703F7881BE4A1B281B3693A65DF8C86E3BEE6,
	AdManager_OnUnityAdsReady_m11DE56EA9248B9F61E77CFD49F565AC9A8A1A725,
	AdManager_OnUnityAdsDidError_mBA37A489E9DCC40E679FD2B1C757F2D9C5655ACC,
	AdManager_OnUnityAdsDidStart_m0110D7C0B5F451603085F927C50EA9DDC8149454,
	AdManager_DetermineIfAdRequired_mF13D5BB47214A22751A0E8AC0F90499C2431A5C9,
	AdManager__ctor_m90E4F3ECE5164648C36A623707AF2B9F67A9C6E6,
	AdManager__cctor_m4DFC56B7E5F3E5582B1DAE04280A04606712C903,
	AdManager_U3CShowVideoWhenReadyU3Eb__12_0_m1E2BC1E0CFCA10A98463B7A17C68033ED6E4757A,
	AdManager_U3CShowBannerWhenReadyU3Eb__13_0_mF475F81D498A73C074630803D03B503019A754AE,
	AdManager_U3CShowRewardVideoU3Eb__14_0_mDD23B6010B4B75D017C19A37650D7AC20B3D6D78,
	AdManager_U3CShowRewardVideoU3Eb__14_1_m40220AB1F9F5737D38DCB1443D82A4E75C5DEA6B,
	AdManager_U3CShowSkipableVideoU3Eb__15_0_m8562CAC9BA1995F5B5A1B317255D06A2FD4E8442,
	AdManager_U3CShowSkipableVideoU3Eb__15_1_m1587D67F5B0B83F3CA4030AEC2F7BA5200018A66,
	CameraFollow_Awake_mDEEAFC7E875D37498563B8045AEEC8B948A74BAD,
	CameraFollow_Start_mB2BDD148E8E2037AE10A4B479CFDD3477E8F8FCA,
	CameraFollow_UpdatePosition_m2905CA042449E8C8A25D15560005F604FF8A72F3,
	CameraFollow_Update_m7DD0DB22B0A37A8E6D882095C8FD3A597CF07D93,
	CameraFollow__ctor_m511C23D2A1559F967CA471B8E1E33CCB9CEDBF3E,
	CameraFollow__cctor_m343CA8142E0CB14FE520A6F2BD91C16B1120EFD0,
	ScrollBackground_Update_mE99D99086BB59D197AD10D7A330DFCD7702B88F2,
	ScrollBackground__ctor_m2686390B5F67D92130F4E3CD9C14E1DC5D215EBC,
	Global__ctor_mE88BB1E563125731AFAF9A6D0670472EC2DE219B,
	MenuManager_Start_m3488752248C062EB8FFA7B9E279B96F7F9882E97,
	MenuManager_StartLevel_m464210AEFE9A76D81BC80F4754D6EA3282CA17AD,
	MenuManager_FadeOutUI_m3785CA328E92BFAC071CDEE1409C064178E4C520,
	MenuManager_OnPlayButton_m398B2D88D092EC8A24D3A843C13723F88688922C,
	MenuManager_OnTutorialButton_mE0CC23B609E83F6EC30A75DA3BCF66FBB5158EBC,
	MenuManager__ctor_mC328EA95D8565E70AAEDA542072A25B4EFB7ECED,
	ScoreKeeper_Awake_m582C6B5EF288CE019ADDE183E5F82E6BF29401A8,
	ScoreKeeper_Start_m3F2568722F6BE86E236EA2F3EC6616083C254B09,
	ScoreKeeper_IncrementScore_mD8588127D30983BA1DFC2280E06398B6984488FC,
	ScoreKeeper_CheckHighScore_mED24FEDF03E673C3D876F5F21B7F7B64872B874F,
	ScoreKeeper__ctor_mAB060FA16AC4FB1EA2A8512271B3E35B9FB13549,
	SpriteAnimation_Start_m20A08E457A626A6844D96C83FA20FA83D109432F,
	SpriteAnimation_Play_m87833C545FB12571166E0F21CE05A2EF75E369DF,
	SpriteAnimation_PlayEnum_m8E0504F15977C07478DFF1F57883C8171972027F,
	SpriteAnimation__ctor_m0402471B79214F0578E98DF27FB2CC97A732CA5B,
	SpriteAnimationClip__ctor_mC70DF943BEAD2B7DB818A799893650237B55730B,
	SpriteAnimator__ctor_m5DB4A7A81BBADE389F1A633AFB75E7EC82DC23DD,
	FlyByMeteor_Start_mF178FB797A28AE6A289A360AC5864AADDC30F72A,
	FlyByMeteor_Update_mAA8F09905210EA00AAEE8CE7D26AEDFC695D5429,
	FlyByMeteor__ctor_m3D74A479994810BDDD8B19CDFDD40AD83D815CB4,
	Meteor_Start_m14AA0D16FEC249A824E9758B3A3F6AA472C8DC7D,
	Meteor_Update_m44859EA973A09EB6DBDCF7071288F81D589E2DC5,
	Meteor_CheckHitPlanet_mE77A8C780A4DFFDD6E51AA6D4783F11E67511F4A,
	Meteor_OnHitPlanet_m005574E6034F6F5C37AB3B8F8F85F96D159445A7,
	Meteor_HitPlanetEffect_mE84182E8047D338E2F013F60A5A5762A56877BC3,
	Meteor_ToVector2_m5060E58584CBB4F99686062FB1544E7CAE781186,
	Meteor__ctor_m6B4E3772F26133D5833915158631769EC1697B1A,
	MeteorSpawner_Start_mDE3F99B27A157C5D9CA700C73FEFE926F877D666,
	MeteorSpawner_Update_m7AF0FFD1E3C7D2DAC4D112042A9D4BB1D1F98B43,
	MeteorSpawner_GetMeteorTarget_m2C3A5506EC6C5AFD39E4872A722CA086F7F8E1BC,
	MeteorSpawner_ScoreIncremented_mE21EE310145D1843750ADF8B76C998DF9C63454A,
	MeteorSpawner_QuickSpawnFlyBy_m4AF6038E2490B8F50A1F8A6742194434CAA7BC25,
	MeteorSpawner_SpawnFlyBy_m9E749FC615F774DEFB8499253A2961998CE3F271,
	MeteorSpawner__ctor_mB655D3E5F9B2D84FCA2FE61594449E942C1B6905,
	Obstacle_Hit_m88ECB453EDB91F6E91725B19347D4811AE2890E5,
	Obstacle_HitCoroutine_m4E31D0ACC9ADF9BD7DD3536CBF3A9AA1B91CF72E,
	Obstacle__ctor_m2A721D4A6D3CB0FB812FC38AFCAE3E86AB2EB0F3,
	PlanetDestructor_Kill_m83B431D92B99C0BF9A4127210270AFBCD8E0836E,
	PlanetDestructor_DieCoroutine_mD166E330F13B7F18EBC55152F991505D3E7D7A9B,
	PlanetDestructor_Explode_mFE381D9AB9D52F6532151A5CA9D4E8D2CA261B7D,
	PlanetDestructor_StopDeath_m6409C00EBF8CDAE2F41B960B62E57AC4769F0067,
	PlanetDestructor__ctor_mB3C9E3C1513A03B7B30B6CE65140C2EC2EC6A6A7,
	ObstacleGenerator_Generate_mE13F9F57093A77D3E2241D145B41022252458AEC,
	ObstacleGenerator_SpawnObstacle_m9BCC1C87FD29973759EF59B08AC44C9E7FE62AD1,
	ObstacleGenerator_SpawnMeteorTarget_mA0D29114CF72D4F54B908A84E986563240329F2B,
	ObstacleGenerator__ctor_m84BD07C44EC80AA2610E4899A7F9928ECCE43F84,
	PlanetGenerator_Awake_m1BE745D32387C09B243904AB55E9F4443388350C,
	PlanetGenerator_SpawnPlanet_mC86A811CCDBE6A09D1AEBCB2D7F68DEA6064E44E,
	PlanetGenerator_RemovePlanet_mC57579688890A67278F661D7091744D60BA97AD1,
	PlanetGenerator_GetNearestPlanet_mD93401C2A5DC804712F83A45888CB18E34243F70,
	PlanetGenerator__ctor_m646CA471F268304549A0FA10C70FF61E510D86C0,
	PlanetPicker_Start_m5F0592F2C63A6E301274A1EED3FD92C80E13ADE0,
	PlanetPicker_Generate_m4CAFEA2D337AB18BD23648935C477D1FDA483F99,
	PlanetPicker__ctor_m472E889E2D8A1F87FF4C2992E16764154EB37840,
	PlanetSetup_Start_m54E478A3892DB2EACEAE9C52B06E95620BC6AE1D,
	PlanetSetup__ctor_m4302BE3DBD72BA60D9CBFB76EF7FB5D9A6F915B9,
	Effects_Awake_m8AAD15DE14CEBE13DD66CF4B2A7D79961B0337A2,
	Effects_QuickInjectPlanet_m1C070DE86EB18734DDED7AAFDE5B63672EA2DC21,
	Effects_InjectPlanet_m349697C7F029D6EF2BE670B68CDA189898D0B33B,
	Effects_SimpleInjectPlanet_mA75A54042CA01530965A5C076920270BB37DA11A,
	Effects_AnimatedInjectPlanet_m27B98B2A0A8B008E7858C1BB6EA2A3C6C2CDA065,
	Effects_ShakeCameraVerySmall_m9CA0F91D0FCA362581C7789A8099DA6C9469DAA8,
	Effects_ShakeCameraSmall_mB10417515ED710D154DA00079BCA0F8FC858619D,
	Effects_ShakeCameraMedium_m4EF2BED0F4E8A1C9550D1371A4A09ABD25BCEFCC,
	Effects_ShakeCameraBig_mC2F2148E20E0C666BA5A5E91E652CAF9D50A8689,
	Effects_FlyLandOnPlanetEffect_m26F3287005C66CD0CE97D4BF115C6E2668D78335,
	Effects_JumpLandOnPlanetEffect_m3BA96EC90CEB4A6943318E6C22448798EB0C2628,
	Effects_JumpEffect_m0EB96ECE3E2F40A0E9199FF3F8074BB69C1289AB,
	Effects_FlyEffect_m84C2B938781B0CB9C5C991F530FB1B53E1C76F41,
	Effects_BoostInAirEffect_mA43A756D916170C7448464BFF589FA19EE05A7BB,
	Effects_DieEffect_mF499F72133124277C338A767F4A5D5E30816F06B,
	Effects_ReviveEffect_m253AA6ABB23D1D1106436C7EFFEBC20ACA5C4E15,
	Effects_ReviveEffectCoroutine_m38A2A361F4B7599CE8DB3C359C19882ACA96463C,
	Effects_ToVector2_m7C62F47B83A9511304993F054121420A2D27ED6F,
	Effects__ctor_m683B62B9C966DE89F31DE842DEE1A5DA2A5849E7,
	Health_Start_m4081E3B6C29AF6A976DE7C096AC28296D0C61E1A,
	Health_Update_mF072C7DA028FD78FE00F308522349F908CB3942A,
	Health_OnRunning_mE93841582AAAE53EB91CEED5CB90A760D344EA3E,
	Health_OnFlying_mC9F128A0AB908079854843C957C898C94B7847CA,
	Health_OnFadeOut_m683AEF5A8C09CE8FD237D1B2BEF858D6DBE559D2,
	Health_OnFadeIn_m9162F2E1FE223E0C4D911D1FAF0852AEF0008E4F,
	Health_OnTriggerEnter2D_m7B4A21320200CA283D587EBC8F884AC16A50AB11,
	Health_Die_m0B7272E045337982C3779E9725D4D36EA51D8500,
	Health_DieCoroutine_m7802D78E90F05577E151C058DC86EAE9D3F8625F,
	Health_WatchVideoAd_mC5ABD79841EFB47DFCC27EF4C8B1A1C79F65D880,
	Health_ResetScene_mC8C6BB27862666E5CD11F4F961FA594B727E0DB0,
	Health_ReloadScene_mD39D9E54958888375E5FFB0B24E2ECD9CFB4A723,
	Health_Revive_m7FABF5096D26431A79AAA46073CC2AFF6C435872,
	Health_ResetPosition_m247FBAED1EDD825306EF156D28FA98EF82AE0EC5,
	Health_OnWatchAdButton_m091E46F82103E793413EE77D112239F3679B0878,
	Health_OnReloadButton_m810DB22DBAC56B65522AF56EBF4C90D186222302,
	Health_OnReplayTutorialButton_mF440EE30EA2B19A28152E8AF069FC27A6999561C,
	Health_OnFly_m3D38C07527A558C40D8A5D985AE1B39A8A836696,
	Health__ctor_m453CFD175281721E22D995E6C4930DD2D092759C,
	Move_get_moveState_m16B3C71EB138F9F5AC823992D4DA3C25F0FD174E,
	Move_set_moveState_m6108E5E15C3CE82A5B03DA58213F4F273A6FD6A1,
	Move_Awake_m03FC74073BD0D7670842B7E6118DED673E612F62,
	Move_Start_m429C3F7FC1D126B379F321BBF2181F1BE1190A15,
	Move_Update_mF39A91C7E7EE81357653523B731F1EE0C4F3EE8C,
	Move_OnRunning_m3D9CE254D96220606FCE11BDD29550260A504435,
	Move_OnJumping_m18EAAAA1E3C43599F45AD8C2BC3CA3C523541ED2,
	Move_CheckGroundedOnPlanet_mE424B165D5B77AFFEE3A9CC1931246BE146C2A20,
	Move_OnFlying_mEC3BCE3CF7F5B078FA8FFC4DA5B5B317424F76B4,
	Move_CheckHitPlanet_m15616C4ED94762D7A82740D5400FC32DAE2DCA23,
	Move_UpdateRunDirection_m0B87B93E578912E764850A480EB357DE9EA126C7,
	Move_LandOnPlanet_mC916D6E6F85834FB3C408058E4F01A3091E2A6F6,
	Move_SetCurrentPlanet_m1F1F5D38965DDF650D7326AD5DA2D4129F23ACC9,
	Move_UpdateUpDirection_mC2BA127749A32CFBC0DE89F4763B949B25C739AC,
	Move_RotateAroundCurrentPlanet_m5ED0B3CE9FC4AEA6D95A88B00C3DCBBE4609095C,
	Move_KillPlanet_mF6C7D784ABA10F50985268C83DF2B6BBD08D6600,
	Move_HurtPlanet_m18C3CF9BA1F0AF978DCA5E598C2871B781291958,
	Move_OnJumpButton_mC90AC996BDC4F8403AFD0EB41889C9DF330814AD,
	Move_Jump_m0F503C76D8F3498E70592E96F167CFC5BBEE8488,
	Move_Fly_m12BFDDE12FBE9D58D77245D5D9DE80A8791824EA,
	Move_OnSwipe_m7D1146FC1BA3EE86BC5BA969705BD2981410675B,
	Move_ToVector2_mAB826BABBEB7954E7BCDA02AE77F26314DC2F02D,
	Move__ctor_mE26369B6EE18DAEBE5F11E093BECEB0A89FF3707,
	MoveStateEvent__ctor_m6575701FD24D6117811963318F079D6297993A28,
	DialogManager_Awake_mD95596C67086C341E1656F225367BD3A975A2E32,
	DialogManager_ShowDialog_m407D070C380D9EEC7A7684BF489E25D1388A00ED,
	DialogManager_FadeIn_m137584C6DD2F8F81C182C7D6F87048950A63E32F,
	DialogManager_FadeOut_mD9EC28EA91343643D86C521A66A9608314998288,
	DialogManager__ctor_m1D0F5CE9C6E3341EEFAB534C82658840798F9165,
	Tutorial_Start_m86A43FDD735D975B6D9DB7FC1564262E398C3275,
	Tutorial_Tut_Jump_m395168CF18B649C6492255E251B766C4292C6A0B,
	Tutorial_Tut_Boost_mB4EF956CA50540B45D73B391C9D95F6CFB3CBC50,
	Tutorial_Tut_Explode_m4A2F0BF3BD1EE09AD223B792941510853F4E3BC3,
	Tutorial_Tut_Practice_m55A8E422A9B39D7D2C79CDF4185653A44FE46781,
	Tutorial_LandOnPlanet_m5FEA5D19935A4C29332D93230EEDAD2D95BECD21,
	Tutorial_Trigger_m4CF438200DE93AAC3E08E9144E52572E7A0E3D74,
	Tutorial_OnTap_mCE12B53B9C0AD989E13A81713EDA469C39365493,
	Tutorial_OnSwipe_m52B977BF03B9437BC283B194E303C7CFE1A55392,
	Tutorial__ctor_m68D89ED1D7DA006C0BEABDAB7681CF3F323C7ED8,
	Tutorial_U3CTut_JumpU3Eb__7_0_m23463F0E3F9A4D3BCB2EFE188AC0CD6E27882B24,
	Tutorial_U3CTut_BoostU3Eb__8_0_mBFD82A0AB229927110D6B69B448B67D9D130E732,
	Tutorial_U3CTut_BoostU3Eb__8_1_m9E9FF2B4FCA04914DB9A8D3E8C3C36C33900A38A,
	Tutorial_U3CTut_ExplodeU3Eb__9_0_mEF9EE462297F3E0F97BDD6C604B9E8005D6F26D5,
	Tutorial_U3CTut_ExplodeU3Eb__9_1_m926DCD4E9EEEAA6CAD4CAB9C128B754DB192F153,
	TutorialCamera_Awake_mEACD12015039D4B7B4052EA2E8F19B0323CB422C,
	TutorialCamera_LateUpdate_m764FAFB8B47963F532EDF9DCF09F4BF4C69F8721,
	TutorialCamera_ZoomCamera_m7C0148E75F008112B6BCA2E80F925921BF7BE06E,
	TutorialCamera__ctor_m1BAF6A06E608855C8E74B332B5C31399039559DB,
	TutorialHealth_Start_m9442304A1762C685832DE63D528C1A74EF47115A,
	TutorialHealth_Die_mAE3AD2B5B9D6483D8213B2A7A06D2096CAE5BA2A,
	TutorialHealth_OnRetryButton_mC7E3F91A6FAAA4A501BB3E81553E91245DBBC78A,
	TutorialHealth_OnReloadButton_mE0D5E2341B3F0C9FD4E747C7EDFE14118573EADA,
	TutorialHealth__ctor_m3BA7E56260CCC08765C859D7E8AC1338CA6D7C99,
	TutorialPlayer_OnTriggerEnter2D_m04A3B14C0253B06EDC52F46DECF6D0D417D9A240,
	TutorialPlayer__ctor_mA2B20CDE111EFC4B67CF494549D7457B8795C100,
	Upgrade_Init_mE9F024E838C6C212A61C73B0D9021C486AC012FD,
	Upgrade_Dispose_mFF9B28ADC2EDB950071DA8D57E228659B53686D3,
	Upgrade_UpdateUI_mFE24356DE688E125DE55EB26E9606E74DF37D56A,
	Upgrade_Complete_m340FC426661517936282BA7632E2E7DEA8944A2C,
	Upgrade__ctor_mCA177A1FB25C5D32C6D1EEE486134661470B9663,
	ScoreUpgrade_Init_mD2D0C13C97789130E850CFE0C30DA0446A7992A3,
	ScoreUpgrade_Increment_m297D5C41DB49652E02A53EBF5D7919C0C54EC19F,
	ScoreUpgrade__ctor_m8E0DDCF9144A2C2EA109277D48E26F0DD0B69B3E,
	HighScoreUpgrade_Init_m831A9FE403331D6C1085E8F8E0105340818CA77B,
	HighScoreUpgrade_Update_m39E75DF45EB2604278658D70E6A6C18B54FB6947,
	HighScoreUpgrade__ctor_m3CCD37A114D8B31BC2F43E7179C172DE450767D8,
	DestroyObstacleUpgrade_Init_m531408F3A3BB944143B6D2393689056A5B3C4877,
	DestroyObstacleUpgrade_Increment_m3F6A14D968E8263B792F5EC67E8526013EA1AE46,
	DestroyObstacleUpgrade__ctor_m84176CF1B8BDB0DDE41FC16523DF91038CEB4E77,
	RunDistanceUpgrade_Init_mCE34EC8E068EFA6EEB0DE0F12636DFD0C7C20189,
	RunDistanceUpgrade_StateChanged_m2805BAC3D349047BD5DA17BAAF6DFEB0EF837605,
	RunDistanceUpgrade_Update_m650BEA68548394AB777AAF20FE9BDEFAC53AD5F2,
	RunDistanceUpgrade__ctor_mEADD663E6DF0047531BB5E2E34BDE777D68395BD,
	FlyDistanceUpgrade_Init_m5574E0929AA9FAC9499A967C92335210DC69FE3F,
	FlyDistanceUpgrade_StateChanged_mF6165E75F6F190787B223B638957DC778E466EC7,
	FlyDistanceUpgrade_Update_mF57C7C8631D618933D27E3FC288AA864E1888D0A,
	FlyDistanceUpgrade__ctor_mA18B2E93F6F5721C7A233F5D5B22731F598158DB,
	JumpUpgrade_Init_mEDCF4BE1305DC62DAB7E80BB327A3EF3D43016A2,
	JumpUpgrade_StateChanged_mC51C161E4F2057B6E07C8013D02CADC5A3EE99A1,
	JumpUpgrade__ctor_m0B70535E412B1BE6799AD4F58CBFCFDCC580141D,
	Upgrades_Start_mF56C88C194FA1735DD6896DAEFCB6E71BB37BB1E,
	Upgrades_Init_mA9C6A90A47B734429D631165CB6E578A379EDB5F,
	Upgrades_Dispose_m13F634843EE2BDA3478230C79BF3330A09F8ACBF,
	Upgrades__ctor_m2085AE54DF0FFBAB33AC516F252A589AC63460C9,
	Upgrades__cctor_mE10B08E1BFD14253C579211CC00BAFC2D869DB48,
	SwipeDetector_add_OnSwipe_mC7504374613C2BCDAB66E2C8A0604419284399EF,
	SwipeDetector_remove_OnSwipe_mEB1C8B52C04B4035AF43B355816798428A83D69C,
	SwipeDetector_Awake_m667DA64654B1B61C3DE6E88602BF68DBA9F77D05,
	SwipeDetector_Update_mA095F65D05FE0B2FC84E88D00DCE273D74A13BEC,
	SwipeDetector_DetectSwipe_mB180D3B7319D0ABA8018FFD241DE61E0BBEF6A04,
	SwipeDetector_IsVerticalSwipe_m4A18894A11C10AFE8B90BE5C46A72101ED0A5550,
	SwipeDetector_SwipeDistanceCheckMet_mA7B50D8242EF75DE6BA87CE8D79680B6696F6391,
	SwipeDetector_VerticalMovementDistance_m14D16164E406E9F36DE81D218A722E6BCD30BCA4,
	SwipeDetector_HorizontalMovementDistance_mC3C2108EDD8B5C6CECD862EB6A6BA4AED318C7B8,
	SwipeDetector_SendSwipe_m0CCD80C1CB0244DB9ED3DC8E441FEDD2FBA7D5C8,
	SwipeDetector__ctor_m2411490382B3EAD042053E6843814D951CE9D535,
	SwipeDetector__cctor_m9FC815266974DB68A2AF5E734193FD3C8774AEC2,
	SwipeDrawer_Awake_mAAFCDB0019A32F7EF383B8476EA20DCA87A988E3,
	SwipeDrawer_SwipeDetector_OnSwipe_m99756C14E37FC26D3549A4FABFDA429FFDAEFE44,
	SwipeDrawer__ctor_mEE3B2042133A3DE219A2D53F60C194799D91EC6F,
	SwipeLogger_Awake_m9E3FF13AEF09A6C9EF5D5A31D51D06D5B05E0F58,
	SwipeLogger_SwipeDetector_OnSwipe_mEC318FFD810E51493952FC5824F8295306AD0FA5,
	SwipeLogger__ctor_m40B0353714BD1354E361B1A2679C0E4FD14F8C3E,
	VibrationExample_Start_m3DAC7A15FAE7DE2C4A060097C7DE611D7F982CF1,
	VibrationExample_Update_m097A5DAB7D2813DAE8B9F4FADDF937CC2E2B8898,
	VibrationExample_TapVibrate_mFC1CCA8028D81DB0ABDA9F03880F3E1EEF575808,
	VibrationExample_TapVibrateCustom_m9449632FF066CF40A7534427E743900F810562BA,
	VibrationExample_TapVibratePattern_m7BE781FA1658AC2087D3EC16E9DA8EE948FDD9B0,
	VibrationExample_TapCancelVibrate_m4F010407AB216C99EDAA12996BF62F125DA1D750,
	VibrationExample_TapPopVibrate_mD38944AA2CB17074F44EE4C36B9CDCA9C3E3F507,
	VibrationExample_TapPeekVibrate_m71F93101E937529DD73517BB35F3A3C01FBAE9A8,
	VibrationExample_TapNopeVibrate_mF53458C0F1B6616EF5AD719634E31B0AE3E356BE,
	VibrationExample__ctor_m404E55A0D0417C6E6221F55F69398A65D28F6DF1,
	Vibration__HasVibrator_mF5D80E6C6D4ED65EA31AF53F37239422DB2AD61A,
	Vibration__Vibrate_m3F16161D6FB468D91AB64E87B3128BA43CCCFE08,
	Vibration__VibratePop_m2DE0606500D6C6E83A7262C9E951BA2CF4AD37DC,
	Vibration__VibratePeek_mA41E2F4A7858DBDD4DAFD194D407E8D35E405323,
	Vibration__VibrateNope_mF571C84C03060C5EA2270E855DE9D7A0BA1832C5,
	Vibration_VibratePop_mFBC32B5F33BE00437A52116FB73EE8B1B89B6C90,
	Vibration_VibratePeek_m8CF7BE3C8DE43C0868EB499F2F79C0F080BF831D,
	Vibration_VibrateNope_m2759889AFA07070CD7D11B36A2E1C89439A7869E,
	Vibration_Vibrate_m507B42BA9A94C2A816AACEF43D4F915AA7EB6BEF,
	Vibration_Vibrate_m7F7AA2EC5D51C3695AD7F74ED7E57DE7EC6E5601,
	Vibration_Cancel_m03501DBD76CFBE60752FCBF522232F7C31D68371,
	Vibration_HasVibrator_mAF5A512BCA28DE44876091B362B716A6FA76B4AD,
	Vibration_Vibrate_mB24FD6BCCA46120E8BC9E2B5A0AC07377A975F66,
	CameraShakeInstance__ctor_mFC5596C19A153DE6BF6D0D3307C2C0C23C28E2DE,
	CameraShakeInstance__ctor_m6F5BD948021AB46E0C20D9C0844460F08F682050,
	CameraShakeInstance_UpdateShake_mCC4D1DA6A345DF8A451FD37D6602D64CB3B8BC92,
	CameraShakeInstance_StartFadeOut_m34CBCB84CD38E30CCEE6EC1F4DD17D767D354BDD,
	CameraShakeInstance_StartFadeIn_mEFA63502BF4BF7F7F7F3BB16ABE84ABA32BC65A0,
	CameraShakeInstance_get_ScaleRoughness_m448D2601DDF4634663D3E79116067A76885A0391,
	CameraShakeInstance_set_ScaleRoughness_m78D7353FC70E7A80ED8CDA52109A37120F5AD157,
	CameraShakeInstance_get_ScaleMagnitude_mBB9BCE67D634BDB5FFD991D0BCBC431F4AAE59D9,
	CameraShakeInstance_set_ScaleMagnitude_mC98C0F1A4B21D68B819950908F3CDB9D8F597C66,
	CameraShakeInstance_get_NormalizedFadeTime_m193EFA194C495DC175A25653FF05A4BBFE1098AF,
	CameraShakeInstance_get_IsShaking_m08CF1FBD03F7A48FF965121F188BCD4A436CDE5C,
	CameraShakeInstance_get_IsFadingOut_m2F735BB85CF48A5814F1A2BE5C4838CD49723670,
	CameraShakeInstance_get_IsFadingIn_m92D5619B0A3746B054EE8AA7200EECE7FF528CE7,
	CameraShakeInstance_get_CurrentState_mD96B0436F64A34F09C14B43FF5F902BF25E21FB6,
	CameraShakePresets_get_Bump_m490EC11DED2B8B797F7FE6160C5DB116C5FBCCE4,
	CameraShakePresets_get_Explosion_m1F4B3F311132B3B1BCE5603523CE6B276B69094B,
	CameraShakePresets_get_Earthquake_mE5DD52B6204F0B1D78AFD3A260BD41ECAB750A3B,
	CameraShakePresets_get_BadTrip_mF78A89A7F0325A4120B4ED1C5812FE00E228F6E8,
	CameraShakePresets_get_HandheldCamera_m7DE7C2BD89100B7D56BB3650B892F52A0503B4B6,
	CameraShakePresets_get_Vibration_mB71BD095DB1796312CFCE06B0B4B46E3DB970C97,
	CameraShakePresets_get_RoughDriving_m1DA39EFEBBC40D5EF3C45210BC18E750108A513C,
	CameraShaker_Awake_mCB503790AE714332E2A4BBCCC8DB9D804646947B,
	CameraShaker_Update_m8C390AF57DA49A52F9C3F7D06CC615BD8DA1ACEF,
	CameraShaker_GetInstance_m55861D8421901E79FA1603E60911B0991EA55478,
	CameraShaker_Shake_m225F6E34DA20D1B94B78EBEF9588351DC815F4B1,
	CameraShaker_ShakeOnce_m4375B86141F295BF0A0D6F2A905F7CDDAC9F7773,
	CameraShaker_ShakeOnce_mA4171FA0972A828E82A86530F38338B93C2D3A9B,
	CameraShaker_StartShake_mB8629FFD4D4FA5B53A51669F53B88D353C3E575F,
	CameraShaker_StartShake_mF25DED37988031A60BCD1B6C219D1BD9A2D97B71,
	CameraShaker_get_ShakeInstances_m2B2986F453E6F17E6F5AC9CD2CFFE5CBD7F95EAE,
	CameraShaker_OnDestroy_m7043F40F1928B2D4840FA91388FE534C07946B37,
	CameraShaker__ctor_mB068A2B5829D8F40BEB22E317CFE65F225B4D5D4,
	CameraShaker__cctor_m5B153C9D59A5DAEDC164F8AD643DD8895C9AAD7B,
	CameraUtilities_SmoothDampEuler_m263B709C1D8A1B7DF206F990BB9173AEADB2CD69,
	CameraUtilities_MultiplyVectors_m6CC4653828412E5C92FC41591C277866C9AC21FC,
	CameraUtilities__ctor_m85D12A01645E4857A57295FA4971A2EFCB450AF2,
	U3CShowVideoWhenReadyU3Ed__12__ctor_m98A7031866E979B71B2591C35D7F73A04A093660,
	U3CShowVideoWhenReadyU3Ed__12_System_IDisposable_Dispose_mAC1F795668349D2EE5B0B95E71237596FD78468C,
	U3CShowVideoWhenReadyU3Ed__12_MoveNext_m7AD3164E7538178A1B35835E7C27B9FA17327558,
	U3CShowVideoWhenReadyU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35332681EC51C6A346A85AF9C6DA1AEE2DE8B29B,
	U3CShowVideoWhenReadyU3Ed__12_System_Collections_IEnumerator_Reset_m6D6CBCA897A40846DCD1A908214B508A74CA6314,
	U3CShowVideoWhenReadyU3Ed__12_System_Collections_IEnumerator_get_Current_mD2D27C6DC724F16A54304AA8CBAD4D0EBAE17511,
	U3CShowBannerWhenReadyU3Ed__13__ctor_mC99A5542C543C6FD4673DC2BD585834E1242C158,
	U3CShowBannerWhenReadyU3Ed__13_System_IDisposable_Dispose_m59F972CEF5FA372D1FD0F8FABA32D422D984F145,
	U3CShowBannerWhenReadyU3Ed__13_MoveNext_mEE131F0C37E7D3842D7783EBF7E2B4C9C6993EAA,
	U3CShowBannerWhenReadyU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m761C40B24422D905858B79C85633B976F332E47D,
	U3CShowBannerWhenReadyU3Ed__13_System_Collections_IEnumerator_Reset_mD1E6D86064C47FE7F099612280F97D31D3FFF6F9,
	U3CShowBannerWhenReadyU3Ed__13_System_Collections_IEnumerator_get_Current_m6F1E151A3289CCB1785FA06A605675ADB0F81A25,
	U3CShowRewardVideoU3Ed__14__ctor_m90D906E77791EB707D2C6BC4BAC4C0D731505B7F,
	U3CShowRewardVideoU3Ed__14_System_IDisposable_Dispose_m8F203E5F3519EFCC36BCB2573497BB6E8F6CF75C,
	U3CShowRewardVideoU3Ed__14_MoveNext_mB2FC0172AB15A1B197C8D8668DCE23D5DA483712,
	U3CShowRewardVideoU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m935432429FC6BC2877A85D36C7432F2E2D62EED8,
	U3CShowRewardVideoU3Ed__14_System_Collections_IEnumerator_Reset_m122E3E9868F6D40EB59A49F08022DB7B3AD3F934,
	U3CShowRewardVideoU3Ed__14_System_Collections_IEnumerator_get_Current_mBB2349ECF4F8F75D08751DDFA0DF3E074D3CC514,
	U3CShowSkipableVideoU3Ed__15__ctor_m09F4507AAB728DE7871C6E8E4BED5A808FC702E2,
	U3CShowSkipableVideoU3Ed__15_System_IDisposable_Dispose_m868AD0583BF061992737C7C1AD56221E213C58CA,
	U3CShowSkipableVideoU3Ed__15_MoveNext_m5F29EA1D008647F049C3E12984A40C7896D273FF,
	U3CShowSkipableVideoU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DC72B7C7E23EC68C5FC800FD8E10D0CAD51047C,
	U3CShowSkipableVideoU3Ed__15_System_Collections_IEnumerator_Reset_m87E9EBA69833374D69D0093339A492F75C7089B8,
	U3CShowSkipableVideoU3Ed__15_System_Collections_IEnumerator_get_Current_m233D159FE9FD1F3A0B58B95592542961CDE59B92,
	U3CStartLevelU3Ed__5__ctor_m40E0F43B47737DC030C73A3E96356F9BD3F8DC3A,
	U3CStartLevelU3Ed__5_System_IDisposable_Dispose_m5B6A3BD7CA5E679DDED35991F6E25C9C73C1C0C9,
	U3CStartLevelU3Ed__5_MoveNext_mBF771A4248B63A656381BB08FAB25E3163352281,
	U3CStartLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB03222A9ECB2B0A5FB517DC8A1992DE21F618EB9,
	U3CStartLevelU3Ed__5_System_Collections_IEnumerator_Reset_mFF66B8B005D400D58205F7659EEA52BD4F193514,
	U3CStartLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m46D1448A62D1C9665DD54CCC0DEE7110E17C6564,
	U3CFadeOutUIU3Ed__6__ctor_m2539C9CC0A45F866592433CF83D246B274238F62,
	U3CFadeOutUIU3Ed__6_System_IDisposable_Dispose_m9B5877393AB42E4FE79B6527098713E2ABA2706D,
	U3CFadeOutUIU3Ed__6_MoveNext_m6F41CFEB427B526C0D09780FC731B2FC5D595DFA,
	U3CFadeOutUIU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CC14574227BB61CA70117E2361C0F6D87CD26EE,
	U3CFadeOutUIU3Ed__6_System_Collections_IEnumerator_Reset_mFB8739A6E257A0720CE9A3AD6A932BC0D91FC5C8,
	U3CFadeOutUIU3Ed__6_System_Collections_IEnumerator_get_Current_m53D5C355FC8DDC1FE0FAE3329A10FB0C7C06BD71,
	U3CPlayEnumU3Ed__6__ctor_m683E1D5BF11617BE9754230490D5B39B35BCDE4B,
	U3CPlayEnumU3Ed__6_System_IDisposable_Dispose_m11744D53E003DE366748550850C1C1E704FC5F18,
	U3CPlayEnumU3Ed__6_MoveNext_mD57644BCB6BB897F860F6EFB211034B0710A3562,
	U3CPlayEnumU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEED7B3ED5FA1A7114E91DC8F0A515EC33CCA5F3E,
	U3CPlayEnumU3Ed__6_System_Collections_IEnumerator_Reset_mBFA15579940218EA9472991CB4241955FC8D701E,
	U3CPlayEnumU3Ed__6_System_Collections_IEnumerator_get_Current_m891D814CA530EAFBE6696FB8052CC30E611EC46D,
	U3CHitCoroutineU3Ed__5__ctor_m6AD40ACDEC1086934D80F542287503167F401F23,
	U3CHitCoroutineU3Ed__5_System_IDisposable_Dispose_m2A80DBEB9842D8BBADC1E619F8FDEC79484C2165,
	U3CHitCoroutineU3Ed__5_MoveNext_m4745EE755D415F989E61AAA96712FA29286359EC,
	U3CHitCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6CD2908A82669BDC95E4F27EEF1FFCB166ABB3,
	U3CHitCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m00610283EDCE305FC6FFCFE43EBD7672346B2F08,
	U3CHitCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_m5908ABFC35FC5053D16FC54AAD13D26AE6EFBBE0,
	U3CU3Ec__cctor_mB709F9F3611E7B81D7C5D61067777C51EDFA2BAF,
	U3CU3Ec__ctor_mC9241A41792541F3728B1769FA1C21093A704572,
	U3CU3Ec_U3CDieCoroutineU3Eb__15_0_mCEE744303E19F3BF656F17E0ACBA25FAB711FB8B,
	U3CDieCoroutineU3Ed__15__ctor_mC526EE7DE51527F8FCF48AC433EAEDDC24CB2D6F,
	U3CDieCoroutineU3Ed__15_System_IDisposable_Dispose_m72CECC45CF18D9AEBF9E72121A626DFC55740E77,
	U3CDieCoroutineU3Ed__15_MoveNext_mACEDF73FF962A543B1B0CBBD1560B4C383A4D40F,
	U3CDieCoroutineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19C0962D36868DDF92F8BAB93CFBE269E912E2D7,
	U3CDieCoroutineU3Ed__15_System_Collections_IEnumerator_Reset_mE7FBC826D4A9E7549C90E7ECE2BA5F75AF023D13,
	U3CDieCoroutineU3Ed__15_System_Collections_IEnumerator_get_Current_mD586E04066BF82A3AAEBEBA345291AA37C32BB2C,
	U3CAnimatedInjectPlanetU3Ed__11__ctor_m98BB095EE5A47C54F8632E65E5E2B24716DDCC45,
	U3CAnimatedInjectPlanetU3Ed__11_System_IDisposable_Dispose_mC95FE3FDD263318ECFA511740A64F4139C2DEDB9,
	U3CAnimatedInjectPlanetU3Ed__11_MoveNext_mD7FB88BB991C09DD155EF214C89199319E772FD1,
	U3CAnimatedInjectPlanetU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0769F9D4CBC8E93031871DDCFD0F8C850C01454,
	U3CAnimatedInjectPlanetU3Ed__11_System_Collections_IEnumerator_Reset_m4BFA69312FFC201FA87F5AF749C70509871FD986,
	U3CAnimatedInjectPlanetU3Ed__11_System_Collections_IEnumerator_get_Current_m371CC32A3F8C5511839ED9FA6941AAAC9C4A6B5B,
	U3CReviveEffectCoroutineU3Ed__28__ctor_mBA9A48DD5F68BDD684B0140514261E2D40E8A1CD,
	U3CReviveEffectCoroutineU3Ed__28_System_IDisposable_Dispose_m30F7FF873B8A3C9ECC3EBABAF209FE66D254E4D2,
	U3CReviveEffectCoroutineU3Ed__28_MoveNext_m5A0DAC4358705EA3150F42D0642A702A4FB0316F,
	U3CReviveEffectCoroutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92FF2104968A48B721C2EB70DF81D860960E71D7,
	U3CReviveEffectCoroutineU3Ed__28_System_Collections_IEnumerator_Reset_mC69F751E657AE8BBEAE33ADA4A18163663B920E1,
	U3CReviveEffectCoroutineU3Ed__28_System_Collections_IEnumerator_get_Current_m52E8094A91EC9066E8F3782B25FC883E0DDE6818,
	U3CDieCoroutineU3Ed__18__ctor_m85A6272C2742837C14B6E9C85D8B7C9496F8AE3F,
	U3CDieCoroutineU3Ed__18_System_IDisposable_Dispose_m3827272D4C78A5081CCDAD8385C43677A81B517B,
	U3CDieCoroutineU3Ed__18_MoveNext_m129439FEFFD54E06C9DE97E682C8642F6C495395,
	U3CDieCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DB5ED9B4489E6212EF73C23A5D574FA7551071A,
	U3CDieCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_m7838FEBAA1F59EF0ADFDCCEFA7A5D04A576D95A8,
	U3CDieCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mDCFD3A37B225C07EFB6F26D99EA405B5BF9EB645,
	U3CWatchVideoAdU3Ed__19__ctor_m755D3C98EAC457887CCDDB5C6A7A6382326B2F7B,
	U3CWatchVideoAdU3Ed__19_System_IDisposable_Dispose_m0781E5A60F6B3E3F9068EA84FCCC4AD5985D38AD,
	U3CWatchVideoAdU3Ed__19_MoveNext_m1F6ED44F2335EFA100C8F3C8A9E468D2BD18A2D5,
	U3CWatchVideoAdU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F28EF7CCC5B92CFDA69403AF0B2C8EB64955760,
	U3CWatchVideoAdU3Ed__19_System_Collections_IEnumerator_Reset_m550423B5B3DFEFB813AA9605F15CA7395CA95748,
	U3CWatchVideoAdU3Ed__19_System_Collections_IEnumerator_get_Current_m48942039A88204D3222BC866AAFC0D3B9D525B1D,
	U3CShowDialogU3Ed__5__ctor_m59490F79477F3F1F0336B49386EDDFD6F2BE5DB6,
	U3CShowDialogU3Ed__5_System_IDisposable_Dispose_mD3C3324D7F52CF790B32776002C14F0D5DF180DF,
	U3CShowDialogU3Ed__5_MoveNext_m8A142805DD0A9D730843D91CC39FF282149E66C2,
	U3CShowDialogU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EDBD3E2F35600B85F7BF5B6938676E9BA941A39,
	U3CShowDialogU3Ed__5_System_Collections_IEnumerator_Reset_m35D259DF22BE8F219C3F63C508E150AA2668C8E6,
	U3CShowDialogU3Ed__5_System_Collections_IEnumerator_get_Current_m58E5E7E9D9EC275715D5A8DA31E9DE96FDB9B6ED,
	U3CFadeInU3Ed__6__ctor_mCBC0193FAACBBAD2FDB0DB7F013AA40C5F75A586,
	U3CFadeInU3Ed__6_System_IDisposable_Dispose_m7C565B903714E7A6886506FAF7FA4E5FA8209B0D,
	U3CFadeInU3Ed__6_MoveNext_m912877866891797DAE4F7308A3C7C3C58F0C03FA,
	U3CFadeInU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m733AF1F2DFCE51D95944D3CD2501AD2B2AA9CDB9,
	U3CFadeInU3Ed__6_System_Collections_IEnumerator_Reset_mAC469BA28C6E64FBEBBFD14F5E72947611F623FF,
	U3CFadeInU3Ed__6_System_Collections_IEnumerator_get_Current_mC04B8879882898F71F71485924CD8D3DE788E4B7,
	U3CFadeOutU3Ed__7__ctor_m0469D5239E03FC76E471F47C6AD20E09C217DA42,
	U3CFadeOutU3Ed__7_System_IDisposable_Dispose_mC0687484F27947CDF841CC52CB5468D295B14224,
	U3CFadeOutU3Ed__7_MoveNext_m166A586295E7D425386167246F19F419B25DEC37,
	U3CFadeOutU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BA1C14E62F274432A381F967016D7ABA24D5465,
	U3CFadeOutU3Ed__7_System_Collections_IEnumerator_Reset_m0E0AADBCFA0827B325D31962F4590DA9AEE32C93,
	U3CFadeOutU3Ed__7_System_Collections_IEnumerator_get_Current_mB1CB7EF6E6DC081BF797B40C3C03A67930165CBA,
	U3CStartU3Ed__6__ctor_m82292CD1868B5627877555B7179646ABEA7B1425,
	U3CStartU3Ed__6_System_IDisposable_Dispose_m6764E5851B11059239F33A67FC6EF60CBAF45173,
	U3CStartU3Ed__6_MoveNext_mB652BD2218FCEB7ACBE84D8BC6B46D9940F834D6,
	U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m172D54AF1F0E9B6B4EB7BE7FB34F0057A187F774,
	U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m6BB0768629033FC61B5A2366DE6CE3629FE2B2FA,
	U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_mB79F0C091CA65C6C54D90599160BD8877212DDE1,
	U3CTut_JumpU3Ed__7__ctor_mD39C0787D1AB2549C4FD6AC1A3E461BF60FCACEA,
	U3CTut_JumpU3Ed__7_System_IDisposable_Dispose_mA9ADE99C8F444B25CA576E9EA60B0B81189CD42B,
	U3CTut_JumpU3Ed__7_MoveNext_m7C8F9862BB038B294CC000094476B7F604614A72,
	U3CTut_JumpU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E0C3B1EE46760F63982A0E73A4C1FFF3D32E032,
	U3CTut_JumpU3Ed__7_System_Collections_IEnumerator_Reset_m01FA362561D670ED095938E719E5315615E20077,
	U3CTut_JumpU3Ed__7_System_Collections_IEnumerator_get_Current_mC18D9511F425AA47D26120219EC5F0407C331D6B,
	U3CTut_BoostU3Ed__8__ctor_mF81ECB0514A06FCBD82614ACF1480A3DC52575DB,
	U3CTut_BoostU3Ed__8_System_IDisposable_Dispose_m3CCCC47F56C961CCA6DD7DA6CE6375E8010DEB0C,
	U3CTut_BoostU3Ed__8_MoveNext_m583C5A5A5EEF4BE405E6524B75B2AFF5FEBEC2CF,
	U3CTut_BoostU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB207A75F57E4299B91D0D587E468999C45A53162,
	U3CTut_BoostU3Ed__8_System_Collections_IEnumerator_Reset_m12770DB896300573532DF134AA146B3663BE7DC8,
	U3CTut_BoostU3Ed__8_System_Collections_IEnumerator_get_Current_mEA021015F7C7E290BFB34DE1E9BB9C891407445D,
	U3CTut_ExplodeU3Ed__9__ctor_m18E467A645CBC3DEDB86A21A68247609424AFE41,
	U3CTut_ExplodeU3Ed__9_System_IDisposable_Dispose_m975EB95E077E651E35E9663C2E8A3AC6DA876AC2,
	U3CTut_ExplodeU3Ed__9_MoveNext_m4A6271A783AF0F9800E76BD0CBD32409965BDCB2,
	U3CTut_ExplodeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46E89232831848ABF810BC2BAF5702D2A521EE3D,
	U3CTut_ExplodeU3Ed__9_System_Collections_IEnumerator_Reset_mAE982EEC6377CCB571A7F06C245B1E685BEB149B,
	U3CTut_ExplodeU3Ed__9_System_Collections_IEnumerator_get_Current_m1C1B730683CFDA089B8DB4D49C2E1827CAA5E32D,
	U3CTut_PracticeU3Ed__10__ctor_mC3468E11ECCF0AC0E390C67756D43200D9567106,
	U3CTut_PracticeU3Ed__10_System_IDisposable_Dispose_m1F54A776717183BAA1A5645EFE61D33EA6D0940A,
	U3CTut_PracticeU3Ed__10_MoveNext_mFF7D6A3FAB4496550638570362B53F7609BD1572,
	U3CTut_PracticeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1426E2C9A61D803F2515ED575BC67E07A558389F,
	U3CTut_PracticeU3Ed__10_System_Collections_IEnumerator_Reset_m2CDBDFD37AD47B2FC56B06DB5EED953967B1B7A2,
	U3CTut_PracticeU3Ed__10_System_Collections_IEnumerator_get_Current_mBEB2F2A6168E866ED63957856897E552CD37F182,
	U3CZoomCameraU3Ed__14__ctor_mEBD3B8676175C2EAB4633C98233908304777E8ED,
	U3CZoomCameraU3Ed__14_System_IDisposable_Dispose_m66A7525BF8778B7CB07A9D027347B9BE9D31A21E,
	U3CZoomCameraU3Ed__14_MoveNext_mB3483B827FB28C11BCC88A7EAE65D2D9E4D6BE62,
	U3CZoomCameraU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9A348AD948F348CEF2D158349BCA6C1B4C8E9FA,
	U3CZoomCameraU3Ed__14_System_Collections_IEnumerator_Reset_m9A908A9B51215ABA388376F2C078F29FC9C3C3DC,
	U3CZoomCameraU3Ed__14_System_Collections_IEnumerator_get_Current_mBFB9959811A8007298254CC5687480124657358D,
	U3CU3Ec__cctor_m49BF333FA13443238FEFD2E849B6169B0DB79B1D,
	U3CU3Ec__ctor_mE3DE0DEB66DE83A30D1E8CF1C40E2586DB0894FB,
	U3CU3Ec_U3CInitU3Eb__4_0_m4A03D7E42891CC89903DA4DD112FA48ED2BDCCDE,
	U3CU3Ec_U3CDisposeU3Eb__5_0_m2FC4986EF38F7F30569FFB50AE63E27868CA92AE,
	U3CU3Ec__cctor_m374A83CE16AE11E127D8A869A3B7271983D2494D,
	U3CU3Ec__ctor_m2E9E3F6A1BA3E29377A0D89AF411B199C7CD8A47,
	U3CU3Ec_U3CAwakeU3Eb__9_0_mD9619314793F9C42C4886B4D6C4BFD3D05A35CBE,
	U3CU3Ec_U3C_cctorU3Eb__18_0_m00910BE03F489E8E64A5238B72B0F3E6C8153EC0,
	U3CU3Ec__cctor_m9F2C55C9D1E69A6EBC7431794D674FE9E313FEF0,
	U3CU3Ec__ctor_m8136DE0AA0459F419FD1A7D801A066079204984B,
	U3CU3Ec_U3CTapVibratePatternU3Eb__7_0_mB27A3BA39CABD41D01E50A1E6A6AD76DCDD69881,
};
static const int32_t s_InvokerIndices[433] = 
{
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	124,
	26,
	26,
	26,
	102,
	23,
	3,
	102,
	102,
	102,
	102,
	102,
	102,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	34,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1804,
	23,
	23,
	23,
	1805,
	23,
	23,
	1020,
	23,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	23,
	1806,
	1807,
	1807,
	23,
	23,
	26,
	32,
	34,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	276,
	1808,
	1809,
	1810,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	1804,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	102,
	419,
	1811,
	1804,
	23,
	23,
	23,
	1812,
	1492,
	1492,
	23,
	14,
	14,
	14,
	14,
	14,
	23,
	26,
	23,
	1811,
	23,
	102,
	102,
	102,
	102,
	102,
	23,
	23,
	1492,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	32,
	23,
	23,
	3,
	3,
	23,
	3,
	111,
	111,
	23,
	23,
	23,
	102,
	102,
	658,
	658,
	32,
	23,
	3,
	23,
	1811,
	23,
	23,
	1811,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	49,
	3,
	3,
	3,
	3,
	3,
	3,
	3,
	1814,
	313,
	3,
	49,
	3,
	991,
	1118,
	1015,
	276,
	276,
	658,
	276,
	658,
	276,
	658,
	102,
	102,
	102,
	10,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	23,
	23,
	0,
	28,
	1815,
	1816,
	1570,
	1817,
	14,
	23,
	23,
	3,
	1818,
	1121,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	3,
	23,
	102,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	3,
	23,
	26,
	26,
	3,
	23,
	1811,
	1811,
	3,
	23,
	1813,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	433,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
