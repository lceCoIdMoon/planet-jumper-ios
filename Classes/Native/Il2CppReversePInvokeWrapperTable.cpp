﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147(intptr_t ___arg0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78(intptr_t ___ptr0, char* ___placementId1, int32_t ___error2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA(intptr_t ___ptr0, char* ___placementId1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699(intptr_t ___ptr0, char* ___placementId1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24(intptr_t ___ptr0, char* ___placementId1, int32_t ___completionState2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675(char* ___message0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4(intptr_t ___ptr0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E(intptr_t ___ptr0, int32_t ___error1, char* ___message2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F(intptr_t ___ptr0, char* ___placementId1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5(intptr_t ___ptr0, char* ___placementId1, int32_t ___error2, char* ___message3);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84(int64_t ___rawError0, char* ___message1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E(char* ___placementId0, int64_t ___rawShowResult1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D(char* ___eventString0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326();


extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[23] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326),
};
