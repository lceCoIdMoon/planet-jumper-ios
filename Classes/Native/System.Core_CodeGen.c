﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::Format(System.String,System.Object)
extern void SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000011 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000016 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000017 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000018 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000019 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x0000001A System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000001B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x0000001C System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000001D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000020 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000021 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000022 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000024 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000025 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000026 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000027 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000029 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x0000002A System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000002B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000002F System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000030 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000031 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000033 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000034 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000035 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000038 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000039 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x0000003A System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000003F System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000042 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000043 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000044 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000045 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000046 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000047 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000048 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x00000049 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000004A System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000004B TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000004C System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000004D System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000004E System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000004F System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000050 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000051 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x00000052 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x00000053 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x00000054 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000055 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x00000056 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000057 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000058 System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000059 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005A System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000005B System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000005C System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005E System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000005F System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000060 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000061 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x00000062 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000063 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000065 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000066 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000067 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000068 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000069 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000006A System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000006B System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000006C System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006D System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000006E System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000006F System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000070 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000071 System.Linq.Expressions.ExpressionType System.Linq.Expressions.Expression::get_NodeType()
extern void Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3 (void);
// 0x00000072 System.Void System.Linq.Expressions.Expression::.cctor()
extern void Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C (void);
// 0x00000073 System.Exception System.Linq.Expressions.Error::ExtensionNodeMustOverrideProperty(System.Object)
extern void Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6 (void);
// 0x00000074 System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::get_Body()
extern void LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0 (void);
// 0x00000075 System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::get_Member()
extern void MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272 (void);
// 0x00000076 System.Linq.Expressions.Expression System.Linq.Expressions.MemberExpression::get_Expression()
extern void MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351 (void);
// 0x00000077 System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::GetMember()
extern void MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2 (void);
// 0x00000078 System.String System.Linq.Expressions.Strings::ExtensionNodeMustOverrideProperty(System.Object)
extern void Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0 (void);
// 0x00000079 System.Void System.Dynamic.Utils.CacheDict`2::.ctor(System.Int32)
// 0x0000007A System.Int32 System.Dynamic.Utils.CacheDict`2::AlignSize(System.Int32)
// 0x0000007B System.Exception System.Dynamic.Utils.ContractUtils::get_Unreachable()
extern void ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F (void);
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000082 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000084 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000085 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000086 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000087 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000088 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000089 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000008A System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000008B System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000008E System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000092 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000095 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000096 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000097 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000099 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000009A System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000009B System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000009C System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x0000009D System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x0000009E T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x0000009F System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[160] = 
{
	SR_Format_mC35B52FE843D9C9D465B6B544FA184058A46E0A9,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Expression_get_NodeType_m230B6FE36D83B480D5ABBD01A1BA27A6F33BFCE3,
	Expression__cctor_m6C47FA07B808E8507CCCEACDCFE8EE668AEC409C,
	Error_ExtensionNodeMustOverrideProperty_mB79A086A50C7F12802A53324D7AF8C4E7EE208C6,
	LambdaExpression_get_Body_m7D6D43418EE260E6523847D5C9E866FC466C89D0,
	MemberExpression_get_Member_m28610D227E14379794673549E914931312AA1272,
	MemberExpression_get_Expression_m5FD4FC0BEE3268821D97C01C87BEB0C1F9D26351,
	MemberExpression_GetMember_m35CBBB03CFAFC32A700D877C35A4CD70C8C3FCA2,
	Strings_ExtensionNodeMustOverrideProperty_m2468ED1C894BD378104B522523876152F9D1F0D0,
	NULL,
	NULL,
	ContractUtils_get_Unreachable_mE78FFD6E65C4EB6EA0FB53A98EBE5AA477309E2F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[160] = 
{
	1,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	3,
	0,
	14,
	14,
	14,
	14,
	0,
	-1,
	-1,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[45] = 
{
	{ 0x02000005, { 63, 4 } },
	{ 0x02000006, { 67, 9 } },
	{ 0x02000007, { 78, 7 } },
	{ 0x02000008, { 87, 10 } },
	{ 0x02000009, { 99, 11 } },
	{ 0x0200000A, { 113, 9 } },
	{ 0x0200000B, { 125, 12 } },
	{ 0x0200000C, { 140, 1 } },
	{ 0x0200000D, { 141, 2 } },
	{ 0x0200000E, { 143, 12 } },
	{ 0x0200000F, { 155, 6 } },
	{ 0x02000011, { 161, 3 } },
	{ 0x02000012, { 166, 5 } },
	{ 0x02000013, { 171, 7 } },
	{ 0x02000014, { 178, 3 } },
	{ 0x02000015, { 181, 7 } },
	{ 0x02000016, { 188, 4 } },
	{ 0x02000021, { 192, 3 } },
	{ 0x02000024, { 195, 34 } },
	{ 0x02000026, { 229, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 3 } },
	{ 0x0600000E, { 39, 2 } },
	{ 0x0600000F, { 41, 2 } },
	{ 0x06000010, { 43, 2 } },
	{ 0x06000011, { 45, 4 } },
	{ 0x06000012, { 49, 3 } },
	{ 0x06000013, { 52, 1 } },
	{ 0x06000014, { 53, 3 } },
	{ 0x06000015, { 56, 2 } },
	{ 0x06000016, { 58, 5 } },
	{ 0x06000026, { 76, 2 } },
	{ 0x0600002B, { 85, 2 } },
	{ 0x06000030, { 97, 2 } },
	{ 0x06000036, { 110, 3 } },
	{ 0x0600003B, { 122, 3 } },
	{ 0x06000040, { 137, 3 } },
	{ 0x0600005D, { 164, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[231] = 
{
	{ (Il2CppRGCTXDataType)2, 17037 },
	{ (Il2CppRGCTXDataType)3, 11302 },
	{ (Il2CppRGCTXDataType)2, 17038 },
	{ (Il2CppRGCTXDataType)2, 17039 },
	{ (Il2CppRGCTXDataType)3, 11303 },
	{ (Il2CppRGCTXDataType)2, 17040 },
	{ (Il2CppRGCTXDataType)2, 17041 },
	{ (Il2CppRGCTXDataType)3, 11304 },
	{ (Il2CppRGCTXDataType)2, 17042 },
	{ (Il2CppRGCTXDataType)3, 11305 },
	{ (Il2CppRGCTXDataType)2, 17043 },
	{ (Il2CppRGCTXDataType)3, 11306 },
	{ (Il2CppRGCTXDataType)2, 17044 },
	{ (Il2CppRGCTXDataType)2, 17045 },
	{ (Il2CppRGCTXDataType)3, 11307 },
	{ (Il2CppRGCTXDataType)2, 17046 },
	{ (Il2CppRGCTXDataType)2, 17047 },
	{ (Il2CppRGCTXDataType)3, 11308 },
	{ (Il2CppRGCTXDataType)2, 17048 },
	{ (Il2CppRGCTXDataType)3, 11309 },
	{ (Il2CppRGCTXDataType)2, 17049 },
	{ (Il2CppRGCTXDataType)3, 11310 },
	{ (Il2CppRGCTXDataType)3, 11311 },
	{ (Il2CppRGCTXDataType)2, 12381 },
	{ (Il2CppRGCTXDataType)3, 11312 },
	{ (Il2CppRGCTXDataType)2, 17050 },
	{ (Il2CppRGCTXDataType)3, 11313 },
	{ (Il2CppRGCTXDataType)3, 11314 },
	{ (Il2CppRGCTXDataType)2, 12388 },
	{ (Il2CppRGCTXDataType)3, 11315 },
	{ (Il2CppRGCTXDataType)3, 11316 },
	{ (Il2CppRGCTXDataType)2, 17051 },
	{ (Il2CppRGCTXDataType)3, 11317 },
	{ (Il2CppRGCTXDataType)2, 17052 },
	{ (Il2CppRGCTXDataType)3, 11318 },
	{ (Il2CppRGCTXDataType)3, 11319 },
	{ (Il2CppRGCTXDataType)2, 17053 },
	{ (Il2CppRGCTXDataType)3, 11320 },
	{ (Il2CppRGCTXDataType)3, 11321 },
	{ (Il2CppRGCTXDataType)2, 12413 },
	{ (Il2CppRGCTXDataType)3, 11322 },
	{ (Il2CppRGCTXDataType)2, 12414 },
	{ (Il2CppRGCTXDataType)3, 11323 },
	{ (Il2CppRGCTXDataType)2, 17054 },
	{ (Il2CppRGCTXDataType)3, 11324 },
	{ (Il2CppRGCTXDataType)2, 17055 },
	{ (Il2CppRGCTXDataType)2, 17056 },
	{ (Il2CppRGCTXDataType)2, 12418 },
	{ (Il2CppRGCTXDataType)2, 17057 },
	{ (Il2CppRGCTXDataType)2, 12420 },
	{ (Il2CppRGCTXDataType)2, 17058 },
	{ (Il2CppRGCTXDataType)3, 11325 },
	{ (Il2CppRGCTXDataType)2, 12423 },
	{ (Il2CppRGCTXDataType)2, 12425 },
	{ (Il2CppRGCTXDataType)2, 17059 },
	{ (Il2CppRGCTXDataType)3, 11326 },
	{ (Il2CppRGCTXDataType)2, 17060 },
	{ (Il2CppRGCTXDataType)3, 11327 },
	{ (Il2CppRGCTXDataType)3, 11328 },
	{ (Il2CppRGCTXDataType)2, 17061 },
	{ (Il2CppRGCTXDataType)2, 12430 },
	{ (Il2CppRGCTXDataType)2, 17062 },
	{ (Il2CppRGCTXDataType)2, 12432 },
	{ (Il2CppRGCTXDataType)3, 11329 },
	{ (Il2CppRGCTXDataType)3, 11330 },
	{ (Il2CppRGCTXDataType)2, 12435 },
	{ (Il2CppRGCTXDataType)3, 11331 },
	{ (Il2CppRGCTXDataType)3, 11332 },
	{ (Il2CppRGCTXDataType)2, 12447 },
	{ (Il2CppRGCTXDataType)2, 17063 },
	{ (Il2CppRGCTXDataType)3, 11333 },
	{ (Il2CppRGCTXDataType)3, 11334 },
	{ (Il2CppRGCTXDataType)2, 12449 },
	{ (Il2CppRGCTXDataType)2, 16958 },
	{ (Il2CppRGCTXDataType)3, 11335 },
	{ (Il2CppRGCTXDataType)3, 11336 },
	{ (Il2CppRGCTXDataType)2, 17064 },
	{ (Il2CppRGCTXDataType)3, 11337 },
	{ (Il2CppRGCTXDataType)3, 11338 },
	{ (Il2CppRGCTXDataType)2, 12459 },
	{ (Il2CppRGCTXDataType)2, 17065 },
	{ (Il2CppRGCTXDataType)3, 11339 },
	{ (Il2CppRGCTXDataType)3, 11340 },
	{ (Il2CppRGCTXDataType)3, 10886 },
	{ (Il2CppRGCTXDataType)3, 11341 },
	{ (Il2CppRGCTXDataType)2, 17066 },
	{ (Il2CppRGCTXDataType)3, 11342 },
	{ (Il2CppRGCTXDataType)3, 11343 },
	{ (Il2CppRGCTXDataType)2, 12471 },
	{ (Il2CppRGCTXDataType)2, 17067 },
	{ (Il2CppRGCTXDataType)3, 11344 },
	{ (Il2CppRGCTXDataType)3, 11345 },
	{ (Il2CppRGCTXDataType)3, 11346 },
	{ (Il2CppRGCTXDataType)3, 11347 },
	{ (Il2CppRGCTXDataType)3, 11348 },
	{ (Il2CppRGCTXDataType)3, 10892 },
	{ (Il2CppRGCTXDataType)3, 11349 },
	{ (Il2CppRGCTXDataType)2, 17068 },
	{ (Il2CppRGCTXDataType)3, 11350 },
	{ (Il2CppRGCTXDataType)3, 11351 },
	{ (Il2CppRGCTXDataType)2, 12484 },
	{ (Il2CppRGCTXDataType)2, 17069 },
	{ (Il2CppRGCTXDataType)3, 11352 },
	{ (Il2CppRGCTXDataType)3, 11353 },
	{ (Il2CppRGCTXDataType)2, 12486 },
	{ (Il2CppRGCTXDataType)2, 17070 },
	{ (Il2CppRGCTXDataType)3, 11354 },
	{ (Il2CppRGCTXDataType)3, 11355 },
	{ (Il2CppRGCTXDataType)2, 17071 },
	{ (Il2CppRGCTXDataType)3, 11356 },
	{ (Il2CppRGCTXDataType)3, 11357 },
	{ (Il2CppRGCTXDataType)2, 17072 },
	{ (Il2CppRGCTXDataType)3, 11358 },
	{ (Il2CppRGCTXDataType)3, 11359 },
	{ (Il2CppRGCTXDataType)2, 12501 },
	{ (Il2CppRGCTXDataType)2, 17073 },
	{ (Il2CppRGCTXDataType)3, 11360 },
	{ (Il2CppRGCTXDataType)3, 11361 },
	{ (Il2CppRGCTXDataType)3, 11362 },
	{ (Il2CppRGCTXDataType)3, 10903 },
	{ (Il2CppRGCTXDataType)2, 17074 },
	{ (Il2CppRGCTXDataType)3, 11363 },
	{ (Il2CppRGCTXDataType)3, 11364 },
	{ (Il2CppRGCTXDataType)2, 17075 },
	{ (Il2CppRGCTXDataType)3, 11365 },
	{ (Il2CppRGCTXDataType)3, 11366 },
	{ (Il2CppRGCTXDataType)2, 12517 },
	{ (Il2CppRGCTXDataType)2, 17076 },
	{ (Il2CppRGCTXDataType)3, 11367 },
	{ (Il2CppRGCTXDataType)3, 11368 },
	{ (Il2CppRGCTXDataType)3, 11369 },
	{ (Il2CppRGCTXDataType)3, 11370 },
	{ (Il2CppRGCTXDataType)3, 11371 },
	{ (Il2CppRGCTXDataType)3, 11372 },
	{ (Il2CppRGCTXDataType)3, 10909 },
	{ (Il2CppRGCTXDataType)2, 17077 },
	{ (Il2CppRGCTXDataType)3, 11373 },
	{ (Il2CppRGCTXDataType)3, 11374 },
	{ (Il2CppRGCTXDataType)2, 17078 },
	{ (Il2CppRGCTXDataType)3, 11375 },
	{ (Il2CppRGCTXDataType)3, 11376 },
	{ (Il2CppRGCTXDataType)3, 11377 },
	{ (Il2CppRGCTXDataType)3, 11378 },
	{ (Il2CppRGCTXDataType)3, 11379 },
	{ (Il2CppRGCTXDataType)3, 11380 },
	{ (Il2CppRGCTXDataType)2, 17079 },
	{ (Il2CppRGCTXDataType)2, 17080 },
	{ (Il2CppRGCTXDataType)3, 11381 },
	{ (Il2CppRGCTXDataType)2, 12552 },
	{ (Il2CppRGCTXDataType)2, 12546 },
	{ (Il2CppRGCTXDataType)3, 11382 },
	{ (Il2CppRGCTXDataType)2, 12545 },
	{ (Il2CppRGCTXDataType)2, 17081 },
	{ (Il2CppRGCTXDataType)3, 11383 },
	{ (Il2CppRGCTXDataType)3, 11384 },
	{ (Il2CppRGCTXDataType)3, 11385 },
	{ (Il2CppRGCTXDataType)2, 12559 },
	{ (Il2CppRGCTXDataType)3, 11386 },
	{ (Il2CppRGCTXDataType)2, 17082 },
	{ (Il2CppRGCTXDataType)3, 11387 },
	{ (Il2CppRGCTXDataType)3, 11388 },
	{ (Il2CppRGCTXDataType)2, 17083 },
	{ (Il2CppRGCTXDataType)3, 11389 },
	{ (Il2CppRGCTXDataType)3, 11390 },
	{ (Il2CppRGCTXDataType)2, 17084 },
	{ (Il2CppRGCTXDataType)3, 11391 },
	{ (Il2CppRGCTXDataType)2, 17085 },
	{ (Il2CppRGCTXDataType)3, 11392 },
	{ (Il2CppRGCTXDataType)3, 11393 },
	{ (Il2CppRGCTXDataType)3, 11394 },
	{ (Il2CppRGCTXDataType)2, 12585 },
	{ (Il2CppRGCTXDataType)3, 11395 },
	{ (Il2CppRGCTXDataType)2, 12593 },
	{ (Il2CppRGCTXDataType)3, 11396 },
	{ (Il2CppRGCTXDataType)2, 17086 },
	{ (Il2CppRGCTXDataType)2, 17087 },
	{ (Il2CppRGCTXDataType)3, 11397 },
	{ (Il2CppRGCTXDataType)3, 11398 },
	{ (Il2CppRGCTXDataType)3, 11399 },
	{ (Il2CppRGCTXDataType)3, 11400 },
	{ (Il2CppRGCTXDataType)3, 11401 },
	{ (Il2CppRGCTXDataType)3, 11402 },
	{ (Il2CppRGCTXDataType)2, 12609 },
	{ (Il2CppRGCTXDataType)2, 17088 },
	{ (Il2CppRGCTXDataType)3, 11403 },
	{ (Il2CppRGCTXDataType)3, 11404 },
	{ (Il2CppRGCTXDataType)2, 12613 },
	{ (Il2CppRGCTXDataType)3, 11405 },
	{ (Il2CppRGCTXDataType)2, 17089 },
	{ (Il2CppRGCTXDataType)2, 12623 },
	{ (Il2CppRGCTXDataType)2, 12621 },
	{ (Il2CppRGCTXDataType)2, 17090 },
	{ (Il2CppRGCTXDataType)3, 11406 },
	{ (Il2CppRGCTXDataType)2, 17091 },
	{ (Il2CppRGCTXDataType)2, 17092 },
	{ (Il2CppRGCTXDataType)3, 11407 },
	{ (Il2CppRGCTXDataType)2, 17093 },
	{ (Il2CppRGCTXDataType)3, 11408 },
	{ (Il2CppRGCTXDataType)3, 11409 },
	{ (Il2CppRGCTXDataType)2, 12663 },
	{ (Il2CppRGCTXDataType)3, 11410 },
	{ (Il2CppRGCTXDataType)2, 12663 },
	{ (Il2CppRGCTXDataType)3, 11411 },
	{ (Il2CppRGCTXDataType)2, 12680 },
	{ (Il2CppRGCTXDataType)3, 11412 },
	{ (Il2CppRGCTXDataType)3, 11413 },
	{ (Il2CppRGCTXDataType)3, 11414 },
	{ (Il2CppRGCTXDataType)2, 17094 },
	{ (Il2CppRGCTXDataType)3, 11415 },
	{ (Il2CppRGCTXDataType)3, 11416 },
	{ (Il2CppRGCTXDataType)3, 11417 },
	{ (Il2CppRGCTXDataType)2, 12660 },
	{ (Il2CppRGCTXDataType)3, 11418 },
	{ (Il2CppRGCTXDataType)3, 11419 },
	{ (Il2CppRGCTXDataType)2, 12665 },
	{ (Il2CppRGCTXDataType)3, 11420 },
	{ (Il2CppRGCTXDataType)1, 17095 },
	{ (Il2CppRGCTXDataType)2, 12664 },
	{ (Il2CppRGCTXDataType)3, 11421 },
	{ (Il2CppRGCTXDataType)1, 12664 },
	{ (Il2CppRGCTXDataType)1, 12660 },
	{ (Il2CppRGCTXDataType)2, 17094 },
	{ (Il2CppRGCTXDataType)2, 12664 },
	{ (Il2CppRGCTXDataType)2, 12662 },
	{ (Il2CppRGCTXDataType)2, 12666 },
	{ (Il2CppRGCTXDataType)3, 11422 },
	{ (Il2CppRGCTXDataType)3, 11423 },
	{ (Il2CppRGCTXDataType)3, 11424 },
	{ (Il2CppRGCTXDataType)2, 12661 },
	{ (Il2CppRGCTXDataType)3, 11425 },
	{ (Il2CppRGCTXDataType)2, 12676 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	160,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	45,
	s_rgctxIndices,
	231,
	s_rgctxValues,
	NULL,
};
