﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E_RuntimeMethod_var;
extern const RuntimeMethod* IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675_RuntimeMethod_var;
extern const RuntimeMethod* IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05_RuntimeMethod_var;
extern const RuntimeMethod* IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988_RuntimeMethod_var;
extern const RuntimeMethod* IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE_RuntimeMethod_var;
extern const RuntimeMethod* IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19_RuntimeMethod_var;
extern const RuntimeMethod* IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4_RuntimeMethod_var;
extern const RuntimeMethod* IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E_RuntimeMethod_var;
extern const RuntimeMethod* IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5_RuntimeMethod_var;
extern const RuntimeMethod* IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F_RuntimeMethod_var;
extern const RuntimeMethod* IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84_RuntimeMethod_var;
extern const RuntimeMethod* IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E_RuntimeMethod_var;
extern const RuntimeMethod* IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5_RuntimeMethod_var;
extern const RuntimeMethod* IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9_RuntimeMethod_var;
extern const RuntimeMethod* IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699_RuntimeMethod_var;
extern const RuntimeMethod* IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24_RuntimeMethod_var;
extern const RuntimeMethod* IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78_RuntimeMethod_var;
extern const RuntimeMethod* IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA_RuntimeMethod_var;
extern const RuntimeMethod* PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D_RuntimeMethod_var;
extern const RuntimeMethod* PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B_RuntimeMethod_var;
extern const RuntimeMethod* PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0_RuntimeMethod_var;
extern const RuntimeMethod* PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_m48D03B84C3797DACB3B611DF970CB0521C03BF4C (void);
// 0x00000002 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_m47A672AC12BE72BAE9CD9E229097E1818F5ED0F9 (void);
// 0x00000003 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_mD36A0B407C54E3D5B7F4600564CE601ED4AE4A0A (void);
// 0x00000004 System.Boolean UnityEngine.Advertisements.Advertisement::get_debugMode()
extern void Advertisement_get_debugMode_mE6031D65A8EC421FBDFFCE90C854C34645F68984 (void);
// 0x00000005 System.Void UnityEngine.Advertisements.Advertisement::set_debugMode(System.Boolean)
extern void Advertisement_set_debugMode_mC42A15A9B574852BDCAE39A40C8F2801A58093E7 (void);
// 0x00000006 System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_m461B0C624EA4E088FC722D9E8D6B38FF3261CF1A (void);
// 0x00000007 System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m170A1D13DC4F43AD95096C38796343381DCA5A3D (void);
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern void Advertisement_Initialize_mE87487FCF006CB2834ABD315CE6996DD63A5E330 (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_m5A9C9564978427DBB41473E96F48065EF68ACA0C (void);
// 0x0000000A System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean)
extern void Advertisement_Initialize_mEADA718BF67610D6B3C6B601A14622C2909AF67D (void);
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Advertisement_Initialize_mF357DA90936A525920FBE7B21E081EF547DEC2FD (void);
// 0x0000000C System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern void Advertisement_IsReady_m330CE055D99555EBB3A33C71153125888FCDDB46 (void);
// 0x0000000D System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_m3DF7CE9A242B25940D728926268576148544F0F5 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement::Load(System.String)
extern void Advertisement_Load_m295F9B24B0DEB55DAC61D10D3EC9EE75BEA50993 (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Advertisement_Load_m40056FD79F3FC90822A7087A2BFA3E4104B1D20F (void);
// 0x00000010 System.Void UnityEngine.Advertisements.Advertisement::Show()
extern void Advertisement_Show_m93B5E5ACC8790D0EDCBC47C5B67835DD3DF62C2A (void);
// 0x00000011 System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m3CC9BFDA5143CBC3F4FDF578CF3173C744ADFC20 (void);
// 0x00000012 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_mAEACF55C73187D8A8EF8C7B25FDC6F3D6A5F0BF6 (void);
// 0x00000013 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_mE3DA5B6D9D7608A732596F280C65F284D849381A (void);
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m12A1239AE0C4E1C01F82EFA8179365165138BB99 (void);
// 0x00000015 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m29855BEE0C3270283DEA90C864D9340D008F62A9 (void);
// 0x00000016 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_m0FED87249A11E3D1A05B52F5EFB135352E6C2249 (void);
// 0x00000017 System.Void UnityEngine.Advertisements.Advertisement::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_AddListener_m213FC840853606FCEA3769A612311B82D9A5F3E3 (void);
// 0x00000018 System.Void UnityEngine.Advertisements.Advertisement::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_RemoveListener_m230106A6206FD8998928618D649DD443C182E40A (void);
// 0x00000019 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState()
extern void Advertisement_GetPlacementState_mF59A05D6FF5DC7730678A7D2E4547CFF13CE7B63 (void);
// 0x0000001A UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState(System.String)
extern void Advertisement_GetPlacementState_mAB71FF7C5B7BD367D7CCA532DCEB8AC92F9161C5 (void);
// 0x0000001B UnityEngine.Advertisements.Platform.IPlatform UnityEngine.Advertisements.Advertisement::CreatePlatform()
extern void Advertisement_CreatePlatform_m4B66F507B4A5AABD2B5830A5DC9149C59AD7FDF6 (void);
// 0x0000001C System.Boolean UnityEngine.Advertisements.Advertisement::IsSupported()
extern void Advertisement_IsSupported_m6A94CAB1E4B1DEE470DB57D39A071D1A01A9DA67 (void);
// 0x0000001D UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Banner::get_UnityLifecycleManager()
extern void Banner_get_UnityLifecycleManager_m5BC3D154673DB285E17084D37FC567525A8764A8 (void);
// 0x0000001E System.Boolean UnityEngine.Advertisements.Banner::get_IsLoaded()
extern void Banner_get_IsLoaded_mD775E3B877C9911F79F6A726404719A1308B4DBF (void);
// 0x0000001F System.Boolean UnityEngine.Advertisements.Banner::get_ShowAfterLoad()
extern void Banner_get_ShowAfterLoad_mD282F4189D9F0A1D85FC77107D1158E1EB2336D6 (void);
// 0x00000020 System.Void UnityEngine.Advertisements.Banner::set_ShowAfterLoad(System.Boolean)
extern void Banner_set_ShowAfterLoad_mC19C331A023B37A66A419F933E0F6353875AC237 (void);
// 0x00000021 System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.INativeBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Banner__ctor_mCC977576124FE54F3E3A32308979533B94695A13 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mAA9305BC1EFA7954A9282A92B2DB97E981747E20 (void);
// 0x00000023 System.Void UnityEngine.Advertisements.Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m58B612A96923452F31968DC293D071D847E7460B (void);
// 0x00000024 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_m590E1E55AE8A031ED3BC507859C83B478AAD7653 (void);
// 0x00000025 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_mB161D7A3B2D73EA3ADDB1194BD8F99028A672FA1 (void);
// 0x00000026 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidShow_mB7E3B7E0D2EB66BCFC56BA1279F2A6EE65248858 (void);
// 0x00000027 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidHide_m74F905A270B98C9601FA64BB9A29234D8B8C5ABA (void);
// 0x00000028 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerClick_m4CD3381A44221C62DDFB9320F8E7CD075AA6F3C2 (void);
// 0x00000029 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidLoad_mDDBB1273EF8F488A9B4A178FCECFE111220FCEF5 (void);
// 0x0000002A System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidError_m6A22953F1C113B2DF7243919343A0F8282E67B54 (void);
// 0x0000002B UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_mE318D088BF76C1C5CB22F9C1B2F4BE57EECF0D90 (void);
// 0x0000002C System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_m93809B6A66C8B3E5747AEAD35E58DE78D752DE06 (void);
// 0x0000002D UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_mABEFF7103EA3F6272DE6867A9BCE0EE44DD0840B (void);
// 0x0000002E System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_mCDB17872657BD6FE53A69C8C9326566CCC986A1F (void);
// 0x0000002F System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_mF832BEDDEB676AEE29616ED6D8E531F372D98EE0 (void);
// 0x00000030 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_mA4703F614E146D18EFD0A204843396C674DFBFDE (void);
// 0x00000031 System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_mEB883111DE1D6F3B76B10627A250FD5CCA903AE7 (void);
// 0x00000032 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_mDE0057958F972BD1188DB8F69DF5DBD8DB634EA3 (void);
// 0x00000033 System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_mE8F4A793E52B8EF17AEED8CD5FBD8A009486F611 (void);
// 0x00000034 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_clickCallback()
extern void BannerOptions_get_clickCallback_m03D90971283749E182F40D53C1CFCA6AEAC09246 (void);
// 0x00000035 System.Void UnityEngine.Advertisements.BannerOptions::set_clickCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_clickCallback_mEB7EBBCB56DFE68E39592A79F6C7E3D267C79EC0 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_mEA79B39E2DA91FAC7FB2001EF81CA0E783F49188 (void);
// 0x00000037 System.Boolean UnityEngine.Advertisements.Configuration::get_enabled()
extern void Configuration_get_enabled_m98287C3271DAB362A4D14194193E16BE265EC296 (void);
// 0x00000038 System.String UnityEngine.Advertisements.Configuration::get_defaultPlacement()
extern void Configuration_get_defaultPlacement_m6EC53DF961903FD991249184CC6E070887BB43D1 (void);
// 0x00000039 System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Configuration::get_placements()
extern void Configuration_get_placements_m68FC06EA30716F2F99776D030F26D9CF7B6360BD (void);
// 0x0000003A System.Void UnityEngine.Advertisements.Configuration::.ctor(System.String)
extern void Configuration__ctor_m83CCC987B3D5FFFFBB56E7794625389A04F76702 (void);
// 0x0000003B UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.IBanner::get_UnityLifecycleManager()
// 0x0000003C System.Boolean UnityEngine.Advertisements.IBanner::get_IsLoaded()
// 0x0000003D System.Boolean UnityEngine.Advertisements.IBanner::get_ShowAfterLoad()
// 0x0000003E System.Void UnityEngine.Advertisements.IBanner::set_ShowAfterLoad(System.Boolean)
// 0x0000003F System.Void UnityEngine.Advertisements.IBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000040 System.Void UnityEngine.Advertisements.IBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000041 System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x00000042 System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000043 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000044 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000045 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000046 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000047 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000048 System.Boolean UnityEngine.Advertisements.INativeBanner::get_IsLoaded()
// 0x00000049 System.Void UnityEngine.Advertisements.INativeBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
// 0x0000004A System.Void UnityEngine.Advertisements.INativeBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000004B System.Void UnityEngine.Advertisements.INativeBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x0000004C System.Void UnityEngine.Advertisements.INativeBanner::Hide(System.Boolean)
// 0x0000004D System.Void UnityEngine.Advertisements.INativeBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x0000004E System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsReady(System.String)
// 0x0000004F System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidError(System.String)
// 0x00000050 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidStart(System.String)
// 0x00000051 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x00000052 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationComplete()
// 0x00000053 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
// 0x00000054 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsAdLoaded(System.String)
// 0x00000055 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
// 0x00000056 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x00000057 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowStart(System.String)
// 0x00000058 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowClick(System.String)
// 0x00000059 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x0000005A System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_mDD39009D6DEA6148686E0A512D3EB623E8387BC0 (void);
// 0x0000005B System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_m3770DB996FBC6CE4816FF9E2EA2119DD6BE7EFC0 (void);
// 0x0000005C System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_m7E11048810D5CB83484D8191D9DAF2FFED3CE56A (void);
// 0x0000005D System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_m4A76D3C5FB14E8A34CFC8C213F171C3C99574F8A (void);
// 0x0000005E System.Object UnityEngine.Advertisements.MetaData::Get(System.String)
extern void MetaData_Get_mDF0ACBAE6B5A8AA3F8CBE4A0992B62121DB53395 (void);
// 0x0000005F System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_m80B9432000D2F381DD17567CEED0623C31DDF835 (void);
// 0x00000060 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_mCE15614B67E51F460947861DA6239A1E6577338F (void);
// 0x00000061 System.Void UnityEngine.Advertisements.AndroidInitializationListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidInitializationListener__ctor_m2B67FC562FC0DF7606FDF8D3495DF2FC668CB972 (void);
// 0x00000062 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationComplete()
extern void AndroidInitializationListener_onInitializationComplete_mB0889527DB1AFF1C4F84CBEF6C065C0DC2C21311 (void);
// 0x00000063 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationFailed(UnityEngine.AndroidJavaObject,System.String)
extern void AndroidInitializationListener_onInitializationFailed_mCA337217A3225397327193197F67F57A7A2BD331 (void);
// 0x00000064 System.Void UnityEngine.Advertisements.AndroidLoadListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidLoadListener__ctor_m36863855640FC57ADAD5565D155769A7A7C8732D (void);
// 0x00000065 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsAdLoaded(System.String)
extern void AndroidLoadListener_onUnityAdsAdLoaded_m8E7009B8A5D23D866FE84DA14EB06300C4F39CCF (void);
// 0x00000066 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsFailedToLoad(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidLoadListener_onUnityAdsFailedToLoad_mA75A0DBC8839D25196C62E1B1DA3D626A8A03B43 (void);
// 0x00000067 System.Void UnityEngine.Advertisements.AndroidShowListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidShowListener__ctor_mD258E23940C40514E2955EA95FA56863B6111E94 (void);
// 0x00000068 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowFailure(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidShowListener_onUnityAdsShowFailure_m7BB848249D6B5BBA1403FAA3E980F412FB2FEF1D (void);
// 0x00000069 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowStart(System.String)
extern void AndroidShowListener_onUnityAdsShowStart_m04C4230BC038588AB161E8A4AED7F1536DD8F153 (void);
// 0x0000006A System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowClick(System.String)
extern void AndroidShowListener_onUnityAdsShowClick_mE4F01FF4C447AAE25184CADB7093ECCC6BDE40B7 (void);
// 0x0000006B System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowComplete(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidShowListener_onUnityAdsShowComplete_m6BC05791EDF7D5C98B63A01421FC31E2612AC2D3 (void);
// 0x0000006C System.Void UnityEngine.Advertisements.INativePlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
// 0x0000006D System.Void UnityEngine.Advertisements.INativePlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x0000006E System.Void UnityEngine.Advertisements.INativePlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x0000006F System.Void UnityEngine.Advertisements.INativePlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x00000070 System.Void UnityEngine.Advertisements.INativePlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x00000071 System.Boolean UnityEngine.Advertisements.INativePlatform::GetDebugMode()
// 0x00000072 System.Void UnityEngine.Advertisements.INativePlatform::SetDebugMode(System.Boolean)
// 0x00000073 System.String UnityEngine.Advertisements.INativePlatform::GetVersion()
// 0x00000074 System.Boolean UnityEngine.Advertisements.INativePlatform::IsInitialized()
// 0x00000075 System.Boolean UnityEngine.Advertisements.INativePlatform::IsReady(System.String)
// 0x00000076 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.INativePlatform::GetPlacementState(System.String)
// 0x00000077 System.String UnityEngine.Advertisements.INativePlatform::GetDefaultPlacement()
// 0x00000078 System.IntPtr UnityEngine.Advertisements.IosShowListener::ShowListenerCreate(UnityEngine.Advertisements.IosShowListener/ShowFailureCallback,UnityEngine.Advertisements.IosShowListener/ShowStartCallback,UnityEngine.Advertisements.IosShowListener/ShowClickCallback,UnityEngine.Advertisements.IosShowListener/ShowCompleteCallback)
extern void IosShowListener_ShowListenerCreate_mC7BC91324D71CBB89B3D3CBC05C52E3BB850BCA6 (void);
// 0x00000079 System.Void UnityEngine.Advertisements.IosShowListener::ShowListenerDestroy(System.IntPtr)
extern void IosShowListener_ShowListenerDestroy_m0B501669515832065870FB64247CCB96EEE14176 (void);
// 0x0000007A System.Void UnityEngine.Advertisements.IosShowListener::.ctor(UnityEngine.Advertisements.IUnityAdsShowListener,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void IosShowListener__ctor_m8F94D4B5E98A3B4D064FA7F4091652CFEC7151E1 (void);
// 0x0000007B System.Void UnityEngine.Advertisements.IosShowListener::Dispose()
extern void IosShowListener_Dispose_mD19AF3F2E67DE19C1491C1D8C6238438A46BE03C (void);
// 0x0000007C System.Void UnityEngine.Advertisements.IosShowListener::OnShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
extern void IosShowListener_OnShowFailure_m5B3E4A8B8649B42B3715FA6B8A10F587F27C51B5 (void);
// 0x0000007D System.Void UnityEngine.Advertisements.IosShowListener::OnShowStart(System.String)
extern void IosShowListener_OnShowStart_mFBFB83F0E8D8B306563D6B77A8CAD9DA32CD4BE6 (void);
// 0x0000007E System.Void UnityEngine.Advertisements.IosShowListener::OnShowClick(System.String)
extern void IosShowListener_OnShowClick_mD0A8B5A8F4B2575848A97AC3F61CBF55BED99EE4 (void);
// 0x0000007F System.Void UnityEngine.Advertisements.IosShowListener::OnShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void IosShowListener_OnShowComplete_m27510514DF576FE90E059379DF79891A5DE83262 (void);
// 0x00000080 System.Void UnityEngine.Advertisements.IosShowListener::OnShowFailure(System.IntPtr,System.String,System.Int32,System.String)
extern void IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78 (void);
// 0x00000081 System.Void UnityEngine.Advertisements.IosShowListener::OnShowStart(System.IntPtr,System.String)
extern void IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA (void);
// 0x00000082 System.Void UnityEngine.Advertisements.IosShowListener::OnShowClick(System.IntPtr,System.String)
extern void IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699 (void);
// 0x00000083 System.Void UnityEngine.Advertisements.IosShowListener::OnShowComplete(System.IntPtr,System.String,System.Int32)
extern void IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24 (void);
// 0x00000084 System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_m2B88B2843F0F244FC88D0A426078BAFA0B80BAEF (void);
// 0x00000085 System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern void ShowOptions_set_resultCallback_m18C5A6A59822D5455650792279D3C445433881C8 (void);
// 0x00000086 System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_mE27D258CAEE96BB44D32B969E05B75596659A65D (void);
// 0x00000087 System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern void ShowOptions_set_gamerSid_m33375B215C04509B722A9EFB974F44CEAEDA430E (void);
// 0x00000088 System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern void ShowOptions__ctor_mE5966E77A7DE48D71B6C338A76B50DFA2835724C (void);
// 0x00000089 System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::add_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_add_OnApplicationQuitEventHandler_m7A860E8393CE6B8A559C8BD6B1CAC6C206CB47AE (void);
// 0x0000008A System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::remove_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_remove_OnApplicationQuitEventHandler_m5046C7F858D0ADC877095EFB6D87D3C2635053A5 (void);
// 0x0000008B System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::OnApplicationQuit()
extern void ApplicationQuit_OnApplicationQuit_m6F4956058FB7974AC9FFF35D5462EF4193D67904 (void);
// 0x0000008C System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m3D93B6B6AC6C8809FC9446B17AF9C8AC011F4593 (void);
// 0x0000008D System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::Update()
extern void CoroutineExecutor_Update_m77E0DF17371AA1DB5D974CA65572031A8EC1798C (void);
// 0x0000008E System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::.ctor()
extern void CoroutineExecutor__ctor_m67CBAA9BDFFFA5A7B06D750A2BCC8CE780F63EA1 (void);
// 0x0000008F UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Utilities.EnumUtilities::GetShowResultsFromCompletionState(UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void EnumUtilities_GetShowResultsFromCompletionState_m86358BF1FB64B21DE191AC347331004B6DC1E2F3 (void);
// 0x00000090 T UnityEngine.Advertisements.Utilities.EnumUtilities::GetEnumFromAndroidJavaObject(UnityEngine.AndroidJavaObject,T)
// 0x00000091 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
// 0x00000092 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::Post(System.Action)
// 0x00000093 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
// 0x00000094 System.Object UnityEngine.Advertisements.Utilities.Json::Deserialize(System.String)
extern void Json_Deserialize_mD2F68749D335BBA41591E53CF8C35A815B132748 (void);
// 0x00000095 System.String UnityEngine.Advertisements.Utilities.Json::Serialize(System.Object)
extern void Json_Serialize_mB474614B0DEFF4888F3E32EA3B0A7F482B4503F9 (void);
// 0x00000096 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::.ctor()
extern void UnityLifecycleManager__ctor_mCE97D5F551D08591644BCE5728375A244754B5BD (void);
// 0x00000097 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Initialize()
extern void UnityLifecycleManager_Initialize_m0544A2845C56FC01ED83BC59D8802666BE93306D (void);
// 0x00000098 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.UnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
extern void UnityLifecycleManager_StartCoroutine_m36CAAC3F71BED1EAF22A1C1E6F79E1218312E52B (void);
// 0x00000099 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Post(System.Action)
extern void UnityLifecycleManager_Post_mF9F2999D7787AF02D275A5CA5F8BA1C79C7A40C5 (void);
// 0x0000009A System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Dispose()
extern void UnityLifecycleManager_Dispose_mACB4683F3EFB169FB94C2AC1F6995ABAB2B361CA (void);
// 0x0000009B System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
extern void UnityLifecycleManager_SetOnApplicationQuitCallback_m186D695D94D336624545A80E74EBB41022AEA325 (void);
// 0x0000009C System.Void UnityEngine.Advertisements.Purchasing.IPurchasingEventSender::SendPurchasingEvent(System.String)
// 0x0000009D System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchasing_Initialize_m7104988051354DE77EAA486ABE610D365BA55E0B (void);
// 0x0000009E System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_mC2FF6E6B0C30012FF716E84B9619068782AECAE5 (void);
// 0x0000009F System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_m324BE5D644F1E15110E9F6B71E383AD40135A1CF (void);
// 0x000000A0 System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_m81DA685AFE504F91A9ED30D9B9B2360668D10198 (void);
// 0x000000A1 System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::SendEvent(System.String)
extern void Purchasing_SendEvent_mD483C0DE4551E9FFD7CA9E54F076A69EF6BFA999 (void);
// 0x000000A2 System.Void UnityEngine.Advertisements.Purchasing.Purchasing::.cctor()
extern void Purchasing__cctor_mCD1A03D73F9E06F9932E7BA9785689CD00678B1B (void);
// 0x000000A3 UnityEngine.Advertisements.Purchasing.PurchasingPlatform UnityEngine.Advertisements.Purchasing.PurchasingPlatform::get_Instance()
extern void PurchasingPlatform_get_Instance_mC0A99720EDD562691CE853E5754F6EBA2EEE8897 (void);
// 0x000000A4 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::set_Instance(UnityEngine.Advertisements.Purchasing.PurchasingPlatform)
extern void PurchasingPlatform_set_Instance_m04D177493F157CCA31F3D78A214D2EE7FA43E893 (void);
// 0x000000A5 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsPurchasingDispatchReturnEvent(System.Int64,System.String)
extern void PurchasingPlatform_UnityAdsPurchasingDispatchReturnEvent_mB6888E03BB35537C53B525775FCDBBEF7F56354B (void);
// 0x000000A6 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsSetDidInitiatePurchasingCommandCallback(UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingDidInitiatePurchasingCommand)
extern void PurchasingPlatform_UnityAdsSetDidInitiatePurchasingCommandCallback_mD3A1B0971A5FACB8E53F95C3F96EAD87FB40F25C (void);
// 0x000000A7 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsSetGetProductCatalogCallback(UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetProductCatalog)
extern void PurchasingPlatform_UnityAdsSetGetProductCatalogCallback_mF34FEFDFA74D55266CDA07EE3FC3A78458BAC4F7 (void);
// 0x000000A8 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsSetGetVersionCallback(UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetPurchasingVersion)
extern void PurchasingPlatform_UnityAdsSetGetVersionCallback_m5BEDD73A45D5DFC3188A794F675C3EDEEEA03E9D (void);
// 0x000000A9 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsSetInitializePurchasingCallback(UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingInitialize)
extern void PurchasingPlatform_UnityAdsSetInitializePurchasingCallback_mA7AAA7AE5C9B9C2D0E8F0F3F9B9266BD3121832D (void);
// 0x000000AA System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsDidInitiatePurchasingCommand(System.String)
extern void PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D (void);
// 0x000000AB System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsPurchasingGetProductCatalog()
extern void PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B (void);
// 0x000000AC System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsPurchasingGetPurchasingVersion()
extern void PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0 (void);
// 0x000000AD System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::UnityAdsPurchasingInitialize()
extern void PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326 (void);
// 0x000000AE System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::Initialize()
extern void PurchasingPlatform_Initialize_m2D55411EF6FB55346916518D3FA93C2B8C2AE6E2 (void);
// 0x000000AF System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::SendPurchasingEvent(System.String)
extern void PurchasingPlatform_SendPurchasingEvent_m999E699534B293A534C1CBB1320F24B8770230FB (void);
// 0x000000B0 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform::.ctor()
extern void PurchasingPlatform__ctor_mBDF2789AEF2E871306077422037314B68BB58CD4 (void);
// 0x000000B1 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000B2 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000B3 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000B4 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000B5 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.IPlatform::get_Banner()
// 0x000000B6 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.IPlatform::get_UnityLifecycleManager()
// 0x000000B7 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.IPlatform::get_NativePlatform()
// 0x000000B8 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsInitialized()
// 0x000000B9 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsShowing()
// 0x000000BA System.String UnityEngine.Advertisements.Platform.IPlatform::get_Version()
// 0x000000BB System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_DebugMode()
// 0x000000BC System.Void UnityEngine.Advertisements.Platform.IPlatform::set_DebugMode(System.Boolean)
// 0x000000BD System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.IPlatform::get_Listeners()
// 0x000000BE System.Void UnityEngine.Advertisements.Platform.IPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x000000BF System.Void UnityEngine.Advertisements.Platform.IPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x000000C0 System.Void UnityEngine.Advertisements.Platform.IPlatform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x000000C1 System.Void UnityEngine.Advertisements.Platform.IPlatform::Show()
// 0x000000C2 System.Void UnityEngine.Advertisements.Platform.IPlatform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000C3 System.Void UnityEngine.Advertisements.Platform.IPlatform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000C4 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::IsReady(System.String)
// 0x000000C5 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.IPlatform::GetPlacementState(System.String)
// 0x000000C6 System.Void UnityEngine.Advertisements.Platform.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x000000C7 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsReady(System.String)
// 0x000000C8 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidError(System.String)
// 0x000000C9 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidStart(System.String)
// 0x000000CA System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x000000CB System.Void UnityEngine.Advertisements.Platform.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_add_OnStart_m9280DC72AA715F601CC8F42453750AA8D85E5E33 (void);
// 0x000000CC System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_remove_OnStart_m3BA78A1EDD0A65E06EE0E728CCB4BB6E8F56C434 (void);
// 0x000000CD System.Void UnityEngine.Advertisements.Platform.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_add_OnFinish_m81C408E141474F16CF3A73223F6522B684D42311 (void);
// 0x000000CE System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_remove_OnFinish_m0D992DAB93D528C8FEA60023259BAFB58A4EEB66 (void);
// 0x000000CF UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.Platform::get_Banner()
extern void Platform_get_Banner_mF6064D7A7CB9D64E65C9593FAACB6F28D978FD63 (void);
// 0x000000D0 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.Platform::get_UnityLifecycleManager()
extern void Platform_get_UnityLifecycleManager_mD14F8704ED0F3FB9D111A82D66D046BB3A690132 (void);
// 0x000000D1 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.Platform::get_NativePlatform()
extern void Platform_get_NativePlatform_m8747D533AA2DD7BABFCA2015D43A5B8E1CF0222C (void);
// 0x000000D2 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsInitialized()
extern void Platform_get_IsInitialized_m5BF0DD189B3AB88B59DDB9ACEF7EF601F4785D7F (void);
// 0x000000D3 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsShowing()
extern void Platform_get_IsShowing_mF73670BCADEFA0425ED32FB54EEF487C0F1B06A5 (void);
// 0x000000D4 System.Void UnityEngine.Advertisements.Platform.Platform::set_IsShowing(System.Boolean)
extern void Platform_set_IsShowing_m85CE41F9A701BE1673B1E8CA2DD7621553E6B62A (void);
// 0x000000D5 System.String UnityEngine.Advertisements.Platform.Platform::get_Version()
extern void Platform_get_Version_mA263392AF90224E52657035043EFD0470ED2F23C (void);
// 0x000000D6 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_DebugMode()
extern void Platform_get_DebugMode_m1953631B9A3EAF854C3B8753D010DE05E9556D5F (void);
// 0x000000D7 System.Void UnityEngine.Advertisements.Platform.Platform::set_DebugMode(System.Boolean)
extern void Platform_set_DebugMode_mE4CB6CA4F2D7E92A699E0D8926D6912F80ABF204 (void);
// 0x000000D8 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::get_Listeners()
extern void Platform_get_Listeners_mF03ACE2F46E871498ED43CF40ECF9D2FE2319BAF (void);
// 0x000000D9 System.Void UnityEngine.Advertisements.Platform.Platform::.ctor(UnityEngine.Advertisements.INativePlatform,UnityEngine.Advertisements.IBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Platform__ctor_m25922F47189C27991D974883E786DDAA515D9DAD (void);
// 0x000000DA System.Void UnityEngine.Advertisements.Platform.Platform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Platform_Initialize_m869080A548A3AF80091A70CB60EA803F7165615C (void);
// 0x000000DB System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String)
extern void Platform_Load_m45AB572B2B330E22A6A273719E04392E21A77473 (void);
// 0x000000DC System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Platform_Load_m34FEE4D7C760F0C5E6C3CC9FB4272A9CA9EEF78B (void);
// 0x000000DD System.Void UnityEngine.Advertisements.Platform.Platform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Platform_Show_m253CB81E1A6C31AC49FC4B845C81F1661C6CEDAE (void);
// 0x000000DE System.Void UnityEngine.Advertisements.Platform.Platform::Show()
extern void Platform_Show_m4C78AEDDBFEDE4D464C42EE3508B1EE60BAAE0AF (void);
// 0x000000DF System.Void UnityEngine.Advertisements.Platform.Platform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_AddListener_mB26FAF1402C8C18E67B83757301E5FC7EFFA841F (void);
// 0x000000E0 System.Void UnityEngine.Advertisements.Platform.Platform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_RemoveListener_m4F537A0E1E8CE0A7FCA5E75353E102228B76FA0A (void);
// 0x000000E1 System.Boolean UnityEngine.Advertisements.Platform.Platform::IsReady(System.String)
extern void Platform_IsReady_mBA9236CC3C70111DF889B7A6EEEDDB81C3D6C8C6 (void);
// 0x000000E2 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Platform::GetPlacementState(System.String)
extern void Platform_GetPlacementState_mE8AB6D973D48D01DE4322874CC83AF5150AEC849 (void);
// 0x000000E3 System.Void UnityEngine.Advertisements.Platform.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mC39A25EFC30F0B7F4C9E84DCAA5E9F75BF7DA159 (void);
// 0x000000E4 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsReady(System.String)
extern void Platform_UnityAdsReady_mABF9A9DF5E0258EAC5DC6C68CDF36BFC75519990 (void);
// 0x000000E5 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidError(System.String)
extern void Platform_UnityAdsDidError_mC980D544AFB9163E76D96CD509E95F5627E7E924 (void);
// 0x000000E6 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidStart(System.String)
extern void Platform_UnityAdsDidStart_m42E3386B442F882B6946F2EEEABE266967CB713C (void);
// 0x000000E7 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void Platform_UnityAdsDidFinish_mACFA2AFBC03C44C6135E6E4B4A32DE61712B9249 (void);
// 0x000000E8 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::GetClonedHashSet(System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener>)
extern void Platform_GetClonedHashSet_m551E60E3C39B0B712A480FEA1D55A79100F1D6C4 (void);
// 0x000000E9 System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_0(System.Object,UnityEngine.Advertisements.Events.StartEventArgs)
extern void Platform_U3CInitializeU3Eb__30_0_mDE5E1A0F546C3450946CA0CB4AD1C1B8ABB6DABC (void);
// 0x000000EA System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_1(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void Platform_U3CInitializeU3Eb__30_1_m77BDEC0A1CA5EC1E012D3903508842CB1E9CA4F1 (void);
// 0x000000EB System.Boolean UnityEngine.Advertisements.Platform.iOS.IosBanner::get_IsLoaded()
extern void IosBanner_get_IsLoaded_m62338D5AA9B65B2D63094FC8843D834880143DF4 (void);
// 0x000000EC System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::set_IsLoaded(System.Boolean)
extern void IosBanner_set_IsLoaded_m5BAEA747825FB2E5BABCD95058877C804DCEB917 (void);
// 0x000000ED System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerShow(System.String,System.Boolean)
extern void IosBanner_UnityAdsBannerShow_m7C796D69AFBB3510DD213A43ABC8A3930126008E (void);
// 0x000000EE System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerHide(System.Boolean)
extern void IosBanner_UnityAdsBannerHide_m3EF341F8BA3894911947BE1283B98FC625245AE3 (void);
// 0x000000EF System.Boolean UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerIsLoaded()
extern void IosBanner_UnityAdsBannerIsLoaded_m63698D956BE7769B03775F12F7A5BEFE65F99C29 (void);
// 0x000000F0 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerSetPosition(System.Int32)
extern void IosBanner_UnityAdsBannerSetPosition_mCF09D7291E6AA5E4F36836918D52C56A67DA062C (void);
// 0x000000F1 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerShowCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerShowDelegate)
extern void IosBanner_UnityAdsSetBannerShowCallback_m7A06A1CECBB07B0C1CF83F7B3A5662B9A1A80018 (void);
// 0x000000F2 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerHideCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerHideDelegate)
extern void IosBanner_UnityAdsSetBannerHideCallback_m2F3B0E3153FD22D0A16DE35A5AA11687F6B6638D (void);
// 0x000000F3 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerClickCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerClickDelegate)
extern void IosBanner_UnityAdsSetBannerClickCallback_mAA8D05BB10AC2D6A5E5DFCC6D6DAAB27DE9DE18D (void);
// 0x000000F4 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerErrorCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerErrorDelegate)
extern void IosBanner_UnityAdsSetBannerErrorCallback_mF3D856DAC456A1E073AFA53BAAAD40A3EE8454EA (void);
// 0x000000F5 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerUnloadCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerUnloadDelegate)
extern void IosBanner_UnityAdsSetBannerUnloadCallback_mD2C1B9A58F47F16F48F7E0DDB99464005D104778 (void);
// 0x000000F6 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsSetBannerLoadCallback(UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerLoadDelegate)
extern void IosBanner_UnityAdsSetBannerLoadCallback_m3FA7784B7E14ED1C19F6EC688F80B258202A072A (void);
// 0x000000F7 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityBannerInitialize()
extern void IosBanner_UnityBannerInitialize_m9941502BF9585D8185B84A3249CABB97BF7FFE51 (void);
// 0x000000F8 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerDidShow(System.String)
extern void IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE (void);
// 0x000000F9 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerDidHide(System.String)
extern void IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05 (void);
// 0x000000FA System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerClick(System.String)
extern void IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E (void);
// 0x000000FB System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerDidError(System.String)
extern void IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675 (void);
// 0x000000FC System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerDidUnload(System.String)
extern void IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19 (void);
// 0x000000FD System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::UnityAdsBannerDidLoad(System.String)
extern void IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988 (void);
// 0x000000FE System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void IosBanner_SetupBanner_mFC8F801CCB2E4845356263B359D309CBE88DC193 (void);
// 0x000000FF System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void IosBanner_Load_m34E01C7AA26556D512A9A127D462C03D988C8C30 (void);
// 0x00000100 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void IosBanner_Show_mD76C8EA840957C368A26C3FE9077D5FF7AA34B1F (void);
// 0x00000101 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::Hide(System.Boolean)
extern void IosBanner_Hide_m0AA12201568983F5621854D382A5B9746BBE5B93 (void);
// 0x00000102 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void IosBanner_SetPosition_m1E8CA92686E7E4C6ED082D4C457493B21AC9C37D (void);
// 0x00000103 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner::.ctor()
extern void IosBanner__ctor_mD10AFF14AED78D9209ADBA2123745944F385C625 (void);
// 0x00000104 System.IntPtr UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::InitializationListenerCreate(UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitSuccessCallback,UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitFailureCallback)
extern void IosInitializationListener_InitializationListenerCreate_m5D4DDC7387934A291E2D4E6068094DEBAFB770DD (void);
// 0x00000105 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::InitializationListenerDestroy(System.IntPtr)
extern void IosInitializationListener_InitializationListenerDestroy_mA0CD5150F8911D18B213467E4814953355AD8C7C (void);
// 0x00000106 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::.ctor(UnityEngine.Advertisements.IUnityAdsInitializationListener,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void IosInitializationListener__ctor_mE87193E63262F1FAF968C59229BB365749417868 (void);
// 0x00000107 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::Dispose()
extern void IosInitializationListener_Dispose_mAED43AF8A77FF4C7CA92247523D225E3CA021F35 (void);
// 0x00000108 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::OnInitializationComplete()
extern void IosInitializationListener_OnInitializationComplete_m08D1A7AC2C7D4E3365524E1AD585FB93BB04BF7C (void);
// 0x00000109 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
extern void IosInitializationListener_OnInitializationFailed_mCA9F0384011CC5D36ED8D5E0D125A28A0AAC05E7 (void);
// 0x0000010A System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::OnInitializationComplete(System.IntPtr)
extern void IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4 (void);
// 0x0000010B System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener::OnInitializationFailed(System.IntPtr,System.Int32,System.String)
extern void IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E (void);
// 0x0000010C System.IntPtr UnityEngine.Advertisements.Platform.iOS.IosLoadListener::LoadListenerCreate(UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadSuccessCallback,UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadFailureCallback)
extern void IosLoadListener_LoadListenerCreate_m551945EB6EFC1516DB85850771808C5E17E94A30 (void);
// 0x0000010D System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::LoadListenerDestroy(System.IntPtr)
extern void IosLoadListener_LoadListenerDestroy_mDD880D872CBE2CA92285F81C9EE84F9DDB5395AE (void);
// 0x0000010E System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::.ctor(UnityEngine.Advertisements.IUnityAdsLoadListener,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void IosLoadListener__ctor_mE00D8BBCE3EA83607D91784778B071D116A45076 (void);
// 0x0000010F System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::Dispose()
extern void IosLoadListener_Dispose_m622D90E6B3937F72D7175B2E67190C300C2AD83F (void);
// 0x00000110 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::OnLoadSuccess(System.String)
extern void IosLoadListener_OnLoadSuccess_m4CAE79C5A3BF78500B201FDD3A92E3034CBB0576 (void);
// 0x00000111 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::OnLoadFailure(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
extern void IosLoadListener_OnLoadFailure_mBA8102263DC8C669447782652CBBE47A3C82FB30 (void);
// 0x00000112 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::OnLoadSuccess(System.IntPtr,System.String)
extern void IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F (void);
// 0x00000113 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener::OnLoadFailure(System.IntPtr,System.String,System.Int32,System.String)
extern void IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5 (void);
// 0x00000114 System.IntPtr UnityEngine.Advertisements.Platform.iOS.IosNativeObject::get_NativePtr()
extern void IosNativeObject_get_NativePtr_m59E9F3081935394429EE75F2598B3A241BDE13C9 (void);
// 0x00000115 System.Void UnityEngine.Advertisements.Platform.iOS.IosNativeObject::set_NativePtr(System.IntPtr)
extern void IosNativeObject_set_NativePtr_mFEED092BD5C14859AA886B1B3836D9EDF39FEF30 (void);
// 0x00000116 T UnityEngine.Advertisements.Platform.iOS.IosNativeObject::Get(System.IntPtr)
// 0x00000117 System.Void UnityEngine.Advertisements.Platform.iOS.IosNativeObject::Dispose()
extern void IosNativeObject_Dispose_m2A0912C85D7750BCAB47810DE33F0152AAB4046E (void);
// 0x00000118 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosNativeObject::CheckDisposedAndLogError(System.String)
extern void IosNativeObject_CheckDisposedAndLogError_mDE54C78A98509B4C89B9BF694ADA7D64D0B847F5 (void);
// 0x00000119 System.Void UnityEngine.Advertisements.Platform.iOS.IosNativeObject::BridgeTransfer(System.IntPtr)
extern void IosNativeObject_BridgeTransfer_m0DA68532C573E123DB6D3DF6278DF96A16AC1E0D (void);
// 0x0000011A System.Void UnityEngine.Advertisements.Platform.iOS.IosNativeObject::.ctor()
extern void IosNativeObject__ctor_m12D04541A783F3A2E5F8AEB935B3F52834F14749 (void);
// 0x0000011B System.Void UnityEngine.Advertisements.Platform.iOS.IosNativeObject::.cctor()
extern void IosNativeObject__cctor_m021A4AA6DC9BA41C678258F6667078F4AEE18308 (void);
// 0x0000011C System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsInitialize(System.String,System.Boolean,System.Boolean,System.IntPtr)
extern void IosPlatform_UnityAdsInitialize_mECE53AB0701EE386E0D5BCDFEB70DDBAC6C64D1D (void);
// 0x0000011D System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsLoad(System.String,System.IntPtr)
extern void IosPlatform_UnityAdsLoad_mE1F4D5B8598992C11564E0B479D22CAFF813038D (void);
// 0x0000011E System.String UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsGetDefaultPlacementID()
extern void IosPlatform_UnityAdsGetDefaultPlacementID_mDED8747FC7E1B13DC2AEC3865257CA8DA5231400 (void);
// 0x0000011F System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsShow(System.String,System.IntPtr)
extern void IosPlatform_UnityAdsShow_mBB8B5203A041A8176C3E71C7C5B11BE83DCD699D (void);
// 0x00000120 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsGetDebugMode()
extern void IosPlatform_UnityAdsGetDebugMode_mE3C1F76F623894CB7F604BF12C5E2D1D0FEF255A (void);
// 0x00000121 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetDebugMode(System.Boolean)
extern void IosPlatform_UnityAdsSetDebugMode_m8EDC93D2C178A83B0E7C26A0EFFFA6B3CF3EBBD9 (void);
// 0x00000122 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsIsReady(System.String)
extern void IosPlatform_UnityAdsIsReady_m7B9242ED7031A5F0977BC380CE47E864C46EC6FB (void);
// 0x00000123 System.Int64 UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsGetPlacementState(System.String)
extern void IosPlatform_UnityAdsGetPlacementState_mA14E79C14F601678668E24EB00DED0A8D63443E5 (void);
// 0x00000124 System.String UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsGetVersion()
extern void IosPlatform_UnityAdsGetVersion_m1358AD2CCD735EB9134B0E95178A6722FFCFCE3F (void);
// 0x00000125 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsIsInitialized()
extern void IosPlatform_UnityAdsIsInitialized_mC1876BD790192AE879CAC5E06DE70D4B5905DA60 (void);
// 0x00000126 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetMetaData(System.String,System.String)
extern void IosPlatform_UnityAdsSetMetaData_m8842D5FC9B23D8040D08A7DA4667AA5E306B5EA4 (void);
// 0x00000127 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetReadyCallback(UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsReadyDelegate)
extern void IosPlatform_UnityAdsSetReadyCallback_m446CFA2C00567380FFA5375DEA00D5F53F364E0B (void);
// 0x00000128 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetDidErrorCallback(UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidErrorDelegate)
extern void IosPlatform_UnityAdsSetDidErrorCallback_mB6C329BB40205E10190DB2B64CBC53E3E12984B9 (void);
// 0x00000129 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetDidStartCallback(UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidStartDelegate)
extern void IosPlatform_UnityAdsSetDidStartCallback_mEAC128A1D30A73B4E89B82DCDED21D38C2AD6233 (void);
// 0x0000012A System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsSetDidFinishCallback(UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidFinishDelegate)
extern void IosPlatform_UnityAdsSetDidFinishCallback_mA2AD6D9088297318B752F6DBADFA4383229EBFCB (void);
// 0x0000012B System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsReady(System.String)
extern void IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9 (void);
// 0x0000012C System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsDidError(System.Int64,System.String)
extern void IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84 (void);
// 0x0000012D System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsDidStart(System.String)
extern void IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5 (void);
// 0x0000012E System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::UnityAdsDidFinish(System.String,System.Int64)
extern void IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E (void);
// 0x0000012F System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void IosPlatform_SetupPlatform_m385C2C5BD739E98CBBDA642E2161426910C57554 (void);
// 0x00000130 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void IosPlatform_Initialize_mFABBB355368491B8C675C392634CE79E76EDA875 (void);
// 0x00000131 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void IosPlatform_Load_mEB5F01869162DEB142E8D0441178BF36B1025C24 (void);
// 0x00000132 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void IosPlatform_Show_mA2EFAA9DAEC442BE2C058AD9CFE25BCA03DDDD30 (void);
// 0x00000133 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void IosPlatform_SetMetaData_m47026330939714B25F60E74C40C28D49C0FAF5A9 (void);
// 0x00000134 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::GetDebugMode()
extern void IosPlatform_GetDebugMode_mFA5ADBD19901A5984637A6CCD4B57DB1933941E2 (void);
// 0x00000135 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::SetDebugMode(System.Boolean)
extern void IosPlatform_SetDebugMode_m7FA7274872E698C711B4187944F1750BFD71059B (void);
// 0x00000136 System.String UnityEngine.Advertisements.Platform.iOS.IosPlatform::GetVersion()
extern void IosPlatform_GetVersion_m1BB31C578D003B2378AC9C333236484A3476B856 (void);
// 0x00000137 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::IsInitialized()
extern void IosPlatform_IsInitialized_mD29EA2769F8730B310158914F3018FDAE7291373 (void);
// 0x00000138 System.Boolean UnityEngine.Advertisements.Platform.iOS.IosPlatform::IsReady(System.String)
extern void IosPlatform_IsReady_mEDD29A241595A9E70ADC1F735C013FCBA024B183 (void);
// 0x00000139 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.iOS.IosPlatform::GetPlacementState(System.String)
extern void IosPlatform_GetPlacementState_mF1C05E5EF574574F16684E7D3400A8E1E8C27157 (void);
// 0x0000013A System.String UnityEngine.Advertisements.Platform.iOS.IosPlatform::GetDefaultPlacement()
extern void IosPlatform_GetDefaultPlacement_m378714BBF0AAE9E43FEAEB6CBD02D6EF18CFCD01 (void);
// 0x0000013B System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnInitializationComplete()
extern void IosPlatform_OnInitializationComplete_mD278AE800393B6A72089CFEB83AA82F002CECED5 (void);
// 0x0000013C System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
extern void IosPlatform_OnInitializationFailed_m39B4D8DCFC75CE77FAA23E31C7A9CAF144F4BCE2 (void);
// 0x0000013D System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsAdLoaded(System.String)
extern void IosPlatform_OnUnityAdsAdLoaded_m41A6DE443697707A668A3A6261337B3F360213C1 (void);
// 0x0000013E System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
extern void IosPlatform_OnUnityAdsFailedToLoad_m8BD4A6316A28CAC39BA08DD19B232239F186F434 (void);
// 0x0000013F System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
extern void IosPlatform_OnUnityAdsShowFailure_m7777774460783293F46732FA6EDC2C44A149E4A9 (void);
// 0x00000140 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsShowStart(System.String)
extern void IosPlatform_OnUnityAdsShowStart_mE81517CF44E81D9EB2CF87576A22198EF515F751 (void);
// 0x00000141 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsShowClick(System.String)
extern void IosPlatform_OnUnityAdsShowClick_mCB7D429BBF86FA4A705773B39F30D21D6D571938 (void);
// 0x00000142 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void IosPlatform_OnUnityAdsShowComplete_mBFEDBDD453A33D5248A9398219346C9868BB8079 (void);
// 0x00000143 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform::.ctor()
extern void IosPlatform__ctor_m3B441570BCE922970FF004797FB102549A6DD3FD (void);
// 0x00000144 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::get_IsLoaded()
extern void UnsupportedBanner_get_IsLoaded_mDC178A2DCE79F464B2375CC54C07508791EF8220 (void);
// 0x00000145 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void UnsupportedBanner_SetupBanner_mED5E5C9580EBFD3D4EF77A4E117753D7C0430A81 (void);
// 0x00000146 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void UnsupportedBanner_Load_mCC131405E895BABFE9613157061F750B3ECDCE02 (void);
// 0x00000147 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void UnsupportedBanner_Show_m706EDE56C0F364EDE36C7912EDCD4EC78F428CF1 (void);
// 0x00000148 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Hide(System.Boolean)
extern void UnsupportedBanner_Hide_mA943DFC0F81643EF03E788BBAC95DC94E6543E49 (void);
// 0x00000149 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void UnsupportedBanner_SetPosition_mF283B7CA6741F7F5EF4320A446FDFF7F76966F9A (void);
// 0x0000014A System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::.ctor()
extern void UnsupportedBanner__ctor_mE09660825501B22793934E4FAF88E2AC551B50AF (void);
// 0x0000014B System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void UnsupportedPlatform_SetupPlatform_mEAE492855EDA3294AFB634A31E530EE8A993DD75 (void);
// 0x0000014C System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void UnsupportedPlatform_Initialize_m4C73B7A0D77D2E601DF19E1AF44B43D1A0977E92 (void);
// 0x0000014D System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void UnsupportedPlatform_Load_m6C51586FCF4B5CB1FA2EE7E4A13351D111E30938 (void);
// 0x0000014E System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void UnsupportedPlatform_Show_m8E9E985F1B06553F20759426389052FDB6DF0AC1 (void);
// 0x0000014F System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_mEF9D2D804BB15C294121F26D62947B353BED0BAC (void);
// 0x00000150 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDebugMode()
extern void UnsupportedPlatform_GetDebugMode_m8E4C6C2056E75FEADAF90800DAE27F3094B6A123 (void);
// 0x00000151 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetDebugMode(System.Boolean)
extern void UnsupportedPlatform_SetDebugMode_m0EB1217093E6A4B3416A6645AD5DD0691FCBA70C (void);
// 0x00000152 System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetVersion()
extern void UnsupportedPlatform_GetVersion_m2A186D53BD52D2F16BA08660E6CC6CF1FAE8F429 (void);
// 0x00000153 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsInitialized()
extern void UnsupportedPlatform_IsInitialized_mC7E87A4B2506854627D1D846C7DE7F4663A80941 (void);
// 0x00000154 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mD1C5A3E57449BD6A464ADDAFF37BA9C06AC4AFF1 (void);
// 0x00000155 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetPlacementState(System.String)
extern void UnsupportedPlatform_GetPlacementState_m4051099D5AACA910EC393ACA22916881D07DB20D (void);
// 0x00000156 System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDefaultPlacement()
extern void UnsupportedPlatform_GetDefaultPlacement_mC994C6A3F4A926E3FE236645E88A76997D9AF500 (void);
// 0x00000157 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m2F549700D3A58CD406CB0536CCD1374E0C5408CC (void);
// 0x00000158 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::Awake()
extern void BannerPlaceholder_Awake_m8797C23CCC67E260BF11FB0494B01E3CF037BD62 (void);
// 0x00000159 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::OnGUI()
extern void BannerPlaceholder_OnGUI_mF6A4575DD98A6C311345F95BF94EAA30B3563940 (void);
// 0x0000015A System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::ShowBanner(UnityEngine.Advertisements.BannerPosition,UnityEngine.Advertisements.BannerOptions)
extern void BannerPlaceholder_ShowBanner_m679869708435680777BCDE8002C1A20B0FD21060 (void);
// 0x0000015B System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::HideBanner()
extern void BannerPlaceholder_HideBanner_m0E9ECE5CED7E050E07C3A160ABA6240CADB89745 (void);
// 0x0000015C UnityEngine.Texture2D UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::BackgroundTexture(System.Int32,System.Int32,UnityEngine.Color)
extern void BannerPlaceholder_BackgroundTexture_m93516C740130C4B42C7FAB978E4D234E95E3B589 (void);
// 0x0000015D UnityEngine.Rect UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::GetBannerRect(UnityEngine.Advertisements.BannerPosition)
extern void BannerPlaceholder_GetBannerRect_mD62442B18D4BA67465510CB21E04DCC9CFBF1AA3 (void);
// 0x0000015E System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::.ctor()
extern void BannerPlaceholder__ctor_m2F312011260B4F57CA2BFB1DEBB071A2C8D63883 (void);
// 0x0000015F UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerView()
extern void BannerBundle_get_bannerView_m9444F9C37FD8F6A3C977ED6ABE1E7BF2F01C9340 (void);
// 0x00000160 System.String UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerPlacementId()
extern void BannerBundle_get_bannerPlacementId_mCBA57814029DC050330433EA892C125D7F90BD03 (void);
// 0x00000161 System.Void UnityEngine.Advertisements.Platform.Android.BannerBundle::.ctor(System.String,UnityEngine.AndroidJavaObject)
extern void BannerBundle__ctor_m52B4940B9EAD63CA3FD67E9B994F25641FBE4C32 (void);
// 0x00000162 System.String UnityEngine.Advertisements.Events.FinishEventArgs::get_placementId()
extern void FinishEventArgs_get_placementId_m1461AC4F5FB9F28D10F5B1695387714D94E15453 (void);
// 0x00000163 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Events.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_m3A4584DB549E20B21873168FAB50D51FD13C2141 (void);
// 0x00000164 System.Void UnityEngine.Advertisements.Events.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_mD6C2E617EC4471D128F95137C670648F391D21AB (void);
// 0x00000165 System.String UnityEngine.Advertisements.Events.StartEventArgs::get_placementId()
extern void StartEventArgs_get_placementId_mD575F46F40275216C6556BE4528D373A9A504417 (void);
// 0x00000166 System.Void UnityEngine.Advertisements.Events.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_m51A040B1198DE55DDFC1561822066C1824C5D11F (void);
// 0x00000167 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load()
extern void Banner_Load_m8F0B89644A7E8227B661B16BFC466734B30D3864 (void);
// 0x00000168 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m679202DA86C37B386E05A4B4E89760E684AF9E31 (void);
// 0x00000169 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String)
extern void Banner_Load_m4327AC4CECD3360C935F5EAA5D621DB5274D6E25 (void);
// 0x0000016A System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m513306F8AE10F6863F5F9707E09B4EDF70475D4B (void);
// 0x0000016B System.Void UnityEngine.Advertisements.Advertisement/Banner::Show()
extern void Banner_Show_mB92E83A3E1D74B4C6BD29764C34A2BF70C3CA015 (void);
// 0x0000016C System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m2BD2E70418544D5CE1537182E478A7EEDD21869E (void);
// 0x0000016D System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
extern void Banner_Show_mE14BF0CF411B798B10284082AF4176CC95CAE266 (void);
// 0x0000016E System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_mE068BD7E7550B6BC43EFE62B697BB766D06C57F8 (void);
// 0x0000016F System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_m6DB127A82F9917B8FE25D6B7520602A3490F3C72 (void);
// 0x00000170 System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_mE2025E86B4232B95C40490E92A7BA6E254705118 (void);
// 0x00000171 System.Boolean UnityEngine.Advertisements.Advertisement/Banner::get_isLoaded()
extern void Banner_get_isLoaded_mBAE619B911977002066D1BBE1F79C2EEB7BC22DA (void);
// 0x00000172 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m48FA11114E8D24B15C4D33F2AA28326B995A07CB (void);
// 0x00000173 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::<UnityAdsBannerDidShow>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m0244AE6260E7538360458B780E7F23B859B71022 (void);
// 0x00000174 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mFAA0F4B67C2D46A305EDCAD38B531B038BEBAB54 (void);
// 0x00000175 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::<UnityAdsBannerDidHide>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mC569F24D68F709C1513557CD97D5AAEF33EAAD4D (void);
// 0x00000176 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m23EF0FBDAFAEF1705835E8D724B8964D5A56AF0A (void);
// 0x00000177 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::<UnityAdsBannerClick>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m972E62511BDE0E578161F3670852088E39EB4198 (void);
// 0x00000178 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mA6EDA97CB3BD8311F21206FC9406D6028F968285 (void);
// 0x00000179 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::<UnityAdsBannerDidLoad>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_mF47C847B6465037DDE1D6648DAA1A663111EE629 (void);
// 0x0000017A System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m13D5E505097D9B978F57A13921512945FCFFAEB2 (void);
// 0x0000017B System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::<UnityAdsBannerDidError>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m05225FDEE16AC76F5A9DEB5CEC653B526DEB2470 (void);
// 0x0000017C System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m554496BBFAF6090101C451EFC3DDA9D4B28AFDD9 (void);
// 0x0000017D System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_mD53ABCE2D8E6D083E691BC14B84F73B4C6A6C0D0 (void);
// 0x0000017E System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LoadCallback_BeginInvoke_m26602CBA25163AE693DAFDF99620BF81C71BF2C5 (void);
// 0x0000017F System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::EndInvoke(System.IAsyncResult)
extern void LoadCallback_EndInvoke_mD80580D2BC1B7700CE24D03AF8CA6F1311B50F94 (void);
// 0x00000180 System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_m5F6C76E2C1A0E69E0279487EBB9FDB24C0856662 (void);
// 0x00000181 System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_m701B4DC15718D7F38A6E9922C6C93ADB294ABB75 (void);
// 0x00000182 System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ErrorCallback_BeginInvoke_mDEDC2599D76EFE86DAF0A2240A69F155EE32AB17 (void);
// 0x00000183 System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::EndInvoke(System.IAsyncResult)
extern void ErrorCallback_EndInvoke_mCC702738417912E72A46EA204D43E05F448CEA27 (void);
// 0x00000184 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m20E3DE6D818E1B6ED007489BCBD94A08450C41AB (void);
// 0x00000185 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_mB376D0DAF03D14D5E9BE688CF45C46AD838F2665 (void);
// 0x00000186 System.IAsyncResult UnityEngine.Advertisements.BannerOptions/BannerCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void BannerCallback_BeginInvoke_m4B1587162594D1A58508A1D4101B413525C8F291 (void);
// 0x00000187 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::EndInvoke(System.IAsyncResult)
extern void BannerCallback_EndInvoke_m94947BDB3EF5E6400E579953A0CE3BD29B9573CC (void);
// 0x00000188 System.Void UnityEngine.Advertisements.IosShowListener/ShowFailureCallback::.ctor(System.Object,System.IntPtr)
extern void ShowFailureCallback__ctor_mABD89A9FE9FDB2620713964214421C64C38334D7 (void);
// 0x00000189 System.Void UnityEngine.Advertisements.IosShowListener/ShowFailureCallback::Invoke(System.IntPtr,System.String,System.Int32,System.String)
extern void ShowFailureCallback_Invoke_mD209237DC1A5B9B00FFDAF4FE4191F17FBA1239A (void);
// 0x0000018A System.IAsyncResult UnityEngine.Advertisements.IosShowListener/ShowFailureCallback::BeginInvoke(System.IntPtr,System.String,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void ShowFailureCallback_BeginInvoke_m82099083389E2ACD46ED4584E25BE8FA85A2EF03 (void);
// 0x0000018B System.Void UnityEngine.Advertisements.IosShowListener/ShowFailureCallback::EndInvoke(System.IAsyncResult)
extern void ShowFailureCallback_EndInvoke_mDCEBCBD00168DF7BEBFA32F4BD493633B4D274D2 (void);
// 0x0000018C System.Void UnityEngine.Advertisements.IosShowListener/ShowStartCallback::.ctor(System.Object,System.IntPtr)
extern void ShowStartCallback__ctor_m306F97E7A6DECBF231574CB30D9F512892113D20 (void);
// 0x0000018D System.Void UnityEngine.Advertisements.IosShowListener/ShowStartCallback::Invoke(System.IntPtr,System.String)
extern void ShowStartCallback_Invoke_mD7AE456A08CB5AC4666FF067D948BF8FE32AF0D9 (void);
// 0x0000018E System.IAsyncResult UnityEngine.Advertisements.IosShowListener/ShowStartCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void ShowStartCallback_BeginInvoke_mE399B86E011C48424B62D6ABE31360C85B28A85D (void);
// 0x0000018F System.Void UnityEngine.Advertisements.IosShowListener/ShowStartCallback::EndInvoke(System.IAsyncResult)
extern void ShowStartCallback_EndInvoke_m52D271424D8BC28793F6E2A5939F7EB5219AA9F0 (void);
// 0x00000190 System.Void UnityEngine.Advertisements.IosShowListener/ShowClickCallback::.ctor(System.Object,System.IntPtr)
extern void ShowClickCallback__ctor_m5C78698DEA92CA52A0FD629BDB46D0692BB0DD67 (void);
// 0x00000191 System.Void UnityEngine.Advertisements.IosShowListener/ShowClickCallback::Invoke(System.IntPtr,System.String)
extern void ShowClickCallback_Invoke_m6D4B721CD1D6D649D957F446BE366BD34B5C9945 (void);
// 0x00000192 System.IAsyncResult UnityEngine.Advertisements.IosShowListener/ShowClickCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void ShowClickCallback_BeginInvoke_mD52C3A7FEEC9BF4A0E20CA759C772F94EE5064C5 (void);
// 0x00000193 System.Void UnityEngine.Advertisements.IosShowListener/ShowClickCallback::EndInvoke(System.IAsyncResult)
extern void ShowClickCallback_EndInvoke_mA101B46A9D755DF51E3721CC118EEF5112AF6193 (void);
// 0x00000194 System.Void UnityEngine.Advertisements.IosShowListener/ShowCompleteCallback::.ctor(System.Object,System.IntPtr)
extern void ShowCompleteCallback__ctor_m7E32BCE6D7DEAB85EAB2DB6CB3CE1DE440B200AA (void);
// 0x00000195 System.Void UnityEngine.Advertisements.IosShowListener/ShowCompleteCallback::Invoke(System.IntPtr,System.String,System.Int32)
extern void ShowCompleteCallback_Invoke_m3E6A77E1DA0A2E8484C2BD25D3CDEC5B0B1BB500 (void);
// 0x00000196 System.IAsyncResult UnityEngine.Advertisements.IosShowListener/ShowCompleteCallback::BeginInvoke(System.IntPtr,System.String,System.Int32,System.AsyncCallback,System.Object)
extern void ShowCompleteCallback_BeginInvoke_m87B7BDE93BE68284BCA218DB2D44C40E13D26670 (void);
// 0x00000197 System.Void UnityEngine.Advertisements.IosShowListener/ShowCompleteCallback::EndInvoke(System.IAsyncResult)
extern void ShowCompleteCallback_EndInvoke_m6C450B449C84C5ECB350F2B5B2BE4B59E3BDEF26 (void);
// 0x00000198 System.Boolean UnityEngine.Advertisements.Utilities.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m70F6E82AE18BF5F9F067BDBDEA1ED2DB7A94B76B (void);
// 0x00000199 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m27A421BE339A651A13124EF5ACDE518D8E92CC59 (void);
// 0x0000019A System.Object UnityEngine.Advertisements.Utilities.Json/Parser::Parse(System.String)
extern void Parser_Parse_m0F27CDB228E7CB42C18F79D26CC119A17A8ACF0B (void);
// 0x0000019B System.Void UnityEngine.Advertisements.Utilities.Json/Parser::Dispose()
extern void Parser_Dispose_m6CF938F856463B3DC41714F68A69A2AEBB1532F4 (void);
// 0x0000019C System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseObject()
extern void Parser_ParseObject_m6E788343570EB3362AED3F4C5A11F235155EFC4F (void);
// 0x0000019D System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseArray()
extern void Parser_ParseArray_m497AF5D1134DA5540EE4670DCAF401346A2FF95B (void);
// 0x0000019E System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseValue()
extern void Parser_ParseValue_mBED236CF835AAB4095A87F4EB7BB21B7D7A1D4DC (void);
// 0x0000019F System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseByToken(UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN)
extern void Parser_ParseByToken_m092294F27563FAD18A25B0975680E85E50203E83 (void);
// 0x000001A0 System.String UnityEngine.Advertisements.Utilities.Json/Parser::ParseString()
extern void Parser_ParseString_mBCE35E8FD99DDBEEBF9537D38B0B546C90A9C774 (void);
// 0x000001A1 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_m07C532E02190931EB5A54F96FBA177283D5A22DF (void);
// 0x000001A2 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mD4D99A568FD15265DE960FE01122751504D2D7A8 (void);
// 0x000001A3 System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m3563C8C6266C03DC2104FC0B3061D722D2C1B959 (void);
// 0x000001A4 System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m99421292EED335E18206C0287DE40FC95883B380 (void);
// 0x000001A5 System.String UnityEngine.Advertisements.Utilities.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m4C2E8FE9FFA6DD1FC4D368E3C1006D7325ABFCC0 (void);
// 0x000001A6 UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN UnityEngine.Advertisements.Utilities.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m8AC572401758AA21CFC724457DEB519C50F568CD (void);
// 0x000001A7 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::.ctor()
extern void Serializer__ctor_mF5D0C61AC8422CAD932288C50754D358FFD36DA8 (void);
// 0x000001A8 System.String UnityEngine.Advertisements.Utilities.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m3237FDAC16D69EFAD4FB76BD806F960CA494CE93 (void);
// 0x000001A9 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m4F242828AFC466BD8B228175F0ACAADD628F489F (void);
// 0x000001AA System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mFB4B7841D716E4B7093909041663C166A442911F (void);
// 0x000001AB System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_mAC12E5C8ADABD35D2B02A2C105D7CA1D4EAD895A (void);
// 0x000001AC System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mD46149CB5C25AA7E0C33E2E1655193080EC6E45E (void);
// 0x000001AD System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m34AB8182921EC7E05370F7401B8C36383BEA3FCC (void);
// 0x000001AE System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingDidInitiatePurchasingCommand::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingDidInitiatePurchasingCommand__ctor_mFF13A4A0FC3A957CCD8F13D49E034B720038D6F0 (void);
// 0x000001AF System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingDidInitiatePurchasingCommand::Invoke(System.String)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_Invoke_m65BF52AA54F79DB8AE555DA347327E4D1B9C21DD (void);
// 0x000001B0 System.IAsyncResult UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingDidInitiatePurchasingCommand::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_BeginInvoke_m166DBB665CEA894D676EC5738083A88401D0C061 (void);
// 0x000001B1 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingDidInitiatePurchasingCommand::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingDidInitiatePurchasingCommand_EndInvoke_m71014DEDA1684F2E444DCCB206FC8D80DC264085 (void);
// 0x000001B2 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetProductCatalog::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingGetProductCatalog__ctor_m49DC4805DB241B51950BA314492192B807388AF8 (void);
// 0x000001B3 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetProductCatalog::Invoke()
extern void unityAdsPurchasingGetProductCatalog_Invoke_mE9B561D5D561143608BEEC5BF3243B16B2C0B2F3 (void);
// 0x000001B4 System.IAsyncResult UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetProductCatalog::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingGetProductCatalog_BeginInvoke_m64F8D25D0F3A26F652BD21C73BC0618685CD6127 (void);
// 0x000001B5 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetProductCatalog::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingGetProductCatalog_EndInvoke_mE62125AB227137F0755586FAC2E88C688FE33258 (void);
// 0x000001B6 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetPurchasingVersion::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingGetPurchasingVersion__ctor_m08135B89E3766213671E4E16F2055BEA920C0FD3 (void);
// 0x000001B7 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetPurchasingVersion::Invoke()
extern void unityAdsPurchasingGetPurchasingVersion_Invoke_m39CDF8A39FED49EA0DE7CB76E6366E74255D78F0 (void);
// 0x000001B8 System.IAsyncResult UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetPurchasingVersion::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingGetPurchasingVersion_BeginInvoke_m73431C230B300FBBB40612E64A48E4A1748A0605 (void);
// 0x000001B9 System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingGetPurchasingVersion::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingGetPurchasingVersion_EndInvoke_m612539347E18DADA8C4FA720D0656E2604408BA4 (void);
// 0x000001BA System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingInitialize::.ctor(System.Object,System.IntPtr)
extern void unityAdsPurchasingInitialize__ctor_mFDEF83AB48F82954C8B2818D7CB2574E1F459953 (void);
// 0x000001BB System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingInitialize::Invoke()
extern void unityAdsPurchasingInitialize_Invoke_mCC266D739326D84559CE94E48AA843460FE5FC6A (void);
// 0x000001BC System.IAsyncResult UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingInitialize::BeginInvoke(System.AsyncCallback,System.Object)
extern void unityAdsPurchasingInitialize_BeginInvoke_mAB2A6B01F22CA9B82A1468914AF2F6D005715DE5 (void);
// 0x000001BD System.Void UnityEngine.Advertisements.Purchasing.PurchasingPlatform/unityAdsPurchasingInitialize::EndInvoke(System.IAsyncResult)
extern void unityAdsPurchasingInitialize_EndInvoke_mD2EBC7FB08515D311E9656E6CF8C248E961980E3 (void);
// 0x000001BE System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mF2B6358160847184E602DF6FD693218A470329E0 (void);
// 0x000001BF System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_1::.ctor()
extern void U3CU3Ec__DisplayClass33_1__ctor_m116A199DD6E00689B16197D06B47C06C996E29C8 (void);
// 0x000001C0 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_1::<Show>b__0(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void U3CU3Ec__DisplayClass33_1_U3CShowU3Eb__0_m04366E1087F5360B5D74D38DF742E089265AED70 (void);
// 0x000001C1 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m37143F5B7AD0E1C937C1931A2FDFCF76F8EC5A8D (void);
// 0x000001C2 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::<UnityAdsReady>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CUnityAdsReadyU3Eb__0_mF0C63ACED7644FA87E01EBA2DB5A4CC7A66A18CB (void);
// 0x000001C3 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mD9E8DC2B4EFBB6BE12549EBFAD5859AE891E64C1 (void);
// 0x000001C4 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::<UnityAdsDidError>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidErrorU3Eb__0_m7C7A32DB133CEA8C48D75B5EFBA84D823A16748A (void);
// 0x000001C5 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_mC22053AEA31D532E87491E7E4D299088B53903AB (void);
// 0x000001C6 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::<UnityAdsDidStart>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidStartU3Eb__0_m2E7BA09775E7525FA6C86C0C9F9B7437230339FE (void);
// 0x000001C7 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m6B6AAA0DA992112F815D94A678A37BA27F72031B (void);
// 0x000001C8 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass43_0::<UnityAdsDidFinish>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CUnityAdsDidFinishU3Eb__0_m671FB4861A8951BE89DB6DFBE9AB0365A210B3AD (void);
// 0x000001C9 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerShowDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerShowDelegate__ctor_m0CC6C088AF8AD82567327FDAA576502918E23B90 (void);
// 0x000001CA System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerShowDelegate::Invoke(System.String)
extern void UnityAdsBannerShowDelegate_Invoke_mE3BF1DEEEF5CA9237E7FCAFC0B9CD961EAC4E8D1 (void);
// 0x000001CB System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerShowDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerShowDelegate_BeginInvoke_m628F7A8CEAC14638F61C5EA07F0455427399F47F (void);
// 0x000001CC System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerShowDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerShowDelegate_EndInvoke_m0878AEDC615D91C378D4DAAD41449D617A8C1977 (void);
// 0x000001CD System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerHideDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerHideDelegate__ctor_mF03058B05B24B78DA5A1132B99EBBC1F09B9522D (void);
// 0x000001CE System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerHideDelegate::Invoke(System.String)
extern void UnityAdsBannerHideDelegate_Invoke_m4C7BC7D90AA9CD2C80F230984B246CC9961A3EF9 (void);
// 0x000001CF System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerHideDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerHideDelegate_BeginInvoke_m656EC42544F2BBE87BF7ADE5A9ED890236C79151 (void);
// 0x000001D0 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerHideDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerHideDelegate_EndInvoke_m49BC5649D0A60DF628309288ED866E0CD9814CC0 (void);
// 0x000001D1 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerClickDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerClickDelegate__ctor_m703270CF4F72AC0D64A5BFEAF1924590E66436B2 (void);
// 0x000001D2 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerClickDelegate::Invoke(System.String)
extern void UnityAdsBannerClickDelegate_Invoke_m61285605E80AA802CA9D72E876BA68FCE8A79ACB (void);
// 0x000001D3 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerClickDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerClickDelegate_BeginInvoke_m8515C2FBAD83B71BECD5AD4764CEF50AA1FCF0C0 (void);
// 0x000001D4 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerClickDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerClickDelegate_EndInvoke_mA6921F240F409E474F90D8B23F5CA506FC1F3E83 (void);
// 0x000001D5 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerUnloadDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerUnloadDelegate__ctor_m5F3A24005669043056E47533C0DAE81153EDF77B (void);
// 0x000001D6 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerUnloadDelegate::Invoke(System.String)
extern void UnityAdsBannerUnloadDelegate_Invoke_mF2E7F6BFB16633F8FF7696491D191D84A8097CEF (void);
// 0x000001D7 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerUnloadDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerUnloadDelegate_BeginInvoke_mC19ADBA7277C5FD4E80AB8C382855A0EE215DCD7 (void);
// 0x000001D8 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerUnloadDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerUnloadDelegate_EndInvoke_m00C956E4CC3AD5B3EF55F78D9D0AFD744E449DD5 (void);
// 0x000001D9 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerLoadDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerLoadDelegate__ctor_m91AEFBD01369511101AA445344C327D8C8D2DB2E (void);
// 0x000001DA System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerLoadDelegate::Invoke(System.String)
extern void UnityAdsBannerLoadDelegate_Invoke_mE8D0944CDA67EB8B3C424052FB4E48ECC117AF14 (void);
// 0x000001DB System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerLoadDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerLoadDelegate_BeginInvoke_mA0E9FDE74CE2ACDED017D119F0247E8A41CCACC1 (void);
// 0x000001DC System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerLoadDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerLoadDelegate_EndInvoke_m8E75F7C70DC378E69F78A298EF0666B774D21D54 (void);
// 0x000001DD System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerErrorDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsBannerErrorDelegate__ctor_m26E223E378F2230181A1AE75D958AC492B89F8A0 (void);
// 0x000001DE System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerErrorDelegate::Invoke(System.String)
extern void UnityAdsBannerErrorDelegate_Invoke_m7982B1E113C92BB6E4725EF720788A8179C216B6 (void);
// 0x000001DF System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerErrorDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsBannerErrorDelegate_BeginInvoke_m257D01AAB86D60024594F5D086B937D1F315EF96 (void);
// 0x000001E0 System.Void UnityEngine.Advertisements.Platform.iOS.IosBanner/UnityAdsBannerErrorDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsBannerErrorDelegate_EndInvoke_mEE20DE6B16CA462EAC98DD468C82FFEA984679CC (void);
// 0x000001E1 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitSuccessCallback::.ctor(System.Object,System.IntPtr)
extern void InitSuccessCallback__ctor_mA89A2E55F84D6A6E87A21CE44E56FCADAAE93974 (void);
// 0x000001E2 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitSuccessCallback::Invoke(System.IntPtr)
extern void InitSuccessCallback_Invoke_m8675E974F7B4A82709024534508B7D1AB8831ED0 (void);
// 0x000001E3 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitSuccessCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void InitSuccessCallback_BeginInvoke_mE43C5BE2C100020F7796FEC223006EA3D09732D8 (void);
// 0x000001E4 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitSuccessCallback::EndInvoke(System.IAsyncResult)
extern void InitSuccessCallback_EndInvoke_m0BCE620369DC8414CAD2103EF00206103CA6DE65 (void);
// 0x000001E5 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitFailureCallback::.ctor(System.Object,System.IntPtr)
extern void InitFailureCallback__ctor_mEB6BA365EE12928514CEE27B38A0C7A4A3D31144 (void);
// 0x000001E6 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitFailureCallback::Invoke(System.IntPtr,System.Int32,System.String)
extern void InitFailureCallback_Invoke_m954A61418AFF6045FA2E02599B67D9E99D119B3E (void);
// 0x000001E7 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitFailureCallback::BeginInvoke(System.IntPtr,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void InitFailureCallback_BeginInvoke_m8742CA3DF5FDA4009D0027A85B33DA1A242C595C (void);
// 0x000001E8 System.Void UnityEngine.Advertisements.Platform.iOS.IosInitializationListener/InitFailureCallback::EndInvoke(System.IAsyncResult)
extern void InitFailureCallback_EndInvoke_m7B2C97054CD64DA05A19421DCA1707353461F712 (void);
// 0x000001E9 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadSuccessCallback::.ctor(System.Object,System.IntPtr)
extern void LoadSuccessCallback__ctor_m0E059E7BD3E47FCEA2C5461FBF1E76AA82DF15FF (void);
// 0x000001EA System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadSuccessCallback::Invoke(System.IntPtr,System.String)
extern void LoadSuccessCallback_Invoke_m3749425EEBD3138836A72138B8341DFC68E7A70F (void);
// 0x000001EB System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadSuccessCallback::BeginInvoke(System.IntPtr,System.String,System.AsyncCallback,System.Object)
extern void LoadSuccessCallback_BeginInvoke_m07CAA25EE81F085427FAA28458408B504CB55EF4 (void);
// 0x000001EC System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadSuccessCallback::EndInvoke(System.IAsyncResult)
extern void LoadSuccessCallback_EndInvoke_mBB4A7AB192BC98F8037ED8C542439E4B6407D9D4 (void);
// 0x000001ED System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadFailureCallback::.ctor(System.Object,System.IntPtr)
extern void LoadFailureCallback__ctor_m0579B893FBCB70E04FDA64D705507D30153021CB (void);
// 0x000001EE System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadFailureCallback::Invoke(System.IntPtr,System.String,System.Int32,System.String)
extern void LoadFailureCallback_Invoke_m561153FA7B42A862AA78042C64E4C6A63B92BC68 (void);
// 0x000001EF System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadFailureCallback::BeginInvoke(System.IntPtr,System.String,System.Int32,System.String,System.AsyncCallback,System.Object)
extern void LoadFailureCallback_BeginInvoke_mBA44D145FF3A5CD2B7FD4A60BCDF8E119FEB9E32 (void);
// 0x000001F0 System.Void UnityEngine.Advertisements.Platform.iOS.IosLoadListener/LoadFailureCallback::EndInvoke(System.IAsyncResult)
extern void LoadFailureCallback_EndInvoke_mD6EC67174D69F153B5E66571CACE905470424A41 (void);
// 0x000001F1 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsReadyDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsReadyDelegate__ctor_m0FDFC0CE23F22D3B3537FE82F957B907363F5771 (void);
// 0x000001F2 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsReadyDelegate::Invoke(System.String)
extern void UnityAdsReadyDelegate_Invoke_mA844E3E8CF864BFD6231FF13F6CD47784D3007C4 (void);
// 0x000001F3 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsReadyDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsReadyDelegate_BeginInvoke_m00CFF1B2A50823F7339C7B329F4932828DEB0541 (void);
// 0x000001F4 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsReadyDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsReadyDelegate_EndInvoke_m584CFB56CB6BBAC28FF40E07C3B953E77D0D2FF2 (void);
// 0x000001F5 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidErrorDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsDidErrorDelegate__ctor_m67D062A988F87DFCAD003B229C6C5BE117C3E6F8 (void);
// 0x000001F6 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidErrorDelegate::Invoke(System.Int64,System.String)
extern void UnityAdsDidErrorDelegate_Invoke_mD57353F02B825394274C30CAA965946BBFFB0108 (void);
// 0x000001F7 System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidErrorDelegate::BeginInvoke(System.Int64,System.String,System.AsyncCallback,System.Object)
extern void UnityAdsDidErrorDelegate_BeginInvoke_m4F136E17CF929CCDA944B573A33C3FD0FE986071 (void);
// 0x000001F8 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidErrorDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsDidErrorDelegate_EndInvoke_mAEC544591F3CE2DD0CBD75B8EDD5BA13C9AE4C4C (void);
// 0x000001F9 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidStartDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsDidStartDelegate__ctor_m5DFE41CFC206FFD54E3B941694AE7CBE69A3614B (void);
// 0x000001FA System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidStartDelegate::Invoke(System.String)
extern void UnityAdsDidStartDelegate_Invoke_mCF9BE5EA3C4AD1C6364CCE473008F0BC9FE0E77F (void);
// 0x000001FB System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidStartDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void UnityAdsDidStartDelegate_BeginInvoke_mE8B31E7888C1C58BBEAEF415D7D76D741B881D1F (void);
// 0x000001FC System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidStartDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsDidStartDelegate_EndInvoke_m5BDAD87414A96F786168D30FFD54B8DEFAB10A5F (void);
// 0x000001FD System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidFinishDelegate::.ctor(System.Object,System.IntPtr)
extern void UnityAdsDidFinishDelegate__ctor_m7D2D98B40574B22EB9774101F6601AC87F5914E2 (void);
// 0x000001FE System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidFinishDelegate::Invoke(System.String,System.Int64)
extern void UnityAdsDidFinishDelegate_Invoke_m3E4FF514A8F6646DEE8FA99F7B1A66E1C63AD7B3 (void);
// 0x000001FF System.IAsyncResult UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidFinishDelegate::BeginInvoke(System.String,System.Int64,System.AsyncCallback,System.Object)
extern void UnityAdsDidFinishDelegate_BeginInvoke_m03CAF2322DEEE64EC9E2FC58E4926FA7CA3242DF (void);
// 0x00000200 System.Void UnityEngine.Advertisements.Platform.iOS.IosPlatform/UnityAdsDidFinishDelegate::EndInvoke(System.IAsyncResult)
extern void UnityAdsDidFinishDelegate_EndInvoke_m20EE3F37979F3F473DD1A867507B9AFA606A458A (void);
static Il2CppMethodPointer s_methodPointers[512] = 
{
	Advertisement__cctor_m48D03B84C3797DACB3B611DF970CB0521C03BF4C,
	Advertisement_get_isInitialized_m47A672AC12BE72BAE9CD9E229097E1818F5ED0F9,
	Advertisement_get_isSupported_mD36A0B407C54E3D5B7F4600564CE601ED4AE4A0A,
	Advertisement_get_debugMode_mE6031D65A8EC421FBDFFCE90C854C34645F68984,
	Advertisement_set_debugMode_mC42A15A9B574852BDCAE39A40C8F2801A58093E7,
	Advertisement_get_version_m461B0C624EA4E088FC722D9E8D6B38FF3261CF1A,
	Advertisement_get_isShowing_m170A1D13DC4F43AD95096C38796343381DCA5A3D,
	Advertisement_Initialize_mE87487FCF006CB2834ABD315CE6996DD63A5E330,
	Advertisement_Initialize_m5A9C9564978427DBB41473E96F48065EF68ACA0C,
	Advertisement_Initialize_mEADA718BF67610D6B3C6B601A14622C2909AF67D,
	Advertisement_Initialize_mF357DA90936A525920FBE7B21E081EF547DEC2FD,
	Advertisement_IsReady_m330CE055D99555EBB3A33C71153125888FCDDB46,
	Advertisement_IsReady_m3DF7CE9A242B25940D728926268576148544F0F5,
	Advertisement_Load_m295F9B24B0DEB55DAC61D10D3EC9EE75BEA50993,
	Advertisement_Load_m40056FD79F3FC90822A7087A2BFA3E4104B1D20F,
	Advertisement_Show_m93B5E5ACC8790D0EDCBC47C5B67835DD3DF62C2A,
	Advertisement_Show_m3CC9BFDA5143CBC3F4FDF578CF3173C744ADFC20,
	Advertisement_Show_mAEACF55C73187D8A8EF8C7B25FDC6F3D6A5F0BF6,
	Advertisement_Show_mE3DA5B6D9D7608A732596F280C65F284D849381A,
	Advertisement_Show_m12A1239AE0C4E1C01F82EFA8179365165138BB99,
	Advertisement_Show_m29855BEE0C3270283DEA90C864D9340D008F62A9,
	Advertisement_SetMetaData_m0FED87249A11E3D1A05B52F5EFB135352E6C2249,
	Advertisement_AddListener_m213FC840853606FCEA3769A612311B82D9A5F3E3,
	Advertisement_RemoveListener_m230106A6206FD8998928618D649DD443C182E40A,
	Advertisement_GetPlacementState_mF59A05D6FF5DC7730678A7D2E4547CFF13CE7B63,
	Advertisement_GetPlacementState_mAB71FF7C5B7BD367D7CCA532DCEB8AC92F9161C5,
	Advertisement_CreatePlatform_m4B66F507B4A5AABD2B5830A5DC9149C59AD7FDF6,
	Advertisement_IsSupported_m6A94CAB1E4B1DEE470DB57D39A071D1A01A9DA67,
	Banner_get_UnityLifecycleManager_m5BC3D154673DB285E17084D37FC567525A8764A8,
	Banner_get_IsLoaded_mD775E3B877C9911F79F6A726404719A1308B4DBF,
	Banner_get_ShowAfterLoad_mD282F4189D9F0A1D85FC77107D1158E1EB2336D6,
	Banner_set_ShowAfterLoad_mC19C331A023B37A66A419F933E0F6353875AC237,
	Banner__ctor_mCC977576124FE54F3E3A32308979533B94695A13,
	Banner_Load_mAA9305BC1EFA7954A9282A92B2DB97E981747E20,
	Banner_Show_m58B612A96923452F31968DC293D071D847E7460B,
	Banner_Hide_m590E1E55AE8A031ED3BC507859C83B478AAD7653,
	Banner_SetPosition_mB161D7A3B2D73EA3ADDB1194BD8F99028A672FA1,
	Banner_UnityAdsBannerDidShow_mB7E3B7E0D2EB66BCFC56BA1279F2A6EE65248858,
	Banner_UnityAdsBannerDidHide_m74F905A270B98C9601FA64BB9A29234D8B8C5ABA,
	Banner_UnityAdsBannerClick_m4CD3381A44221C62DDFB9320F8E7CD075AA6F3C2,
	Banner_UnityAdsBannerDidLoad_mDDBB1273EF8F488A9B4A178FCECFE111220FCEF5,
	Banner_UnityAdsBannerDidError_m6A22953F1C113B2DF7243919343A0F8282E67B54,
	BannerLoadOptions_get_loadCallback_mE318D088BF76C1C5CB22F9C1B2F4BE57EECF0D90,
	BannerLoadOptions_set_loadCallback_m93809B6A66C8B3E5747AEAD35E58DE78D752DE06,
	BannerLoadOptions_get_errorCallback_mABEFF7103EA3F6272DE6867A9BCE0EE44DD0840B,
	BannerLoadOptions_set_errorCallback_mCDB17872657BD6FE53A69C8C9326566CCC986A1F,
	BannerLoadOptions__ctor_mF832BEDDEB676AEE29616ED6D8E531F372D98EE0,
	BannerOptions_get_showCallback_mA4703F614E146D18EFD0A204843396C674DFBFDE,
	BannerOptions_set_showCallback_mEB883111DE1D6F3B76B10627A250FD5CCA903AE7,
	BannerOptions_get_hideCallback_mDE0057958F972BD1188DB8F69DF5DBD8DB634EA3,
	BannerOptions_set_hideCallback_mE8F4A793E52B8EF17AEED8CD5FBD8A009486F611,
	BannerOptions_get_clickCallback_m03D90971283749E182F40D53C1CFCA6AEAC09246,
	BannerOptions_set_clickCallback_mEB7EBBCB56DFE68E39592A79F6C7E3D267C79EC0,
	BannerOptions__ctor_mEA79B39E2DA91FAC7FB2001EF81CA0E783F49188,
	Configuration_get_enabled_m98287C3271DAB362A4D14194193E16BE265EC296,
	Configuration_get_defaultPlacement_m6EC53DF961903FD991249184CC6E070887BB43D1,
	Configuration_get_placements_m68FC06EA30716F2F99776D030F26D9CF7B6360BD,
	Configuration__ctor_m83CCC987B3D5FFFFBB56E7794625389A04F76702,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_mDD39009D6DEA6148686E0A512D3EB623E8387BC0,
	MetaData_set_category_m3770DB996FBC6CE4816FF9E2EA2119DD6BE7EFC0,
	MetaData__ctor_m7E11048810D5CB83484D8191D9DAF2FFED3CE56A,
	MetaData_Set_m4A76D3C5FB14E8A34CFC8C213F171C3C99574F8A,
	MetaData_Get_mDF0ACBAE6B5A8AA3F8CBE4A0992B62121DB53395,
	MetaData_Values_m80B9432000D2F381DD17567CEED0623C31DDF835,
	MetaData_ToJSON_mCE15614B67E51F460947861DA6239A1E6577338F,
	AndroidInitializationListener__ctor_m2B67FC562FC0DF7606FDF8D3495DF2FC668CB972,
	AndroidInitializationListener_onInitializationComplete_mB0889527DB1AFF1C4F84CBEF6C065C0DC2C21311,
	AndroidInitializationListener_onInitializationFailed_mCA337217A3225397327193197F67F57A7A2BD331,
	AndroidLoadListener__ctor_m36863855640FC57ADAD5565D155769A7A7C8732D,
	AndroidLoadListener_onUnityAdsAdLoaded_m8E7009B8A5D23D866FE84DA14EB06300C4F39CCF,
	AndroidLoadListener_onUnityAdsFailedToLoad_mA75A0DBC8839D25196C62E1B1DA3D626A8A03B43,
	AndroidShowListener__ctor_mD258E23940C40514E2955EA95FA56863B6111E94,
	AndroidShowListener_onUnityAdsShowFailure_m7BB848249D6B5BBA1403FAA3E980F412FB2FEF1D,
	AndroidShowListener_onUnityAdsShowStart_m04C4230BC038588AB161E8A4AED7F1536DD8F153,
	AndroidShowListener_onUnityAdsShowClick_mE4F01FF4C447AAE25184CADB7093ECCC6BDE40B7,
	AndroidShowListener_onUnityAdsShowComplete_m6BC05791EDF7D5C98B63A01421FC31E2612AC2D3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IosShowListener_ShowListenerCreate_mC7BC91324D71CBB89B3D3CBC05C52E3BB850BCA6,
	IosShowListener_ShowListenerDestroy_m0B501669515832065870FB64247CCB96EEE14176,
	IosShowListener__ctor_m8F94D4B5E98A3B4D064FA7F4091652CFEC7151E1,
	IosShowListener_Dispose_mD19AF3F2E67DE19C1491C1D8C6238438A46BE03C,
	IosShowListener_OnShowFailure_m5B3E4A8B8649B42B3715FA6B8A10F587F27C51B5,
	IosShowListener_OnShowStart_mFBFB83F0E8D8B306563D6B77A8CAD9DA32CD4BE6,
	IosShowListener_OnShowClick_mD0A8B5A8F4B2575848A97AC3F61CBF55BED99EE4,
	IosShowListener_OnShowComplete_m27510514DF576FE90E059379DF79891A5DE83262,
	IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78,
	IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA,
	IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699,
	IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24,
	ShowOptions_get_resultCallback_m2B88B2843F0F244FC88D0A426078BAFA0B80BAEF,
	ShowOptions_set_resultCallback_m18C5A6A59822D5455650792279D3C445433881C8,
	ShowOptions_get_gamerSid_mE27D258CAEE96BB44D32B969E05B75596659A65D,
	ShowOptions_set_gamerSid_m33375B215C04509B722A9EFB974F44CEAEDA430E,
	ShowOptions__ctor_mE5966E77A7DE48D71B6C338A76B50DFA2835724C,
	ApplicationQuit_add_OnApplicationQuitEventHandler_m7A860E8393CE6B8A559C8BD6B1CAC6C206CB47AE,
	ApplicationQuit_remove_OnApplicationQuitEventHandler_m5046C7F858D0ADC877095EFB6D87D3C2635053A5,
	ApplicationQuit_OnApplicationQuit_m6F4956058FB7974AC9FFF35D5462EF4193D67904,
	ApplicationQuit__ctor_m3D93B6B6AC6C8809FC9446B17AF9C8AC011F4593,
	CoroutineExecutor_Update_m77E0DF17371AA1DB5D974CA65572031A8EC1798C,
	CoroutineExecutor__ctor_m67CBAA9BDFFFA5A7B06D750A2BCC8CE780F63EA1,
	EnumUtilities_GetShowResultsFromCompletionState_m86358BF1FB64B21DE191AC347331004B6DC1E2F3,
	NULL,
	NULL,
	NULL,
	NULL,
	Json_Deserialize_mD2F68749D335BBA41591E53CF8C35A815B132748,
	Json_Serialize_mB474614B0DEFF4888F3E32EA3B0A7F482B4503F9,
	UnityLifecycleManager__ctor_mCE97D5F551D08591644BCE5728375A244754B5BD,
	UnityLifecycleManager_Initialize_m0544A2845C56FC01ED83BC59D8802666BE93306D,
	UnityLifecycleManager_StartCoroutine_m36CAAC3F71BED1EAF22A1C1E6F79E1218312E52B,
	UnityLifecycleManager_Post_mF9F2999D7787AF02D275A5CA5F8BA1C79C7A40C5,
	UnityLifecycleManager_Dispose_mACB4683F3EFB169FB94C2AC1F6995ABAB2B361CA,
	UnityLifecycleManager_SetOnApplicationQuitCallback_m186D695D94D336624545A80E74EBB41022AEA325,
	NULL,
	Purchasing_Initialize_m7104988051354DE77EAA486ABE610D365BA55E0B,
	Purchasing_InitiatePurchasingCommand_mC2FF6E6B0C30012FF716E84B9619068782AECAE5,
	Purchasing_GetPurchasingCatalog_m324BE5D644F1E15110E9F6B71E383AD40135A1CF,
	Purchasing_GetPromoVersion_m81DA685AFE504F91A9ED30D9B9B2360668D10198,
	Purchasing_SendEvent_mD483C0DE4551E9FFD7CA9E54F076A69EF6BFA999,
	Purchasing__cctor_mCD1A03D73F9E06F9932E7BA9785689CD00678B1B,
	PurchasingPlatform_get_Instance_mC0A99720EDD562691CE853E5754F6EBA2EEE8897,
	PurchasingPlatform_set_Instance_m04D177493F157CCA31F3D78A214D2EE7FA43E893,
	PurchasingPlatform_UnityAdsPurchasingDispatchReturnEvent_mB6888E03BB35537C53B525775FCDBBEF7F56354B,
	PurchasingPlatform_UnityAdsSetDidInitiatePurchasingCommandCallback_mD3A1B0971A5FACB8E53F95C3F96EAD87FB40F25C,
	PurchasingPlatform_UnityAdsSetGetProductCatalogCallback_mF34FEFDFA74D55266CDA07EE3FC3A78458BAC4F7,
	PurchasingPlatform_UnityAdsSetGetVersionCallback_m5BEDD73A45D5DFC3188A794F675C3EDEEEA03E9D,
	PurchasingPlatform_UnityAdsSetInitializePurchasingCallback_mA7AAA7AE5C9B9C2D0E8F0F3F9B9266BD3121832D,
	PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D,
	PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B,
	PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0,
	PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326,
	PurchasingPlatform_Initialize_m2D55411EF6FB55346916518D3FA93C2B8C2AE6E2,
	PurchasingPlatform_SendPurchasingEvent_m999E699534B293A534C1CBB1320F24B8770230FB,
	PurchasingPlatform__ctor_mBDF2789AEF2E871306077422037314B68BB58CD4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Platform_add_OnStart_m9280DC72AA715F601CC8F42453750AA8D85E5E33,
	Platform_remove_OnStart_m3BA78A1EDD0A65E06EE0E728CCB4BB6E8F56C434,
	Platform_add_OnFinish_m81C408E141474F16CF3A73223F6522B684D42311,
	Platform_remove_OnFinish_m0D992DAB93D528C8FEA60023259BAFB58A4EEB66,
	Platform_get_Banner_mF6064D7A7CB9D64E65C9593FAACB6F28D978FD63,
	Platform_get_UnityLifecycleManager_mD14F8704ED0F3FB9D111A82D66D046BB3A690132,
	Platform_get_NativePlatform_m8747D533AA2DD7BABFCA2015D43A5B8E1CF0222C,
	Platform_get_IsInitialized_m5BF0DD189B3AB88B59DDB9ACEF7EF601F4785D7F,
	Platform_get_IsShowing_mF73670BCADEFA0425ED32FB54EEF487C0F1B06A5,
	Platform_set_IsShowing_m85CE41F9A701BE1673B1E8CA2DD7621553E6B62A,
	Platform_get_Version_mA263392AF90224E52657035043EFD0470ED2F23C,
	Platform_get_DebugMode_m1953631B9A3EAF854C3B8753D010DE05E9556D5F,
	Platform_set_DebugMode_mE4CB6CA4F2D7E92A699E0D8926D6912F80ABF204,
	Platform_get_Listeners_mF03ACE2F46E871498ED43CF40ECF9D2FE2319BAF,
	Platform__ctor_m25922F47189C27991D974883E786DDAA515D9DAD,
	Platform_Initialize_m869080A548A3AF80091A70CB60EA803F7165615C,
	Platform_Load_m45AB572B2B330E22A6A273719E04392E21A77473,
	Platform_Load_m34FEE4D7C760F0C5E6C3CC9FB4272A9CA9EEF78B,
	Platform_Show_m253CB81E1A6C31AC49FC4B845C81F1661C6CEDAE,
	Platform_Show_m4C78AEDDBFEDE4D464C42EE3508B1EE60BAAE0AF,
	Platform_AddListener_mB26FAF1402C8C18E67B83757301E5FC7EFFA841F,
	Platform_RemoveListener_m4F537A0E1E8CE0A7FCA5E75353E102228B76FA0A,
	Platform_IsReady_mBA9236CC3C70111DF889B7A6EEEDDB81C3D6C8C6,
	Platform_GetPlacementState_mE8AB6D973D48D01DE4322874CC83AF5150AEC849,
	Platform_SetMetaData_mC39A25EFC30F0B7F4C9E84DCAA5E9F75BF7DA159,
	Platform_UnityAdsReady_mABF9A9DF5E0258EAC5DC6C68CDF36BFC75519990,
	Platform_UnityAdsDidError_mC980D544AFB9163E76D96CD509E95F5627E7E924,
	Platform_UnityAdsDidStart_m42E3386B442F882B6946F2EEEABE266967CB713C,
	Platform_UnityAdsDidFinish_mACFA2AFBC03C44C6135E6E4B4A32DE61712B9249,
	Platform_GetClonedHashSet_m551E60E3C39B0B712A480FEA1D55A79100F1D6C4,
	Platform_U3CInitializeU3Eb__30_0_mDE5E1A0F546C3450946CA0CB4AD1C1B8ABB6DABC,
	Platform_U3CInitializeU3Eb__30_1_m77BDEC0A1CA5EC1E012D3903508842CB1E9CA4F1,
	IosBanner_get_IsLoaded_m62338D5AA9B65B2D63094FC8843D834880143DF4,
	IosBanner_set_IsLoaded_m5BAEA747825FB2E5BABCD95058877C804DCEB917,
	IosBanner_UnityAdsBannerShow_m7C796D69AFBB3510DD213A43ABC8A3930126008E,
	IosBanner_UnityAdsBannerHide_m3EF341F8BA3894911947BE1283B98FC625245AE3,
	IosBanner_UnityAdsBannerIsLoaded_m63698D956BE7769B03775F12F7A5BEFE65F99C29,
	IosBanner_UnityAdsBannerSetPosition_mCF09D7291E6AA5E4F36836918D52C56A67DA062C,
	IosBanner_UnityAdsSetBannerShowCallback_m7A06A1CECBB07B0C1CF83F7B3A5662B9A1A80018,
	IosBanner_UnityAdsSetBannerHideCallback_m2F3B0E3153FD22D0A16DE35A5AA11687F6B6638D,
	IosBanner_UnityAdsSetBannerClickCallback_mAA8D05BB10AC2D6A5E5DFCC6D6DAAB27DE9DE18D,
	IosBanner_UnityAdsSetBannerErrorCallback_mF3D856DAC456A1E073AFA53BAAAD40A3EE8454EA,
	IosBanner_UnityAdsSetBannerUnloadCallback_mD2C1B9A58F47F16F48F7E0DDB99464005D104778,
	IosBanner_UnityAdsSetBannerLoadCallback_m3FA7784B7E14ED1C19F6EC688F80B258202A072A,
	IosBanner_UnityBannerInitialize_m9941502BF9585D8185B84A3249CABB97BF7FFE51,
	IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE,
	IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05,
	IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E,
	IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675,
	IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19,
	IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988,
	IosBanner_SetupBanner_mFC8F801CCB2E4845356263B359D309CBE88DC193,
	IosBanner_Load_m34E01C7AA26556D512A9A127D462C03D988C8C30,
	IosBanner_Show_mD76C8EA840957C368A26C3FE9077D5FF7AA34B1F,
	IosBanner_Hide_m0AA12201568983F5621854D382A5B9746BBE5B93,
	IosBanner_SetPosition_m1E8CA92686E7E4C6ED082D4C457493B21AC9C37D,
	IosBanner__ctor_mD10AFF14AED78D9209ADBA2123745944F385C625,
	IosInitializationListener_InitializationListenerCreate_m5D4DDC7387934A291E2D4E6068094DEBAFB770DD,
	IosInitializationListener_InitializationListenerDestroy_mA0CD5150F8911D18B213467E4814953355AD8C7C,
	IosInitializationListener__ctor_mE87193E63262F1FAF968C59229BB365749417868,
	IosInitializationListener_Dispose_mAED43AF8A77FF4C7CA92247523D225E3CA021F35,
	IosInitializationListener_OnInitializationComplete_m08D1A7AC2C7D4E3365524E1AD585FB93BB04BF7C,
	IosInitializationListener_OnInitializationFailed_mCA9F0384011CC5D36ED8D5E0D125A28A0AAC05E7,
	IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4,
	IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E,
	IosLoadListener_LoadListenerCreate_m551945EB6EFC1516DB85850771808C5E17E94A30,
	IosLoadListener_LoadListenerDestroy_mDD880D872CBE2CA92285F81C9EE84F9DDB5395AE,
	IosLoadListener__ctor_mE00D8BBCE3EA83607D91784778B071D116A45076,
	IosLoadListener_Dispose_m622D90E6B3937F72D7175B2E67190C300C2AD83F,
	IosLoadListener_OnLoadSuccess_m4CAE79C5A3BF78500B201FDD3A92E3034CBB0576,
	IosLoadListener_OnLoadFailure_mBA8102263DC8C669447782652CBBE47A3C82FB30,
	IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F,
	IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5,
	IosNativeObject_get_NativePtr_m59E9F3081935394429EE75F2598B3A241BDE13C9,
	IosNativeObject_set_NativePtr_mFEED092BD5C14859AA886B1B3836D9EDF39FEF30,
	NULL,
	IosNativeObject_Dispose_m2A0912C85D7750BCAB47810DE33F0152AAB4046E,
	IosNativeObject_CheckDisposedAndLogError_mDE54C78A98509B4C89B9BF694ADA7D64D0B847F5,
	IosNativeObject_BridgeTransfer_m0DA68532C573E123DB6D3DF6278DF96A16AC1E0D,
	IosNativeObject__ctor_m12D04541A783F3A2E5F8AEB935B3F52834F14749,
	IosNativeObject__cctor_m021A4AA6DC9BA41C678258F6667078F4AEE18308,
	IosPlatform_UnityAdsInitialize_mECE53AB0701EE386E0D5BCDFEB70DDBAC6C64D1D,
	IosPlatform_UnityAdsLoad_mE1F4D5B8598992C11564E0B479D22CAFF813038D,
	IosPlatform_UnityAdsGetDefaultPlacementID_mDED8747FC7E1B13DC2AEC3865257CA8DA5231400,
	IosPlatform_UnityAdsShow_mBB8B5203A041A8176C3E71C7C5B11BE83DCD699D,
	IosPlatform_UnityAdsGetDebugMode_mE3C1F76F623894CB7F604BF12C5E2D1D0FEF255A,
	IosPlatform_UnityAdsSetDebugMode_m8EDC93D2C178A83B0E7C26A0EFFFA6B3CF3EBBD9,
	IosPlatform_UnityAdsIsReady_m7B9242ED7031A5F0977BC380CE47E864C46EC6FB,
	IosPlatform_UnityAdsGetPlacementState_mA14E79C14F601678668E24EB00DED0A8D63443E5,
	IosPlatform_UnityAdsGetVersion_m1358AD2CCD735EB9134B0E95178A6722FFCFCE3F,
	IosPlatform_UnityAdsIsInitialized_mC1876BD790192AE879CAC5E06DE70D4B5905DA60,
	IosPlatform_UnityAdsSetMetaData_m8842D5FC9B23D8040D08A7DA4667AA5E306B5EA4,
	IosPlatform_UnityAdsSetReadyCallback_m446CFA2C00567380FFA5375DEA00D5F53F364E0B,
	IosPlatform_UnityAdsSetDidErrorCallback_mB6C329BB40205E10190DB2B64CBC53E3E12984B9,
	IosPlatform_UnityAdsSetDidStartCallback_mEAC128A1D30A73B4E89B82DCDED21D38C2AD6233,
	IosPlatform_UnityAdsSetDidFinishCallback_mA2AD6D9088297318B752F6DBADFA4383229EBFCB,
	IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9,
	IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84,
	IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5,
	IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E,
	IosPlatform_SetupPlatform_m385C2C5BD739E98CBBDA642E2161426910C57554,
	IosPlatform_Initialize_mFABBB355368491B8C675C392634CE79E76EDA875,
	IosPlatform_Load_mEB5F01869162DEB142E8D0441178BF36B1025C24,
	IosPlatform_Show_mA2EFAA9DAEC442BE2C058AD9CFE25BCA03DDDD30,
	IosPlatform_SetMetaData_m47026330939714B25F60E74C40C28D49C0FAF5A9,
	IosPlatform_GetDebugMode_mFA5ADBD19901A5984637A6CCD4B57DB1933941E2,
	IosPlatform_SetDebugMode_m7FA7274872E698C711B4187944F1750BFD71059B,
	IosPlatform_GetVersion_m1BB31C578D003B2378AC9C333236484A3476B856,
	IosPlatform_IsInitialized_mD29EA2769F8730B310158914F3018FDAE7291373,
	IosPlatform_IsReady_mEDD29A241595A9E70ADC1F735C013FCBA024B183,
	IosPlatform_GetPlacementState_mF1C05E5EF574574F16684E7D3400A8E1E8C27157,
	IosPlatform_GetDefaultPlacement_m378714BBF0AAE9E43FEAEB6CBD02D6EF18CFCD01,
	IosPlatform_OnInitializationComplete_mD278AE800393B6A72089CFEB83AA82F002CECED5,
	IosPlatform_OnInitializationFailed_m39B4D8DCFC75CE77FAA23E31C7A9CAF144F4BCE2,
	IosPlatform_OnUnityAdsAdLoaded_m41A6DE443697707A668A3A6261337B3F360213C1,
	IosPlatform_OnUnityAdsFailedToLoad_m8BD4A6316A28CAC39BA08DD19B232239F186F434,
	IosPlatform_OnUnityAdsShowFailure_m7777774460783293F46732FA6EDC2C44A149E4A9,
	IosPlatform_OnUnityAdsShowStart_mE81517CF44E81D9EB2CF87576A22198EF515F751,
	IosPlatform_OnUnityAdsShowClick_mCB7D429BBF86FA4A705773B39F30D21D6D571938,
	IosPlatform_OnUnityAdsShowComplete_mBFEDBDD453A33D5248A9398219346C9868BB8079,
	IosPlatform__ctor_m3B441570BCE922970FF004797FB102549A6DD3FD,
	UnsupportedBanner_get_IsLoaded_mDC178A2DCE79F464B2375CC54C07508791EF8220,
	UnsupportedBanner_SetupBanner_mED5E5C9580EBFD3D4EF77A4E117753D7C0430A81,
	UnsupportedBanner_Load_mCC131405E895BABFE9613157061F750B3ECDCE02,
	UnsupportedBanner_Show_m706EDE56C0F364EDE36C7912EDCD4EC78F428CF1,
	UnsupportedBanner_Hide_mA943DFC0F81643EF03E788BBAC95DC94E6543E49,
	UnsupportedBanner_SetPosition_mF283B7CA6741F7F5EF4320A446FDFF7F76966F9A,
	UnsupportedBanner__ctor_mE09660825501B22793934E4FAF88E2AC551B50AF,
	UnsupportedPlatform_SetupPlatform_mEAE492855EDA3294AFB634A31E530EE8A993DD75,
	UnsupportedPlatform_Initialize_m4C73B7A0D77D2E601DF19E1AF44B43D1A0977E92,
	UnsupportedPlatform_Load_m6C51586FCF4B5CB1FA2EE7E4A13351D111E30938,
	UnsupportedPlatform_Show_m8E9E985F1B06553F20759426389052FDB6DF0AC1,
	UnsupportedPlatform_SetMetaData_mEF9D2D804BB15C294121F26D62947B353BED0BAC,
	UnsupportedPlatform_GetDebugMode_m8E4C6C2056E75FEADAF90800DAE27F3094B6A123,
	UnsupportedPlatform_SetDebugMode_m0EB1217093E6A4B3416A6645AD5DD0691FCBA70C,
	UnsupportedPlatform_GetVersion_m2A186D53BD52D2F16BA08660E6CC6CF1FAE8F429,
	UnsupportedPlatform_IsInitialized_mC7E87A4B2506854627D1D846C7DE7F4663A80941,
	UnsupportedPlatform_IsReady_mD1C5A3E57449BD6A464ADDAFF37BA9C06AC4AFF1,
	UnsupportedPlatform_GetPlacementState_m4051099D5AACA910EC393ACA22916881D07DB20D,
	UnsupportedPlatform_GetDefaultPlacement_mC994C6A3F4A926E3FE236645E88A76997D9AF500,
	UnsupportedPlatform__ctor_m2F549700D3A58CD406CB0536CCD1374E0C5408CC,
	BannerPlaceholder_Awake_m8797C23CCC67E260BF11FB0494B01E3CF037BD62,
	BannerPlaceholder_OnGUI_mF6A4575DD98A6C311345F95BF94EAA30B3563940,
	BannerPlaceholder_ShowBanner_m679869708435680777BCDE8002C1A20B0FD21060,
	BannerPlaceholder_HideBanner_m0E9ECE5CED7E050E07C3A160ABA6240CADB89745,
	BannerPlaceholder_BackgroundTexture_m93516C740130C4B42C7FAB978E4D234E95E3B589,
	BannerPlaceholder_GetBannerRect_mD62442B18D4BA67465510CB21E04DCC9CFBF1AA3,
	BannerPlaceholder__ctor_m2F312011260B4F57CA2BFB1DEBB071A2C8D63883,
	BannerBundle_get_bannerView_m9444F9C37FD8F6A3C977ED6ABE1E7BF2F01C9340,
	BannerBundle_get_bannerPlacementId_mCBA57814029DC050330433EA892C125D7F90BD03,
	BannerBundle__ctor_m52B4940B9EAD63CA3FD67E9B994F25641FBE4C32,
	FinishEventArgs_get_placementId_m1461AC4F5FB9F28D10F5B1695387714D94E15453,
	FinishEventArgs_get_showResult_m3A4584DB549E20B21873168FAB50D51FD13C2141,
	FinishEventArgs__ctor_mD6C2E617EC4471D128F95137C670648F391D21AB,
	StartEventArgs_get_placementId_mD575F46F40275216C6556BE4528D373A9A504417,
	StartEventArgs__ctor_m51A040B1198DE55DDFC1561822066C1824C5D11F,
	Banner_Load_m8F0B89644A7E8227B661B16BFC466734B30D3864,
	Banner_Load_m679202DA86C37B386E05A4B4E89760E684AF9E31,
	Banner_Load_m4327AC4CECD3360C935F5EAA5D621DB5274D6E25,
	Banner_Load_m513306F8AE10F6863F5F9707E09B4EDF70475D4B,
	Banner_Show_mB92E83A3E1D74B4C6BD29764C34A2BF70C3CA015,
	Banner_Show_m2BD2E70418544D5CE1537182E478A7EEDD21869E,
	Banner_Show_mE14BF0CF411B798B10284082AF4176CC95CAE266,
	Banner_Show_mE068BD7E7550B6BC43EFE62B697BB766D06C57F8,
	Banner_Hide_m6DB127A82F9917B8FE25D6B7520602A3490F3C72,
	Banner_SetPosition_mE2025E86B4232B95C40490E92A7BA6E254705118,
	Banner_get_isLoaded_mBAE619B911977002066D1BBE1F79C2EEB7BC22DA,
	U3CU3Ec__DisplayClass15_0__ctor_m48FA11114E8D24B15C4D33F2AA28326B995A07CB,
	U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m0244AE6260E7538360458B780E7F23B859B71022,
	U3CU3Ec__DisplayClass16_0__ctor_mFAA0F4B67C2D46A305EDCAD38B531B038BEBAB54,
	U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mC569F24D68F709C1513557CD97D5AAEF33EAAD4D,
	U3CU3Ec__DisplayClass17_0__ctor_m23EF0FBDAFAEF1705835E8D724B8964D5A56AF0A,
	U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m972E62511BDE0E578161F3670852088E39EB4198,
	U3CU3Ec__DisplayClass18_0__ctor_mA6EDA97CB3BD8311F21206FC9406D6028F968285,
	U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_mF47C847B6465037DDE1D6648DAA1A663111EE629,
	U3CU3Ec__DisplayClass19_0__ctor_m13D5E505097D9B978F57A13921512945FCFFAEB2,
	U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m05225FDEE16AC76F5A9DEB5CEC653B526DEB2470,
	LoadCallback__ctor_m554496BBFAF6090101C451EFC3DDA9D4B28AFDD9,
	LoadCallback_Invoke_mD53ABCE2D8E6D083E691BC14B84F73B4C6A6C0D0,
	LoadCallback_BeginInvoke_m26602CBA25163AE693DAFDF99620BF81C71BF2C5,
	LoadCallback_EndInvoke_mD80580D2BC1B7700CE24D03AF8CA6F1311B50F94,
	ErrorCallback__ctor_m5F6C76E2C1A0E69E0279487EBB9FDB24C0856662,
	ErrorCallback_Invoke_m701B4DC15718D7F38A6E9922C6C93ADB294ABB75,
	ErrorCallback_BeginInvoke_mDEDC2599D76EFE86DAF0A2240A69F155EE32AB17,
	ErrorCallback_EndInvoke_mCC702738417912E72A46EA204D43E05F448CEA27,
	BannerCallback__ctor_m20E3DE6D818E1B6ED007489BCBD94A08450C41AB,
	BannerCallback_Invoke_mB376D0DAF03D14D5E9BE688CF45C46AD838F2665,
	BannerCallback_BeginInvoke_m4B1587162594D1A58508A1D4101B413525C8F291,
	BannerCallback_EndInvoke_m94947BDB3EF5E6400E579953A0CE3BD29B9573CC,
	ShowFailureCallback__ctor_mABD89A9FE9FDB2620713964214421C64C38334D7,
	ShowFailureCallback_Invoke_mD209237DC1A5B9B00FFDAF4FE4191F17FBA1239A,
	ShowFailureCallback_BeginInvoke_m82099083389E2ACD46ED4584E25BE8FA85A2EF03,
	ShowFailureCallback_EndInvoke_mDCEBCBD00168DF7BEBFA32F4BD493633B4D274D2,
	ShowStartCallback__ctor_m306F97E7A6DECBF231574CB30D9F512892113D20,
	ShowStartCallback_Invoke_mD7AE456A08CB5AC4666FF067D948BF8FE32AF0D9,
	ShowStartCallback_BeginInvoke_mE399B86E011C48424B62D6ABE31360C85B28A85D,
	ShowStartCallback_EndInvoke_m52D271424D8BC28793F6E2A5939F7EB5219AA9F0,
	ShowClickCallback__ctor_m5C78698DEA92CA52A0FD629BDB46D0692BB0DD67,
	ShowClickCallback_Invoke_m6D4B721CD1D6D649D957F446BE366BD34B5C9945,
	ShowClickCallback_BeginInvoke_mD52C3A7FEEC9BF4A0E20CA759C772F94EE5064C5,
	ShowClickCallback_EndInvoke_mA101B46A9D755DF51E3721CC118EEF5112AF6193,
	ShowCompleteCallback__ctor_m7E32BCE6D7DEAB85EAB2DB6CB3CE1DE440B200AA,
	ShowCompleteCallback_Invoke_m3E6A77E1DA0A2E8484C2BD25D3CDEC5B0B1BB500,
	ShowCompleteCallback_BeginInvoke_m87B7BDE93BE68284BCA218DB2D44C40E13D26670,
	ShowCompleteCallback_EndInvoke_m6C450B449C84C5ECB350F2B5B2BE4B59E3BDEF26,
	Parser_IsWordBreak_m70F6E82AE18BF5F9F067BDBDEA1ED2DB7A94B76B,
	Parser__ctor_m27A421BE339A651A13124EF5ACDE518D8E92CC59,
	Parser_Parse_m0F27CDB228E7CB42C18F79D26CC119A17A8ACF0B,
	Parser_Dispose_m6CF938F856463B3DC41714F68A69A2AEBB1532F4,
	Parser_ParseObject_m6E788343570EB3362AED3F4C5A11F235155EFC4F,
	Parser_ParseArray_m497AF5D1134DA5540EE4670DCAF401346A2FF95B,
	Parser_ParseValue_mBED236CF835AAB4095A87F4EB7BB21B7D7A1D4DC,
	Parser_ParseByToken_m092294F27563FAD18A25B0975680E85E50203E83,
	Parser_ParseString_mBCE35E8FD99DDBEEBF9537D38B0B546C90A9C774,
	Parser_ParseNumber_m07C532E02190931EB5A54F96FBA177283D5A22DF,
	Parser_EatWhitespace_mD4D99A568FD15265DE960FE01122751504D2D7A8,
	Parser_get_PeekChar_m3563C8C6266C03DC2104FC0B3061D722D2C1B959,
	Parser_get_NextChar_m99421292EED335E18206C0287DE40FC95883B380,
	Parser_get_NextWord_m4C2E8FE9FFA6DD1FC4D368E3C1006D7325ABFCC0,
	Parser_get_NextToken_m8AC572401758AA21CFC724457DEB519C50F568CD,
	Serializer__ctor_mF5D0C61AC8422CAD932288C50754D358FFD36DA8,
	Serializer_Serialize_m3237FDAC16D69EFAD4FB76BD806F960CA494CE93,
	Serializer_SerializeValue_m4F242828AFC466BD8B228175F0ACAADD628F489F,
	Serializer_SerializeObject_mFB4B7841D716E4B7093909041663C166A442911F,
	Serializer_SerializeArray_mAC12E5C8ADABD35D2B02A2C105D7CA1D4EAD895A,
	Serializer_SerializeString_mD46149CB5C25AA7E0C33E2E1655193080EC6E45E,
	Serializer_SerializeOther_m34AB8182921EC7E05370F7401B8C36383BEA3FCC,
	unityAdsPurchasingDidInitiatePurchasingCommand__ctor_mFF13A4A0FC3A957CCD8F13D49E034B720038D6F0,
	unityAdsPurchasingDidInitiatePurchasingCommand_Invoke_m65BF52AA54F79DB8AE555DA347327E4D1B9C21DD,
	unityAdsPurchasingDidInitiatePurchasingCommand_BeginInvoke_m166DBB665CEA894D676EC5738083A88401D0C061,
	unityAdsPurchasingDidInitiatePurchasingCommand_EndInvoke_m71014DEDA1684F2E444DCCB206FC8D80DC264085,
	unityAdsPurchasingGetProductCatalog__ctor_m49DC4805DB241B51950BA314492192B807388AF8,
	unityAdsPurchasingGetProductCatalog_Invoke_mE9B561D5D561143608BEEC5BF3243B16B2C0B2F3,
	unityAdsPurchasingGetProductCatalog_BeginInvoke_m64F8D25D0F3A26F652BD21C73BC0618685CD6127,
	unityAdsPurchasingGetProductCatalog_EndInvoke_mE62125AB227137F0755586FAC2E88C688FE33258,
	unityAdsPurchasingGetPurchasingVersion__ctor_m08135B89E3766213671E4E16F2055BEA920C0FD3,
	unityAdsPurchasingGetPurchasingVersion_Invoke_m39CDF8A39FED49EA0DE7CB76E6366E74255D78F0,
	unityAdsPurchasingGetPurchasingVersion_BeginInvoke_m73431C230B300FBBB40612E64A48E4A1748A0605,
	unityAdsPurchasingGetPurchasingVersion_EndInvoke_m612539347E18DADA8C4FA720D0656E2604408BA4,
	unityAdsPurchasingInitialize__ctor_mFDEF83AB48F82954C8B2818D7CB2574E1F459953,
	unityAdsPurchasingInitialize_Invoke_mCC266D739326D84559CE94E48AA843460FE5FC6A,
	unityAdsPurchasingInitialize_BeginInvoke_mAB2A6B01F22CA9B82A1468914AF2F6D005715DE5,
	unityAdsPurchasingInitialize_EndInvoke_mD2EBC7FB08515D311E9656E6CF8C248E961980E3,
	U3CU3Ec__DisplayClass33_0__ctor_mF2B6358160847184E602DF6FD693218A470329E0,
	U3CU3Ec__DisplayClass33_1__ctor_m116A199DD6E00689B16197D06B47C06C996E29C8,
	U3CU3Ec__DisplayClass33_1_U3CShowU3Eb__0_m04366E1087F5360B5D74D38DF742E089265AED70,
	U3CU3Ec__DisplayClass40_0__ctor_m37143F5B7AD0E1C937C1931A2FDFCF76F8EC5A8D,
	U3CU3Ec__DisplayClass40_0_U3CUnityAdsReadyU3Eb__0_mF0C63ACED7644FA87E01EBA2DB5A4CC7A66A18CB,
	U3CU3Ec__DisplayClass41_0__ctor_mD9E8DC2B4EFBB6BE12549EBFAD5859AE891E64C1,
	U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidErrorU3Eb__0_m7C7A32DB133CEA8C48D75B5EFBA84D823A16748A,
	U3CU3Ec__DisplayClass42_0__ctor_mC22053AEA31D532E87491E7E4D299088B53903AB,
	U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidStartU3Eb__0_m2E7BA09775E7525FA6C86C0C9F9B7437230339FE,
	U3CU3Ec__DisplayClass43_0__ctor_m6B6AAA0DA992112F815D94A678A37BA27F72031B,
	U3CU3Ec__DisplayClass43_0_U3CUnityAdsDidFinishU3Eb__0_m671FB4861A8951BE89DB6DFBE9AB0365A210B3AD,
	UnityAdsBannerShowDelegate__ctor_m0CC6C088AF8AD82567327FDAA576502918E23B90,
	UnityAdsBannerShowDelegate_Invoke_mE3BF1DEEEF5CA9237E7FCAFC0B9CD961EAC4E8D1,
	UnityAdsBannerShowDelegate_BeginInvoke_m628F7A8CEAC14638F61C5EA07F0455427399F47F,
	UnityAdsBannerShowDelegate_EndInvoke_m0878AEDC615D91C378D4DAAD41449D617A8C1977,
	UnityAdsBannerHideDelegate__ctor_mF03058B05B24B78DA5A1132B99EBBC1F09B9522D,
	UnityAdsBannerHideDelegate_Invoke_m4C7BC7D90AA9CD2C80F230984B246CC9961A3EF9,
	UnityAdsBannerHideDelegate_BeginInvoke_m656EC42544F2BBE87BF7ADE5A9ED890236C79151,
	UnityAdsBannerHideDelegate_EndInvoke_m49BC5649D0A60DF628309288ED866E0CD9814CC0,
	UnityAdsBannerClickDelegate__ctor_m703270CF4F72AC0D64A5BFEAF1924590E66436B2,
	UnityAdsBannerClickDelegate_Invoke_m61285605E80AA802CA9D72E876BA68FCE8A79ACB,
	UnityAdsBannerClickDelegate_BeginInvoke_m8515C2FBAD83B71BECD5AD4764CEF50AA1FCF0C0,
	UnityAdsBannerClickDelegate_EndInvoke_mA6921F240F409E474F90D8B23F5CA506FC1F3E83,
	UnityAdsBannerUnloadDelegate__ctor_m5F3A24005669043056E47533C0DAE81153EDF77B,
	UnityAdsBannerUnloadDelegate_Invoke_mF2E7F6BFB16633F8FF7696491D191D84A8097CEF,
	UnityAdsBannerUnloadDelegate_BeginInvoke_mC19ADBA7277C5FD4E80AB8C382855A0EE215DCD7,
	UnityAdsBannerUnloadDelegate_EndInvoke_m00C956E4CC3AD5B3EF55F78D9D0AFD744E449DD5,
	UnityAdsBannerLoadDelegate__ctor_m91AEFBD01369511101AA445344C327D8C8D2DB2E,
	UnityAdsBannerLoadDelegate_Invoke_mE8D0944CDA67EB8B3C424052FB4E48ECC117AF14,
	UnityAdsBannerLoadDelegate_BeginInvoke_mA0E9FDE74CE2ACDED017D119F0247E8A41CCACC1,
	UnityAdsBannerLoadDelegate_EndInvoke_m8E75F7C70DC378E69F78A298EF0666B774D21D54,
	UnityAdsBannerErrorDelegate__ctor_m26E223E378F2230181A1AE75D958AC492B89F8A0,
	UnityAdsBannerErrorDelegate_Invoke_m7982B1E113C92BB6E4725EF720788A8179C216B6,
	UnityAdsBannerErrorDelegate_BeginInvoke_m257D01AAB86D60024594F5D086B937D1F315EF96,
	UnityAdsBannerErrorDelegate_EndInvoke_mEE20DE6B16CA462EAC98DD468C82FFEA984679CC,
	InitSuccessCallback__ctor_mA89A2E55F84D6A6E87A21CE44E56FCADAAE93974,
	InitSuccessCallback_Invoke_m8675E974F7B4A82709024534508B7D1AB8831ED0,
	InitSuccessCallback_BeginInvoke_mE43C5BE2C100020F7796FEC223006EA3D09732D8,
	InitSuccessCallback_EndInvoke_m0BCE620369DC8414CAD2103EF00206103CA6DE65,
	InitFailureCallback__ctor_mEB6BA365EE12928514CEE27B38A0C7A4A3D31144,
	InitFailureCallback_Invoke_m954A61418AFF6045FA2E02599B67D9E99D119B3E,
	InitFailureCallback_BeginInvoke_m8742CA3DF5FDA4009D0027A85B33DA1A242C595C,
	InitFailureCallback_EndInvoke_m7B2C97054CD64DA05A19421DCA1707353461F712,
	LoadSuccessCallback__ctor_m0E059E7BD3E47FCEA2C5461FBF1E76AA82DF15FF,
	LoadSuccessCallback_Invoke_m3749425EEBD3138836A72138B8341DFC68E7A70F,
	LoadSuccessCallback_BeginInvoke_m07CAA25EE81F085427FAA28458408B504CB55EF4,
	LoadSuccessCallback_EndInvoke_mBB4A7AB192BC98F8037ED8C542439E4B6407D9D4,
	LoadFailureCallback__ctor_m0579B893FBCB70E04FDA64D705507D30153021CB,
	LoadFailureCallback_Invoke_m561153FA7B42A862AA78042C64E4C6A63B92BC68,
	LoadFailureCallback_BeginInvoke_mBA44D145FF3A5CD2B7FD4A60BCDF8E119FEB9E32,
	LoadFailureCallback_EndInvoke_mD6EC67174D69F153B5E66571CACE905470424A41,
	UnityAdsReadyDelegate__ctor_m0FDFC0CE23F22D3B3537FE82F957B907363F5771,
	UnityAdsReadyDelegate_Invoke_mA844E3E8CF864BFD6231FF13F6CD47784D3007C4,
	UnityAdsReadyDelegate_BeginInvoke_m00CFF1B2A50823F7339C7B329F4932828DEB0541,
	UnityAdsReadyDelegate_EndInvoke_m584CFB56CB6BBAC28FF40E07C3B953E77D0D2FF2,
	UnityAdsDidErrorDelegate__ctor_m67D062A988F87DFCAD003B229C6C5BE117C3E6F8,
	UnityAdsDidErrorDelegate_Invoke_mD57353F02B825394274C30CAA965946BBFFB0108,
	UnityAdsDidErrorDelegate_BeginInvoke_m4F136E17CF929CCDA944B573A33C3FD0FE986071,
	UnityAdsDidErrorDelegate_EndInvoke_mAEC544591F3CE2DD0CBD75B8EDD5BA13C9AE4C4C,
	UnityAdsDidStartDelegate__ctor_m5DFE41CFC206FFD54E3B941694AE7CBE69A3614B,
	UnityAdsDidStartDelegate_Invoke_mCF9BE5EA3C4AD1C6364CCE473008F0BC9FE0E77F,
	UnityAdsDidStartDelegate_BeginInvoke_mE8B31E7888C1C58BBEAEF415D7D76D741B881D1F,
	UnityAdsDidStartDelegate_EndInvoke_m5BDAD87414A96F786168D30FFD54B8DEFAB10A5F,
	UnityAdsDidFinishDelegate__ctor_m7D2D98B40574B22EB9774101F6601AC87F5914E2,
	UnityAdsDidFinishDelegate_Invoke_m3E4FF514A8F6646DEE8FA99F7B1A66E1C63AD7B3,
	UnityAdsDidFinishDelegate_BeginInvoke_m03CAF2322DEEE64EC9E2FC58E4926FA7CA3242DF,
	UnityAdsDidFinishDelegate_EndInvoke_m20EE3F37979F3F473DD1A867507B9AFA606A458A,
};
static const int32_t s_InvokerIndices[512] = 
{
	3,
	49,
	49,
	49,
	769,
	4,
	49,
	111,
	550,
	1442,
	1696,
	49,
	109,
	111,
	122,
	3,
	111,
	111,
	122,
	122,
	144,
	111,
	111,
	111,
	518,
	186,
	4,
	49,
	14,
	102,
	102,
	31,
	27,
	27,
	27,
	31,
	32,
	27,
	27,
	27,
	27,
	27,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	102,
	14,
	14,
	26,
	14,
	102,
	102,
	31,
	27,
	27,
	31,
	32,
	27,
	27,
	27,
	27,
	27,
	102,
	26,
	27,
	27,
	31,
	32,
	26,
	26,
	26,
	124,
	23,
	62,
	26,
	661,
	661,
	26,
	26,
	124,
	14,
	26,
	26,
	27,
	28,
	14,
	14,
	27,
	23,
	27,
	27,
	26,
	156,
	27,
	156,
	26,
	26,
	27,
	26,
	963,
	27,
	27,
	26,
	102,
	31,
	14,
	102,
	9,
	104,
	14,
	1697,
	25,
	27,
	23,
	661,
	26,
	26,
	124,
	1698,
	1429,
	1429,
	1699,
	14,
	26,
	14,
	26,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	21,
	-1,
	28,
	26,
	26,
	0,
	0,
	23,
	23,
	28,
	26,
	23,
	26,
	26,
	109,
	109,
	4,
	4,
	109,
	3,
	4,
	111,
	1706,
	111,
	111,
	111,
	111,
	111,
	3,
	3,
	3,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	14,
	14,
	14,
	102,
	102,
	14,
	102,
	31,
	14,
	963,
	27,
	156,
	23,
	26,
	26,
	9,
	104,
	26,
	26,
	26,
	26,
	124,
	26,
	26,
	26,
	26,
	14,
	14,
	14,
	102,
	102,
	31,
	14,
	102,
	31,
	14,
	156,
	963,
	26,
	27,
	156,
	23,
	26,
	26,
	9,
	104,
	26,
	26,
	26,
	26,
	124,
	0,
	27,
	27,
	102,
	31,
	550,
	769,
	49,
	121,
	111,
	111,
	111,
	111,
	111,
	111,
	3,
	111,
	111,
	111,
	111,
	111,
	111,
	26,
	27,
	27,
	31,
	32,
	23,
	1343,
	25,
	27,
	23,
	23,
	62,
	25,
	1707,
	1343,
	25,
	27,
	23,
	26,
	661,
	1429,
	1698,
	15,
	7,
	-1,
	23,
	9,
	25,
	23,
	3,
	1710,
	891,
	4,
	891,
	49,
	769,
	109,
	107,
	4,
	49,
	122,
	111,
	111,
	111,
	111,
	111,
	1706,
	111,
	108,
	26,
	963,
	27,
	27,
	26,
	102,
	31,
	14,
	102,
	9,
	104,
	14,
	23,
	62,
	26,
	661,
	661,
	26,
	26,
	124,
	23,
	102,
	26,
	27,
	27,
	31,
	32,
	23,
	26,
	963,
	27,
	27,
	26,
	102,
	31,
	14,
	102,
	9,
	104,
	14,
	23,
	23,
	23,
	62,
	23,
	1714,
	1338,
	23,
	14,
	14,
	27,
	14,
	10,
	124,
	14,
	26,
	3,
	111,
	111,
	122,
	3,
	111,
	111,
	122,
	769,
	121,
	49,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	163,
	23,
	101,
	26,
	163,
	26,
	166,
	26,
	163,
	23,
	101,
	26,
	163,
	1700,
	1701,
	26,
	163,
	1702,
	1703,
	26,
	163,
	1702,
	1703,
	26,
	163,
	1704,
	1705,
	26,
	48,
	26,
	0,
	23,
	14,
	14,
	14,
	34,
	14,
	14,
	23,
	201,
	201,
	14,
	10,
	23,
	0,
	26,
	26,
	26,
	26,
	26,
	163,
	26,
	166,
	26,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	163,
	23,
	101,
	26,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
	163,
	26,
	166,
	26,
	163,
	7,
	756,
	26,
	163,
	1708,
	1709,
	26,
	163,
	1702,
	1703,
	26,
	163,
	1700,
	1701,
	26,
	163,
	26,
	166,
	26,
	163,
	1711,
	1712,
	26,
	163,
	26,
	166,
	26,
	163,
	129,
	1713,
	26,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[22] = 
{
	{ 0x06000080, 1,  (void**)&IosShowListener_OnShowFailure_m3568157D0CBC25F6ECE3E001BF561AF4BA0FCF78_RuntimeMethod_var, 0 },
	{ 0x06000081, 2,  (void**)&IosShowListener_OnShowStart_m48335DB86C7B1B2FA4E91B656570E8F6B69080FA_RuntimeMethod_var, 0 },
	{ 0x06000082, 3,  (void**)&IosShowListener_OnShowClick_m52C8352B224676820EB99ED0E6499C59D12F5699_RuntimeMethod_var, 0 },
	{ 0x06000083, 4,  (void**)&IosShowListener_OnShowComplete_m1A7A9C6D239919C52C487278EAC22A9A24770F24_RuntimeMethod_var, 0 },
	{ 0x060000AA, 19,  (void**)&PurchasingPlatform_UnityAdsDidInitiatePurchasingCommand_mC1D2F236D4ACB716AD486655BB5EFC32976FCA0D_RuntimeMethod_var, 0 },
	{ 0x060000AB, 20,  (void**)&PurchasingPlatform_UnityAdsPurchasingGetProductCatalog_mFFB327A61F1945C5F24CDC88BDD2E52DF1294E1B_RuntimeMethod_var, 0 },
	{ 0x060000AC, 21,  (void**)&PurchasingPlatform_UnityAdsPurchasingGetPurchasingVersion_m4F728423ED62EA6444412192E70D8BC9B1FF4EB0_RuntimeMethod_var, 0 },
	{ 0x060000AD, 22,  (void**)&PurchasingPlatform_UnityAdsPurchasingInitialize_m24E5E10B74DD412262116E8082BD9FA419C04326_RuntimeMethod_var, 0 },
	{ 0x060000F8, 5,  (void**)&IosBanner_UnityAdsBannerDidShow_m0805F6AF4AB19FCD2A450A187A04F87B60063EAE_RuntimeMethod_var, 0 },
	{ 0x060000F9, 6,  (void**)&IosBanner_UnityAdsBannerDidHide_m2B384FE086F476F5C2C8D8421BEF930C5BA34E05_RuntimeMethod_var, 0 },
	{ 0x060000FA, 7,  (void**)&IosBanner_UnityAdsBannerClick_mEF6D21807F72D20C3E3FE0D135D37E0127B3007E_RuntimeMethod_var, 0 },
	{ 0x060000FB, 8,  (void**)&IosBanner_UnityAdsBannerDidError_mE3F1CE80A797FC32EB3178A5276F5FDBBAEF4675_RuntimeMethod_var, 0 },
	{ 0x060000FC, 9,  (void**)&IosBanner_UnityAdsBannerDidUnload_m011C89E9226450887213B18DB68F354BB6F64B19_RuntimeMethod_var, 0 },
	{ 0x060000FD, 10,  (void**)&IosBanner_UnityAdsBannerDidLoad_m4580204D26FE7D8D994E92CF148DABFDDB167988_RuntimeMethod_var, 0 },
	{ 0x0600010A, 11,  (void**)&IosInitializationListener_OnInitializationComplete_mFB705435D8E2BDCABB4D364B02FB5716601C9FA4_RuntimeMethod_var, 0 },
	{ 0x0600010B, 12,  (void**)&IosInitializationListener_OnInitializationFailed_m10D5415B1038853857A7EC123C6563899A0C323E_RuntimeMethod_var, 0 },
	{ 0x06000112, 13,  (void**)&IosLoadListener_OnLoadSuccess_m5DB2765C426BCA11C70BA32002889D6EDD14C70F_RuntimeMethod_var, 0 },
	{ 0x06000113, 14,  (void**)&IosLoadListener_OnLoadFailure_m2A06E2C90516012AEAC8AF3B01440316494F92C5_RuntimeMethod_var, 0 },
	{ 0x0600012B, 15,  (void**)&IosPlatform_UnityAdsReady_m9141A58FFD113DB5EFA7E7FF53BE07B7AD64E2A9_RuntimeMethod_var, 0 },
	{ 0x0600012C, 16,  (void**)&IosPlatform_UnityAdsDidError_m2962ABEBF11A7D48923B98C7A36539620C2EFD84_RuntimeMethod_var, 0 },
	{ 0x0600012D, 17,  (void**)&IosPlatform_UnityAdsDidStart_m27B5697487D994AEA04696A84AC7AC7CE2B4D3D5_RuntimeMethod_var, 0 },
	{ 0x0600012E, 18,  (void**)&IosPlatform_UnityAdsDidFinish_m7EB6249040C8FD7C5CEED7B003FCA599139C9F9E_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000090, { 0, 2 } },
	{ 0x06000116, { 2, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)1, 15965 },
	{ (Il2CppRGCTXDataType)2, 15965 },
	{ (Il2CppRGCTXDataType)2, 16053 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AdvertisementsCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AdvertisementsCodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	512,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	22,
	s_reversePInvokeIndices,
	2,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
};
